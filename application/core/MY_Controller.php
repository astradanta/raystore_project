<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("brand","",true);
		$this->load->model('web',"",true);
		$this->load->model('kategori',"",true);
		$this->load->model('produk',"",true);
		$this->load->model('produk_po',"",true);
	}

	public function index()
	{
		
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */