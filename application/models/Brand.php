<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	function listBrand(){
		return $this->db->get('tb_brand')->result();
	}
	function brandInc(){
		$sql = "SHOW TABLE STATUS LIKE 'tb_brand'";
		return $this->db->query($sql)->result();
	}
	function addBrand($name,$icon){
		$created_at = date('Y-m-d h:i:s;');
		$data = array("name"=>$name,"icon"=>$icon,"created_at"=>$created_at);
		return $this->db->insert('tb_brand',$data);
	}
	function detailBrand($id_brand){
		$this->db->where('id_brand',$id_brand);
		return $this->db->get('tb_brand')->result();
	}
	function editBrand($id_brand,$name,$icon){
		$updated_at = date("Y-m-d h:i:s");
		$data = array("name"=>$name,"icon"=>$icon,"updated_at"=>$updated_at);
		$this->db->where('id_brand',$id_brand);
		return $this->db->update('tb_brand',$data);
	}
	function deleteBrand($id_brand){
		$this->db->where('id_brand',$id_brand);
		return $this->db->delete('tb_brand');
	}

}

/* End of file Brand.php */
/* Location: ./application/models/Brand.php */