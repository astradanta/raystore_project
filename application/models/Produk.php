<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	function listProduk(){
		$this->db->select('tb_produk.*,tb_kategori.name as kategori, tb_brand.name as brand, if(sum(tb_stock.amount) is null,0,sum(tb_stock.amount)) as amount, b.subimage');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori', 'tb_produk.id_kategori = tb_kategori.id_kategori');
		$this->db->join('tb_brand', 'tb_produk.id_brand = tb_brand.id_brand');
		$this->db->join('tb_stock', 'tb_produk.id_produk = tb_stock.id_produk','left');
		$this->db->join('(SELECT GROUP_CONCAT(tb_product_image.image) as subimage, tb_product_image.id_produk from tb_product_image GROUP by tb_product_image.id_produk) b', 'tb_produk.id_produk = b.id_produk','left');
		$this->db->where('tb_produk.type_produk', 0);				
		$this->db->group_by('tb_stock.id_produk');
		return $this->db->get()->result();
	}
	function produkInc(){
		$sql = "SHOW TABLE STATUS LIKE 'tb_produk'";
		return $this->db->query($sql)->result();
	}
	function addProduk($id_kategori,$id_brand,$nama_produk,$harga_beli,$harga_jual,$cover,$deskripsi,$bestseller){
		$created_at = date('Y-m-d h:i:s;');
		$data = array("id_brand"=>$id_brand,"id_kategori"=>$id_kategori,"nama_produk"=>$nama_produk,"harga_beli"=>$harga_beli,"harga_jual"=>$harga_jual,"cover"=>$cover,"deskripsi"=>$deskripsi,"created_at"=>$created_at,"bestseller"=>$bestseller);
		$result['status'] = $this->db->insert('tb_produk',$data);
		if($result['status']){
			$result['id'] = $this->db->insert_id();
		}
		return $result;
	}
	function detailProduk($id_produk){
		$this->db->select('tb_produk.*,tb_kategori.name as kategori, tb_brand.name as brand, if(sum(tb_stock.amount) is null,0,sum(tb_stock.amount)) as amount, b.subimage');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori', 'tb_produk.id_kategori = tb_kategori.id_kategori');
		$this->db->join('tb_brand', 'tb_produk.id_brand = tb_brand.id_brand');
		$this->db->join('tb_stock', 'tb_produk.id_produk = tb_stock.id_produk','left');
		$this->db->join('(SELECT GROUP_CONCAT(tb_product_image.image) as subimage, tb_product_image.id_produk from tb_product_image GROUP by tb_product_image.id_produk) b', 'tb_produk.id_produk = b.id_produk','left');
		$this->db->where('tb_produk.type_produk', 0);				
		$this->db->group_by('tb_stock.id_produk');
		$this->db->where('tb_produk.id_produk',$id_produk);
		return $this->db->get()->result();
	}
	function editProduk($id_produk,$id_kategori,$id_brand,$nama_produk,$harga_beli,$harga_jual,$cover,$deskripsi,$bestseller){
		$updated_at = date("Y-m-d h:i:s");
		$data = array("id_brand"=>$id_brand,"id_kategori"=>$id_kategori,"nama_produk"=>$nama_produk,"harga_beli"=>$harga_beli,"harga_jual"=>$harga_jual,"cover"=>$cover,"deskripsi"=>$deskripsi,"updated_at"=>$updated_at,"bestseller"=>$bestseller);
		$this->db->where('id_produk',$id_produk);
		return $this->db->update('tb_produk',$data);
	}
	function deleteProduk($id_produk){
		$this->db->where('id_produk',$id_produk);
		return $this->db->delete('tb_produk');
	}
	function firstStock($id_produk,$amount){
		$created_at = date("Y-m-d h:i:s");
		$data = array("id_produk"=>$id_produk,"type"=>"0","amount"=>$amount,"description"=>"First add stock","created_at"=>$created_at);
		return $this->db->insert('tb_stock',$data);
	}

	function checkImage($id_produk){
		$this->db->where('id_produk', $id_produk);
		return $this->db->get('tb_product_image')->result();
	}

	function addImage($id_produk,$image){
		$data = array("id_produk"=>$id_produk,"image"=>$image);
		return $this->db->insert('tb_product_image', $data);
	}
	function editImage($id_image,$id_produk,$image){
		$data = array("id_produk"=>$id_produk,"image"=>$image);
		$this->db->where('id_image', $id_image);
		return $this->db->update('tb_product_image', $data);
	}
	function detailImage($id_image){
		$this->db->where('id_image', $id_image);
		return $this->db->get('tb_product_image')->result();
	}
	function deleteImage($id_image){
		$this->db->where('id_image', $id_image);
		return $this->db->delete('tb_product_image');
	}
	function produkByKategori($id_kategori,$sort = null, $limit = 20, $page = 1){
		$this->db->select('tb_produk.*, tb_kategori.name as kategori, tb_brand.name as brand, if(c.amount is null, 0, c.amount) as amount, b.subimage');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori', 'tb_produk.id_kategori = tb_kategori.id_kategori');
		$this->db->join('tb_brand', 'tb_produk.id_brand = tb_brand.id_brand');
		$this->db->join('(SELECT tb_stock.id_produk,sum(tb_stock.amount) as amount FROM tb_stock GROUP BY tb_stock.id_produk) as c', 'tb_produk.id_produk = c.id_produk','left');
		$this->db->join('(SELECT GROUP_CONCAT(tb_product_image.image) as subimage, tb_product_image.id_produk from tb_product_image GROUP by tb_product_image.id_produk) b', 'tb_produk.id_produk = b.id_produk','left');				
		($id_kategori != "all") ? $this->db->where('tb_kategori.id_kategori', $id_kategori):"";
		$this->db->where('c.amount is not null');
		$this->db->where('c.amount >', 0);
		$this->db->where('tb_produk.type_produk', 0);
		$this->db->group_by('tb_produk.id_produk');
		$offset = ($page - 1) * $limit;
		if($sort != null){
			switch ($sort) {
				case 'name_asc':
					$this->db->order_by('nama_produk');
					break;
				case 'name_desc':
					$this->db->order_by('nama_produk','desc');
					break;
				case 'price_asc':
					$this->db->order_by('harga_beli');
					break;
				case 'price_desc':
					$this->db->order_by('harga_beli','desc');
					break;
				default:
					# code...
					break;
			}
		}
		return $this->db->get('', $limit, $offset)->result();		
	}
	function produkBestseller(){
		$this->db->select('tb_produk.*, tb_kategori.name as kategori, tb_brand.name as brand, if(c.amount is null, 0, c.amount) as amount, b.subimage');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori', 'tb_produk.id_kategori = tb_kategori.id_kategori');
		$this->db->join('tb_brand', 'tb_produk.id_brand = tb_brand.id_brand');
		$this->db->join('(SELECT tb_stock.id_produk,sum(tb_stock.amount) as amount FROM tb_stock GROUP BY tb_stock.id_produk) as c', 'tb_produk.id_produk = c.id_produk','left');
		$this->db->join('(SELECT GROUP_CONCAT(tb_product_image.image) as subimage, tb_product_image.id_produk from tb_product_image GROUP by tb_product_image.id_produk) b', 'tb_produk.id_produk = b.id_produk','left');				
		$this->db->where('c.amount is not null');
		$this->db->where('c.amount >', 0);
		$this->db->where('tb_produk.bestseller', 1);
		$this->db->group_by('tb_produk.id_produk');
		return $this->db->get()->result();
	}
	function pagginationKategori($id_kategori,$limit = 20){
		$this->db->select('tb_produk.*, tb_kategori.name as kategori, tb_brand.name as brand, if(c.amount is null, 0, c.amount) as amount, b.subimage');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori', 'tb_produk.id_kategori = tb_kategori.id_kategori');
		$this->db->join('tb_brand', 'tb_produk.id_brand = tb_brand.id_brand');
		$this->db->join('(SELECT tb_stock.id_produk,sum(tb_stock.amount) as amount FROM tb_stock GROUP BY tb_stock.id_produk) as c', 'tb_produk.id_produk = c.id_produk','left');
		$this->db->join('(SELECT GROUP_CONCAT(tb_product_image.image) as subimage, tb_product_image.id_produk from tb_product_image GROUP by tb_product_image.id_produk) b', 'tb_produk.id_produk = b.id_produk','left');				
		($id_kategori != "all") ? $this->db->where('tb_kategori.id_kategori', $id_kategori):"";
		$this->db->where('c.amount is not null');
		$this->db->where('c.amount >', 0);
		$this->db->where('tb_produk.type_produk', 0);
		$this->db->group_by('tb_produk.id_produk');
		$count = $this->db->get()->num_rows();
		$result['page_count'] = floor($count/$limit) + ($count%$limit === 0 ? 0:1);
		$result['item_count'] = $count;
		$result['show_item'] = ($count < $limit ? $count : $limit);
		return $result;
	}
	function detailProdukFront($id_produk){
		$this->db->select('tb_produk.*, tb_kategori.name as kategori, tb_brand.name as brand, if(c.amount is null, 0, c.amount) as amount, b.subimage');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori', 'tb_produk.id_kategori = tb_kategori.id_kategori');
		$this->db->join('tb_brand', 'tb_produk.id_brand = tb_brand.id_brand');
		$this->db->join('(SELECT tb_stock.id_produk,sum(tb_stock.amount) as amount FROM tb_stock GROUP BY tb_stock.id_produk) as c', 'tb_produk.id_produk = c.id_produk','left');
		$this->db->join('(SELECT GROUP_CONCAT(tb_product_image.image) as subimage, tb_product_image.id_produk from tb_product_image GROUP by tb_product_image.id_produk) b', 'tb_produk.id_produk = b.id_produk','left');				
		$this->db->where('tb_produk.id_produk',$id_produk);
		$this->db->where('c.amount is not null');
		$this->db->where('c.amount >', 0);
		$this->db->group_by('tb_produk.id_produk');
		return $this->db->get()->result();
	}
	function produkByBrand($id_brand,$sort = null, $limit = 20, $page = 1){
		$this->db->select('tb_produk.*, tb_kategori.name as kategori, tb_brand.name as brand, if(c.amount is null, 0, c.amount) as amount, b.subimage');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori', 'tb_produk.id_kategori = tb_kategori.id_kategori');
		$this->db->join('tb_brand', 'tb_produk.id_brand = tb_brand.id_brand');
		$this->db->join('(SELECT tb_stock.id_produk,sum(tb_stock.amount) as amount FROM tb_stock GROUP BY tb_stock.id_produk) as c', 'tb_produk.id_produk = c.id_produk','left');
		$this->db->join('(SELECT GROUP_CONCAT(tb_product_image.image) as subimage, tb_product_image.id_produk from tb_product_image GROUP by tb_product_image.id_produk) b', 'tb_produk.id_produk = b.id_produk','left');				
		$this->db->where('tb_brand.id_brand',$id_brand);
		$this->db->where('c.amount is not null');
		$this->db->where('c.amount >', 0);
		$this->db->group_start('','or');
			$this->db->where('tb_produk.type_produk', 1);
			$this->db->where('tb_brand.id_brand',$id_brand);
		$this->db->group_end();
		$this->db->group_by('tb_produk.id_produk');
		$offset = ($page - 1) * $limit;
		if($sort != null){
			switch ($sort) {
				case 'name_asc':
					$this->db->order_by('nama_produk');
					break;
				case 'name_desc':
					$this->db->order_by('nama_produk','desc');
					break;
				case 'price_asc':
					$this->db->order_by('harga_beli');
					break;
				case 'price_desc':
					$this->db->order_by('harga_beli','desc');
					break;
				default:
					# code...
					break;
			}
		}		
		return $this->db->get('', $limit, $offset)->result();		
	}
	function pagginationBrand($id_brand,$limit = 20){
		$this->db->select('tb_produk.*, tb_kategori.name as kategori, tb_brand.name as brand, if(c.amount is null, 0, c.amount) as amount, b.subimage');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori', 'tb_produk.id_kategori = tb_kategori.id_kategori');
		$this->db->join('tb_brand', 'tb_produk.id_brand = tb_brand.id_brand');
		$this->db->join('(SELECT tb_stock.id_produk,sum(tb_stock.amount) as amount FROM tb_stock GROUP BY tb_stock.id_produk) as c', 'tb_produk.id_produk = c.id_produk','left');
		$this->db->join('(SELECT GROUP_CONCAT(tb_product_image.image) as subimage, tb_product_image.id_produk from tb_product_image GROUP by tb_product_image.id_produk) b', 'tb_produk.id_produk = b.id_produk','left');				
		$this->db->where('tb_brand.id_brand',$id_brand);
		$this->db->where('c.amount is not null');
		$this->db->where('c.amount >', 0);
		$this->db->group_start('','or');
			$this->db->where('tb_produk.type_produk', 1);
			$this->db->where('tb_brand.id_brand',$id_brand);
		$this->db->group_end();
		$this->db->group_by('tb_produk.id_produk');
		$count = $this->db->get()->num_rows();
		$result['page_count'] = floor($count/$limit) + ($count%$limit === 0 ? 0:1);
		$result['item_count'] = $count;
		$result['show_item'] = ($count < $limit ? $count : $limit);
		return $result;
	}
	function produkSearch($key,$id_kategori,$sort = null, $limit = 20, $page = 1){
		$this->db->select('tb_produk.*, tb_kategori.name as kategori, tb_brand.name as brand, if(c.amount is null, 0, c.amount) as amount, b.subimage');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori', 'tb_produk.id_kategori = tb_kategori.id_kategori');
		$this->db->join('tb_brand', 'tb_produk.id_brand = tb_brand.id_brand');
		$this->db->join('(SELECT tb_stock.id_produk,sum(tb_stock.amount) as amount FROM tb_stock GROUP BY tb_stock.id_produk) as c', 'tb_produk.id_produk = c.id_produk','left');
		$this->db->join('(SELECT GROUP_CONCAT(tb_product_image.image) as subimage, tb_product_image.id_produk from tb_product_image GROUP by tb_product_image.id_produk) b', 'tb_produk.id_produk = b.id_produk','left');
		$this->db->group_start();
			($id_kategori != "all") ? $this->db->where('tb_kategori.id_kategori', $id_kategori):"";				
			$this->db->where('c.amount is not null');
			$this->db->where('c.amount >', 0);
			$this->db->group_start('','or');
				$this->db->where('tb_produk.type_produk', 1);
				($id_kategori != "all") ? $this->db->where('tb_kategori.id_kategori', $id_kategori):"";	
			$this->db->group_end();
		$this->db->group_end();
		$this->db->group_start();
			$this->db->like('tb_brand.name', $key, 'BOTH');
			$this->db->or_like('tb_produk.nama_produk', $key, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('tb_produk.id_produk');
		$offset = ($page - 1) * $limit;
		if($sort != null){
			switch ($sort) {
				case 'name_asc':
					$this->db->order_by('nama_produk');
					break;
				case 'name_desc':
					$this->db->order_by('nama_produk','desc');
					break;
				case 'price_asc':
					$this->db->order_by('harga_beli');
					break;
				case 'price_desc':
					$this->db->order_by('harga_beli','desc');
					break;
				default:
					# code...
					break;
			}
		}		
		return $this->db->get('', $limit, $offset)->result();
	}
	function pagginationSearch($key,$id_kategori,$limit = 20){
		$this->db->select('tb_produk.*, tb_kategori.name as kategori, tb_brand.name as brand, if(c.amount is null, 0, c.amount) as amount, b.subimage');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori', 'tb_produk.id_kategori = tb_kategori.id_kategori');
		$this->db->join('tb_brand', 'tb_produk.id_brand = tb_brand.id_brand');
		$this->db->join('(SELECT tb_stock.id_produk,sum(tb_stock.amount) as amount FROM tb_stock GROUP BY tb_stock.id_produk) as c', 'tb_produk.id_produk = c.id_produk','left');
		$this->db->join('(SELECT GROUP_CONCAT(tb_product_image.image) as subimage, tb_product_image.id_produk from tb_product_image GROUP by tb_product_image.id_produk) b', 'tb_produk.id_produk = b.id_produk','left');
		$this->db->group_start();
			($id_kategori != "all") ? $this->db->where('tb_kategori.id_kategori', $id_kategori):"";				
			$this->db->where('c.amount is not null');
			$this->db->where('c.amount >', 0);
			$this->db->group_start('','or');
				$this->db->where('tb_produk.type_produk', 1);
				($id_kategori != "all") ? $this->db->where('tb_kategori.id_kategori', $id_kategori):"";	
			$this->db->group_end();
		$this->db->group_end();
		$this->db->group_start();
			$this->db->like('tb_brand.name', $key, 'BOTH');
			$this->db->or_like('tb_produk.nama_produk', $key, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('tb_produk.id_produk');
		$count = $this->db->get()->num_rows();
		$result['page_count'] = floor($count/$limit) + ($count%$limit === 0 ? 0:1);
		$result['item_count'] = $count;
		$result['show_item'] = ($count < $limit ? $count : $limit);
		return $result;
	}
}

/* End of file Product.php */
/* Location: ./application/models/Product.php */