<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	function listStock(){
		$this->db->select('tb_stock.*, tb_produk.nama_produk');
		$this->db->from('tb_stock');
		$this->db->join('tb_produk', 'tb_produk.id_produk = tb_stock.id_produk');
		return $this->db->get()->result();
	}
	function stockInc(){
		$sql = "SHOW TABLE STATUS LIKE 'tb_stock'";
		return $this->db->query($sql)->result();
	}
	function addStock($id_produk,$type,$amount,$description){
		$created_at = date('Y-m-d h:i:s;');
		$data = array("id_produk"=>$id_produk,"type"=>$type,"amount"=>$amount,"description"=>$description,"created_at"=>$created_at);
		return $this->db->insert('tb_stock',$data);
	}
	function detailStock($id_stock){
		$this->db->where('id_stock',$id_stock);
				$this->db->select('tb_stock.*, tb_produk.nama_produk');
		$this->db->from('tb_stock');
		$this->db->join('tb_produk', 'tb_produk.id_produk = tb_stock.id_produk');
		return $this->db->get()->result();
	}
	function editStock($id_stock,$id_produk,$type,$amount,$description){
		$updated_at = date("Y-m-d h:i:s");
		$data = array("id_produk"=>$id_produk,"type"=>$type,"amount"=>$amount,"description"=>$description,"updated_at"=>$updated_at);
		$this->db->where('id_stock',$id_stock);
		return $this->db->update('tb_stock',$data);
	}
	function deleteStock($id_stock){
		$this->db->where('id_stock',$id_stock);
		return $this->db->delete('tb_stock');
	}

}

/* End of file Stock.php */
/* Location: ./application/models/Stock.php */