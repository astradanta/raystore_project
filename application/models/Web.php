<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	function listSlider(){
		return $this->db->get('tb_slider')->result();
	}
	function addSlider($image,$link){
		$created_at = date('Y-m-d h:i:s;');
		$data = array("image"=>$image,"link"=>$link,"created_at"=>$created_at);
		return $this->db->insert('tb_slider',$data);
	}
	function detailSlider($id_slider){
		$this->db->where('id_slider',$id_slider);
		return $this->db->get('tb_slider')->result();
	}
	function editSlider($id_slider,$image,$link){
		$updated_at = date("Y-m-d h:i:s");
		$data = array("image"=>$image,"link"=>$link,"updated_at"=>$updated_at);
		$this->db->where('id_slider',$id_slider);
		return $this->db->update('tb_slider',$data);
	}
	function deleteSlider($id_slider){
		$this->db->where('id_slider',$id_slider);
		return $this->db->delete('tb_slider');
	}
	function listBanner(){
		return $this->db->get('tb_banner')->result();
	}
	function detailBanner($id_banner){
		$this->db->where('id_banner',$id_banner);
		return $this->db->get('tb_banner')->result();
	}
	function editBanner($id_banner,$image,$link){
		$updated_at = date("Y-m-d h:i:s");
		$data = array("image"=>$image,"link"=>$link,"updated_at"=>$updated_at);
		$this->db->where('id_banner',$id_banner);
		return $this->db->update('tb_banner',$data);
	}
	function listNegara(){
		return $this->db->get('tb_negara')->result();
	}
}

/* End of file Web.php */
/* Location: ./application/models/Web.php */