<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	function authCpanel($email){
		$this->db->select("tb_user.*,tb_role.description");
		$this->db->from("tb_user");
		$this->db->join("tb_role","tb_user.id_role = tb_role.id_role");
		$this->db->where('email',$email);
		$this->db->where('tb_user.id_role !=',1);
		//echo $this->db->_compile_select();
		return $this->db->get()->result();
	}
	function auth($email){
		$this->db->select("tb_user.*,tb_role.description");
		$this->db->from("tb_user");
		$this->db->join("tb_role","tb_user.id_role = tb_role.id_role");
		$this->db->where('email',$email);
		$this->db->where('tb_user.id_role',1);
		//echo $this->db->_compile_select();
		return $this->db->get()->result();
	}
	function listUser(){
		$this->db->select("tb_user.*,tb_role.description");
		$this->db->from("tb_user");
		$this->db->join("tb_role","tb_user.id_role = tb_role.id_role");
		return $this->db->get()->result();
	}
	function userInc(){
		$sql = "SHOW TABLE STATUS LIKE 'tb_user'";
		return $this->db->query($sql)->result();
	}
	function userRole(){
		$this->db->where('id_role >',1);
		return $this->db->get('tb_role')->result();
	}
	function addUser($name,$id_role,$email,$password,$icon){
		$created_at = date('Y-m-d h:i:s;');
		$data = array("name"=>$name,"id_role"=>$id_role,"email"=>$email,"password"=>$password,"icon"=>$icon,"created_at"=>$created_at);
		return $this->db->insert('tb_user',$data);
	}
	function detailUser($id_user){
		$this->db->select("tb_user.*,tb_role.description");
		$this->db->from("tb_user");
		$this->db->join("tb_role","tb_user.id_role = tb_role.id_role");
		$this->db->where('id_user',$id_user);
		return $this->db->get()->result();
	}
	function editUser($id_user,$name,$id_role,$email,$password,$icon){
		$updated_at = date("Y-m-d h:i:s");
		$data = array("name"=>$name,"id_role"=>$id_role,"email"=>$email,"password"=>$password,"icon"=>$icon,"updated_at"=>$updated_at);
		$this->db->where('id_user',$id_user);
		return $this->db->update('tb_user',$data);
	}
	function deleteUser($id_user){
		$this->db->where('id_user',$id_user);
		return $this->db->delete('tb_user');
	}
	function addCustomerLogin($name,$id_customer,$email,$password){
		$created_at = date('Y-m-d h:i:s;');
		$data = array("name"=>$name,"id_customer"=>$id_customer,"id_role"=>1,"email"=>$email,"password"=>$password,"created_at"=>$created_at);
		return $this->db->insert('tb_user',$data);
	}
}

/* End of file User.php */
/* Location: ./application/models/User.php */