<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_po extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	function listProdukPo(){
		$this->db->select('tb_produk.*,tb_brand.name as brand, tb_kategori.name as kategori, GROUP_CONCAT(tb_product_image.image) as subimage');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_produk.id_kategori');
		$this->db->join('tb_brand', 'tb_brand.id_brand = tb_produk.id_brand');
		$this->db->join('tb_product_image', ' tb_produk.id_produk = tb_product_image.id_produk','left');
		$this->db->where('tb_produk.type_produk', 1);				
		$this->db->group_by('tb_produk.id_produk');	
		return $this->db->get()->result();
	}
	function produkPoInc(){
		$sql = "SHOW TABLE STATUS LIKE 'tb_produk'";
		return $this->db->query($sql)->result();
	}
	function addProdukPo($id_kategori,$id_brand,$nama_barang,$harga_beli,$harga_jual,$cover,$deskripsi){
		$created_at = date('Y-m-d h:i:s;');
		$data = array("id_brand"=>$id_brand,"id_kategori"=>$id_kategori,"nama_produk"=>$nama_barang,"harga_beli"=>$harga_beli,"harga_jual"=>$harga_jual,"cover"=>$cover,"deskripsi"=>$deskripsi,"created_at"=>$created_at,"type_produk"=>1);
		return  $this->db->insert('tb_produk',$data);
	}
	function detailProdukPo($id_barang_po){
		$this->db->select('tb_produk.*,tb_brand.name as brand, tb_kategori.name as kategori, GROUP_CONCAT(tb_product_image.image) as subimage');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_produk.id_kategori');
		$this->db->join('tb_brand', 'tb_brand.id_brand = tb_produk.id_brand');
		$this->db->join('tb_product_image', ' tb_produk.id_produk = tb_product_image.id_produk','left');
		$this->db->where('tb_produk.type_produk', 1);				
		$this->db->group_by('tb_produk.id_produk');	
		$this->db->where('tb_produk.id_produk',$id_barang_po);
		return $this->db->get()->result();
	}
	function editProdukPo($id_barang_po,$id_kategori,$id_brand,$nama_barang,$harga_beli,$harga_jual,$cover,$deskripsi){
		$updated_at = date("Y-m-d h:i:s");
		$data = array("id_brand"=>$id_brand,"id_kategori"=>$id_kategori,"nama_produk"=>$nama_barang,"harga_beli"=>$harga_beli,"harga_jual"=>$harga_jual,"cover"=>$cover,"deskripsi"=>$deskripsi,"updated_at"=>$updated_at);
		$this->db->where('id_produk',$id_barang_po);
		return $this->db->update('tb_produk',$data);
	}
	function deleteProdukPo($id_barang_po){
		$this->db->where('id_produk',$id_barang_po);
		return $this->db->delete('tb_produk');
	}
	function checkImagePo($id_barang_po){
		$this->db->where('id_produk', $id_barang_po);
		return $this->db->get('tb_product_image')->result();
	}

	function addImagePo($id_barang_po,$image){
		$data = array("id_produk"=>$id_barang_po,"image"=>$image);
		return $this->db->insert('tb_product_image', $data);
	}
	function editImagePo($id_image,$id_barang_po,$image){
		$data = array("id_produk"=>$id_barang_po,"image"=>$image);
		$this->db->where('id_image', $id_image);
		return $this->db->update('tb_product_image', $data);
	}
	function detailImagePo($id_image){
		$this->db->where('id_image', $id_image);
		return $this->db->get('tb_product_image')->result();
	}
	function deleteImagePo($id_image){
		$this->db->where('id_image', $id_image);
		return $this->db->delete('tb_product_image');
	}
	function poFront($sort = null, $limit = 20, $page = 1){
		$this->db->select('tb_produk.*,tb_brand.name as brand, tb_kategori.name as kategori, GROUP_CONCAT(tb_product_image.image) as subimage');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_produk.id_kategori');
		$this->db->join('tb_brand', 'tb_brand.id_brand = tb_produk.id_brand');
		$this->db->join('tb_product_image', ' tb_produk.id_produk = tb_product_image.id_produk','left');
		$this->db->where('tb_produk.type_produk', 1);				
		$this->db->group_by('tb_produk.id_produk');		
		$offset = ($page - 1) * $limit;
		if($sort != null){
			switch ($sort) {
				case 'name_asc':
					$this->db->order_by('nama_produk');
					break;
				case 'name_desc':
					$this->db->order_by('nama_produk','desc');
					break;
				case 'price_asc':
					$this->db->order_by('harga_beli');
					break;
				case 'price_desc':
					$this->db->order_by('harga_beli','desc');
					break;
				default:
					# code...
					break;
			}
		}
		return $this->db->get('', $limit, $offset)->result();		
	}
	function poPagination($limit = 20){
		$this->db->select('tb_produk.*,tb_brand.name as brand, tb_kategori.name as kategori, GROUP_CONCAT(tb_product_image.image) as subimage');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_produk.id_kategori');
		$this->db->join('tb_brand', 'tb_brand.id_brand = tb_produk.id_brand');
		$this->db->join('tb_product_image', ' tb_produk.id_produk = tb_product_image.id_produk','left');
		$this->db->where('tb_produk.type_produk', 1);				
		$this->db->group_by('tb_produk.id_produk');	
		$count = $this->db->get()->num_rows();
		$result['page_count'] = floor($count/$limit) + ($count%$limit === 0 ? 0:1);
		$result['item_count'] = $count;
		$result['show_item'] = ($count < $limit ? $count : $limit);
		return $result;
	}
}

/* End of file Produk_po.php */
/* Location: ./application/models/Produk_po.php */