<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	function addCustomer($nama,$email,$no_hp,$alamat,$kota,$kode_pos,$code,$type_customer){
		$created_at = date('Y-m-d');
		$data = array("nama"=>$nama,"email"=>$email,"no_hp"=>$no_hp,"alamat"=>$alamat,"kota"=>$kota,"kode_pos"=>$kode_pos,"code"=>$code,"type_customer"=>$type_customer,"created_at"=>$created_at);
		$result['status'] = $this->db->insert('tb_customer', $data);
		if($result['status']){
			$result['id'] = $this->db->insert_id();
		}
		return $result;
	}
}

/* End of file Customer.php */
/* Location: ./application/models/Customer.php */