<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	function listKategori(){
		return $this->db->get('tb_kategori')->result();
	}
	function kategoriInc(){
		$sql = "SHOW TABLE STATUS LIKE 'tb_kategori'";
		return $this->db->query($sql)->result();
	}
	function addKategori($name){
		$created_at = date('Y-m-d h:i:s;');
		$data = array("name"=>$name,"created_at"=>$created_at);
		return $this->db->insert('tb_kategori',$data);
	}
	function detailKategori($id_kategori){
		$this->db->where('id_kategori',$id_kategori);
		return $this->db->get('tb_kategori')->result();
	}
	function editKategori($id_kategori,$name){
		$updated_at = date("Y-m-d h:i:s");
		$data = array("name"=>$name,"updated_at"=>$updated_at);
		$this->db->where('id_kategori',$id_kategori);
		return $this->db->update('tb_kategori',$data);
	}
	function deleteKategori($id_kategori){
		$this->db->where('id_kategori',$id_kategori);
		return $this->db->delete('tb_kategori');
	}
	function listKategoriCount(){
		$this->db->select('tb_kategori.*,if(count(tb_produk.id_produk) is null , 0, count(tb_produk.id_produk)) as "count"');
		$this->db->from('tb_kategori');
		$this->db->join('tb_produk', 'tb_produk on tb_kategori.id_kategori = tb_produk.id_kategori', 'left');
		$this->db->group_by('tb_kategori.id_kategori');
		return $this->db->get('')->result();
	}

}

/* End of file Kategori.php */
/* Location: ./application/models/Kategori.php */