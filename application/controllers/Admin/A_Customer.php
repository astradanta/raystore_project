<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class A_Customer extends CI_Controller {

	public function index()
	{
		$this->load->view('admin/static/header');
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/customer');
		$this->load->view('admin/static/footer');		
	}

}

/* End of file A_Customer.php */
/* Location: ./application/controllers/Admin/A_Customer.php */