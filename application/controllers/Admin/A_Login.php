<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class A_Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user','',true);
		
	}

	public function index()
	{
		$this->load->view('admin/login');
	}
	function auth(){
		$result["status"] = 0;
		$result["message"] = "misiing requiere field";
		if(isset($_POST['email']) && isset($_POST['password'])){
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$auth = $this->user->authCpanel($email);
			if(sizeof($auth)>0){
				$dataPassword = $this->encryption->decrypt($auth[0]->password);
				if($password == $dataPassword){
					$_SESSION['cpanel_id'] = $auth[0]->id_user;
					$_SESSION['cpanel_name'] = $auth[0]->name;
					$_SESSION['cpanel_idRole'] = $auth[0]->id_role;
					$_SESSION['cpanel_role'] = $auth[0]->description;
					$_SESSION['cpanel_email'] = $auth[0]->email;
					$result["status"] = 1;
					$result["message"] = "success login";
				} else {
					$result["message"] = "wrong password";
				}
			} else {
				$result["message"] = "you are not registered";
			}
		}
		echo json_encode($result);

	}
	function forget(){
		$result["status"] = 0;
		$result["message"] = "missing requiere field";
		if(isset($_POST['email'])){
			$email = $this->input->post('email');
			$auth = $this->user->authCpanel($email);
			if(sizeof($auth)>0){
				$password = $this->encryption->decrypt($auth[0]->password);
				$this->sendPassword($email,$password,$result);
			} else {
				$result["message"] = "you are not registered";
				echo json_encode($result);
			}
		} else {
			echo json_encode($result);
		}
		
	}
	function sendPassword($email,$password,$result) {
		$this->load->library('email');
		$htmlMessage = '<!DOCTYPE html> 
						<head> 
					    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
						</head>
						<style>
							.content {
							    max-width: 500px;
							    margin: auto;
							}
							.title{
								width: 60%;
							}

						</style>
						<body> 
							<div class="content">
<div bgcolor="#f9f9f9" style="font-family:'."'Open Sans'".',sans-serif">
    <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">
        
        <tbody><tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#643612" style="padding:10px 15px;font-size:14px">
                    <tbody><tr>
                        <td width="60%" align="left" style="padding:5px 0 0">
                            <img src="http://garage-d.online/raystore_project/assets/front/image/logo-ray.png" alt="Logo" width="50" class="CToWUd">
                        </td>
                        <td width="40%" align="right" style="padding:5px 0 0">
                            <span style="font-size:18px;font-weight:300;color:#ffffff">
                                Password
                            </span>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>        
        <tr>
            <td style="padding:25px 15px 10px">
                <table width="100%">
                    <tbody><tr>
                        <td>
                            <h1 style="margin:0;font-size:16px;font-weight:bold;line-height:24px;color:rgba(0,0,0,0.70)">Halo Customer,</h1>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="margin:0;font-size:16px;line-height:24px;color:rgba(0,0,0,0.70)">Password lama anda adalah </p>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
        <tr>
            <td style="padding:0 15px">
                
                <table width="100%" style="margin:15px 0;color:rgba(0,0,0,0.70);font-size:14px;border-collapse:collapse">
                    <tbody>
                        <tr>
                            <td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12)" width="40%">Password</td>
                            <td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold;color:#f5a84c" width="60%">
                                '.$password.'
                            </td>
                        </tr>                                                                                                                       
                    </tbody>
                </table>
                <div style="text-align:center">
                    <div style="text-align:left;font-size:13px;margin:30px 0 10px;color:#999999">
                        Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                    </div>
                </div>

            </td>
        </tr>
    </tbody></table>
<p>&nbsp;<br></p>
</div>

							</div>	
									
						</body>
</html>';
		$this->email->from('noreply@garage-d.online', 'Ray Store');
		$this->email->to($email);

		$this->email->set_mailtype('html');
		$this->email->subject('Forgot Password');
		$this->email->message($htmlMessage);
		
		$send = $this->email->send();
		if($send){
			$result["status"] = 1;
			$result["message"] = "your password is successfuly sended to your email";
			echo json_encode($result);
		}
		
	}
}

/* End of file A_Login.php */
/* Location: ./application/controllers/Admin/A_Login.php */