<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class A_Brand extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('brand','',true);
		$this->load->helper('string');
	}

	public function index()
	{
		$this->load->view('admin/static/header');
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/brand');
		$this->load->view('admin/static/footer');		
	}
	function list(){
		if(isset($_SESSION['cpanel_id'])){
			$data = $this->brand->listBrand();
			echo json_encode($data);
		}
	}

	function inc(){
		if(isset($_SESSION['cpanel_id'])){
			$data = $this->brand->brandInc();
			echo $data[0]->Auto_increment;
		}
	}
	function add(){
		$result['status'] = 0;
		$result["message"] = "failed";
		if(isset($_SESSION['cpanel_id'])){
			$icon = "";
			if($_FILES["icon"]["name"] != "") {
				
				$icon = $this->uploadIcon($icon);
			}
			$name = $this->input->post('name');
			$insert = $this->brand->addBrand($name,$icon);
			if($insert){
				$result['status'] = 1;
				$result["message"] = "success";
			}
		}
		echo json_encode($result);
	}
	function edit(){
		$result['status'] = 0;
		$result["message"] = "failed";
		if(isset($_SESSION['cpanel_id'])){
			$id_brand = $this->input->post('id_brand');
			$name = $this->input->post('name');
			$old = $this->brand->detailBrand($id_brand);
			$icon = $old[0]->icon;
			if($_FILES["icon"]["name"] != "") {
				
				$icon = $this->uploadIcon($icon);
			}
			$edit = $this->brand->editBrand($id_brand,$name,$icon);
			if($edit){
				$result['status'] = 1;
				$result["message"] = "success";
				if ($icon != $old[0]->icon){
					if(file_exists(FCPATH.$old[0]->cover)){
						
						unlink(FCPATH.$old[0]->cover);
					}
				}
			}
		}
		echo json_encode($result);
	}
	function detail(){
		if(isset($_SESSION['cpanel_id'])){
			$id_brand = $this->input->post('id_brand');
			$brand = $this->brand->detailBrand($id_brand);
			foreach ($brand as $key) {
				if($key->icon != ""){
					$key->icon = base_url().$key->icon;	
				} else {
					$key->icon = base_url().'assets/dist/img/empty.png';	
				}
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->updated_at = date('d M Y, h:i:s ',$time);
				}else{
					$key->updated_at = "";
				}
				$time = strtotime($key->created_at);
				$key->created_at = date('d M Y, h:i:s ',$time);
				
			}
			echo json_encode($brand);
		}
	}
	function delete(){
		$result['status'] = 1;
		$result['message'] = "success";		
		if(isset($_SESSION['cpanel_id'])){
			$id_brand = $this->input->post('id_brand');
			$delete = $this->brand->deleteBrand($id_brand);
			if($delete){
				$result['status'] = 1;
				$result['message'] = "success";
			}
		}
		echo json_encode($result);
	}
	function uploadIcon($url){
		$date = new DateTime();
		$config['file_name'] = $date->getTimestamp().random_string('alnum', 5);
		$config['upload_path'] = 'assets/img/brand/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']  = 2000;
		
		$this->load->library('upload', $config);
		
		 if ( ! $this->upload->do_upload('icon'))
                {
                        $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $url = $config['upload_path'].$data['upload_data']['orig_name'];
                }		
        return $url;
	}
}

