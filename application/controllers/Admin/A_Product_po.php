<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class A_Product_po extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk_po','',true);
		$this->load->model('brand','',true);
		$this->load->model('kategori','',true);
		$this->load->helper('string');
		$this->load->library('upload');
	}

	public function index()
	{
		$this->load->view('admin/static/header');
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/product_po');
		$this->load->view('admin/static/footer');
	}
	function list(){
		if(isset($_SESSION['cpanel_id'])){
			$data = $this->produk_po->listProdukPo();
			echo json_encode($data);
		}
	}

	function inc(){
		if(isset($_SESSION['cpanel_id'])){
			$data = $this->produk_po->produkPoInc();
			echo $data[0]->Auto_increment;
		}
	}
	function add(){
		$result['status'] = 0;
		$result["message"] = "failed";
		if(isset($_SESSION['cpanel_id'])){
			$id_kategori = $this->input->post('id_kategori');
			$id_brand = $this->input->post('id_brand');
			$nama_barang = $this->input->post('nama_barang');
			$harga_jual = str_replace(".", "", $this->input->post('harga_jual'));
			$harga_beli = str_replace(".", "", $this->input->post('harga_beli'));
			$deskripsi = $this->input->post('deskripsi');
			$cover = "";
			if($_FILES["cover"]["name"] != "") {
				
				$cover = $this->uploadIcon($cover);
			}
			$insert = $this->produk_po->addProdukPo($id_kategori,$id_brand,$nama_barang,$harga_beli,$harga_jual,$cover,$deskripsi);
			if($insert){
				$result['status'] = 1;
				$result["message"] = "success";

			}
		}
		echo json_encode($result);
	}
	function edit(){
		$result['status'] = 0;
		$result["message"] = "failed";
		if(isset($_SESSION['cpanel_id'])){
			$id_barang_po = $this->input->post('id_barang_po');
			$id_kategori = $this->input->post('id_kategori');
			$id_brand = $this->input->post('id_brand');
			$nama_produk = $this->input->post('nama_barang');
			$harga_jual = str_replace(".", "", $this->input->post('harga_jual'));
			$harga_beli = str_replace(".", "", $this->input->post('harga_beli'));
			$deskripsi = $this->input->post('deskripsi');
			$old = $this->produk_po->detailProdukPo($id_barang_po);
			$cover = $old[0]->cover;
			if($_FILES["cover"]["name"] != "") {
				
				$cover = $this->uploadIcon($cover);
			}
			$edit = $this->produk_po->editProdukPo($id_barang_po,$id_kategori,$id_brand,$nama_produk,$harga_beli,$harga_jual,$cover,$deskripsi);
			if($edit){
				$result['status'] = 1;
				$result["message"] = "success";
				if ($cover != $old[0]->cover){				
					if(file_exists(FCPATH.$old[0]->cover)){						
						unlink(FCPATH.$old[0]->cover);
					}
				}
			}
		}
		echo json_encode($result);
	}
	function detail(){
		if(isset($_SESSION['cpanel_id'])){
			$id_barang_po = $this->input->post('id_barang_po');
			$produk = $this->produk_po->detailProdukPo($id_barang_po);
			foreach ($produk as $key) {
				if($key->cover != ""){
					$key->cover = base_url().$key->cover;	
				} else {
					$key->cover = base_url().'assets/dist/img/empty.png';	
				}
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->updated_at = date('d M Y, h:i:s ',$time);
				}else{
					$key->updated_at = "";
				}
				$time = strtotime($key->created_at);
				$key->created_at = date('d M Y, h:i:s ',$time);
				
			}
			echo json_encode($produk);
		}
	}
	function delete(){
		$result['status'] = 1;
		$result['message'] = "success";		
		if(isset($_SESSION['cpanel_id'])){
			$id_barang_po = $this->input->post('id_barang_po');
			$barang = $this->produk_po->detailProdukPo($id_barang_po);			
			$delete = $this->produk_po->deleteProdukPo($id_barang_po);
			if($delete){
				$result['status'] = 1;
				$result['message'] = "success";
				if(($barang[0]->subimage != "")||($barang[0]->subimage != null)){
				$subimage = explode(",", $barang[0]->subimage);
				foreach ($subimage as $key ) {
					if(file_exists(FCPATH.$key)){						
						unlink(FCPATH.$key);
					}
				}
				}
			}
		}
		echo json_encode($result);
	}
	function uploadIcon($url){
		$date = new DateTime();
		$config['file_name'] = $date->getTimestamp().random_string('alnum', 5);
		$config['upload_path'] = 'assets/img/produk/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']  = 2000;
		
		$this->upload->initialize($config);
		
		 if ( ! $this->upload->do_upload('cover'))
                {
                        $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $url = $config['upload_path'].$data['upload_data']['orig_name'];
                }		
        return $url;
	}
	function checkImage(){
		if(isset($_SESSION['cpanel_id'])){
			$id_barang_po = $this->input->post('id_barang_po');
			$image = $this->produk_po->checkImagePo($id_barang_po);
			echo json_encode($image);
		}
	}
	function image(){
		if(isset($_SESSION['cpanel_id'])){
			$id_barang_po = $this->input->post('id_barang_po');
			if($_FILES['image1']['name'] != ""){
				$url = "";
				while ($url == "") {
					$url = $this->uploadImage("image1",$url);
					
				}
				if(($url != "failed")||($url != "")){
					$id_image = $this->input->post('indicator1');					
					if($id_image != 0){
						$image = $this->produk_po->detailImagePo($id_image);
						$editImage = $this->produk_po->editImagePo($id_image,$id_barang_po,$url);
						if($editImage){
							if(file_exists(FCPATH.$image[0]->image)){						
								unlink(FCPATH.$image[0]->image);
							}
						}
					} else {
						$insert = $this->produk_po->addImagePo($id_barang_po,$url);
					}
					
				}
			}
			if($_FILES['image2']['name'] != ""){
				$url = "";
				while ($url == "") {
					$url = $this->uploadImage("image2",$url);
				}
				if(($url != "failed")||($url != "")){
					$id_image = $this->input->post('indicator2');
					if($id_image != 0){
						$image = $this->produk_po->detailImagePo($id_image);
						$editImage = $this->produk_po->editImagePo($id_image,$id_barang_po,$url);
						if($editImage){
							if(file_exists(FCPATH.$image[0]->image)){						
								unlink(FCPATH.$image[0]->image);
							}
						}
					} else {
						$insert = $this->produk_po->addImagePo($id_barang_po,$url);
					}
					
				}
			}
			if($_FILES['image3']['name'] != ""){
				$url = "";
				while ($url == "") {
					$url = $this->uploadImage("image3",$url);
				}
				if(($url != "failed")||($url != "")){
					$id_image = $this->input->post('indicator3');
					if($id_image != 0){
						$image = $this->produk_po->detailImagePo($id_image);

						$editImage = $this->produk_po->editImagePo($id_image,$id_barang_po,$url);
						if($editImage){
							if(file_exists(FCPATH.$image[0]->image)){						
								unlink(FCPATH.$image[0]->image);
							}
						}
					} else {
						$insert = $this->produk_po->addImagePo($id_barang_po,$url);
					}
					
				}
			}
			$result['status'] = 1;
			$result['message'] = "success";
			echo json_encode($result);

		}
	}
	function uploadImage($file,$url){
		$date = new DateTime();
		$config['file_name'] = $date->getTimestamp().random_string('alnum', 5);
		$config['upload_path'] = 'assets/img/produk/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']  = 2000;
		
		$this->upload->initialize($config);
		
		 if ( ! $this->upload->do_upload($file))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $url = "failed";
                        return $url;
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $url = $config['upload_path'].$data['upload_data']['orig_name'];
                }		
        return $url;
	}
	function deleteImage(){
		$result['status'] = 0;
		if(isset($_SESSION['cpanel_id'])){
			$id_image = $this->input->post('id_image');
			$detailImage = $this->produk_po->detailImagePo($id_image);
			$delete = $this->produk_po->deleteImagePo($id_image);
			if($delete){
				$result['status'] = 1;
				if(file_exists(FCPATH.$detailImage[0]->image)){						
					unlink(FCPATH.$detailImage[0]->image);
				}
			}
		}
		echo json_encode($result);
	}
}

/* End of file A_Product_po.php */
/* Location: ./application/controllers/Admin/A_Product_po.php */