<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class A_Report extends CI_Controller {

	public function index()
	{
		$this->load->view('admin/static/header');
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/report');
		$this->load->view('admin/static/footer');			
	}

}
