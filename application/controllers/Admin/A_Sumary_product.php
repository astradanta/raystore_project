<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class A_Sumary_product extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk','',true);
		$this->load->model('stock','',true);
	}

	public function index()
	{
		$this->load->view('admin/static/header');
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/stock');
		$this->load->view('admin/static/footer');
	}
	function list(){
		if(isset($_SESSION['cpanel_id'])){
			$data = $this->stock->listStock();
			foreach ($data as $key ) {
				$key->typeLabel = ($key->type === "0") ? "Add" : "Remove";
			}
			echo json_encode($data);
		}
	}

	function inc(){
		if(isset($_SESSION['cpanel_id'])){
			$data = $this->stock->stockInc();
			echo $data[0]->Auto_increment;
		}
	}
	function add(){
		$result['status'] = 0;
		$result["message"] = "failed";
		if(isset($_SESSION['cpanel_id'])){
			$id_produk = $this->input->post('id_produk');
			$type = $this->input->post('type');
			$amount = str_replace(".", "", $this->input->post('amount'));
			$amount = ($type == '1') ? ("-".$amount) : $amount;
			$description = $this->input->post('description');
			$insert = $this->stock->addStock($id_produk,$type,$amount,$description);
			if($insert){
				$result['status'] = 1;
				$result["message"] = "success";
			}
		}
		echo json_encode($result);
	}
	function edit(){
		$result['status'] = 0;
		$result["message"] = "failed";
		if(isset($_SESSION['cpanel_id'])){
			$id_stock = $this->input->post('id_stock');
			$id_produk = $this->input->post('id_produk');
			$type = $this->input->post('type');
			$amount = str_replace(".", "", $this->input->post('amount'));
			$amount = ($type == '1') ? ("-".$amount) : $amount;
			$description = $this->input->post('description');
			$edit = $this->stock->editStock($id_stock,$id_produk,$type,$amount,$description);
			if($edit){
				$result['status'] = 1;
				$result["message"] = "success";
			}
		}
		echo json_encode($result);
	}
	function detail(){
		if(isset($_SESSION['cpanel_id'])){
			$id_stock = $this->input->post('id_stock');
			$stock = $this->stock->detailStock($id_stock);
			foreach ($stock as $key) {
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->updated_at = date('d M Y, h:i:s ',$time);
				}else{
					$key->updated_at = "";
				}
				$time = strtotime($key->created_at);
				$key->created_at = date('d M Y, h:i:s ',$time);
				$key->typeLabel = ($key->type === "0") ? "Add" : "Remove";
				
			}
			echo json_encode($stock);
		}
	}
	function delete(){
		$result['status'] = 1;
		$result['message'] = "success";		
		if(isset($_SESSION['cpanel_id'])){
			$id_stock = $this->input->post('id_stock');
			$delete = $this->stock->deleteStock($id_stock);
			if($delete){
				$result['status'] = 1;
				$result['message'] = "success";
			}
		}
		echo json_encode($result);
	}
}

/* End of file A_Sumary_product.php */
/* Location: ./application/controllers/Admin/A_Sumary_product.php */