<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class A_Product extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk','',true);
		$this->load->model('brand','',true);
		$this->load->model('kategori','',true);
		$this->load->helper('string');
		$this->load->library('upload');
	}

	public function index()
	{
		$this->load->view('admin/static/header');
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/product');
		$this->load->view('admin/static/footer');
	}
	function list(){
		if(isset($_SESSION['cpanel_id'])){
			$data = $this->produk->listProduk();
			echo json_encode($data);
		}
	}

	function inc(){
		if(isset($_SESSION['cpanel_id'])){
			$data = $this->produk->produkInc();
			echo $data[0]->Auto_increment;
		}
	}
	function add(){
		$result['status'] = 0;
		$result["message"] = "failed";
		if(isset($_SESSION['cpanel_id'])){
			$bestseller = (isset($_POST['bestseller']) ? 1 : 0 );
			$id_kategori = $this->input->post('id_kategori');
			$id_brand = $this->input->post('id_brand');
			$nama_produk = $this->input->post('nama_produk');
			$harga_jual = str_replace(".", "", $this->input->post('harga_jual'));
			$harga_beli = str_replace(".", "", $this->input->post('harga_beli'));
			$stok = str_replace(".", "", $this->input->post('stok'));
			$deskripsi = $this->input->post('deskripsi');
			$cover = "";
			if($_FILES["cover"]["name"] != "") {
				
				$cover = $this->uploadIcon($cover);
			}
			$insert = $this->produk->addProduk($id_kategori,$id_brand,$nama_produk,$harga_beli,$harga_jual,$cover,$deskripsi,$bestseller);
			if($insert['status']){
				$id_produk = $insert['id'];
				$stock = $this->produk->firstStock($id_produk,$stok);
				if($stock){
					$result['status'] = 1;
					$result["message"] = "success";
				}

			}
		}
		echo json_encode($result);
	}
	function edit(){
		$result['status'] = 0;
		$result["message"] = "failed";
		if(isset($_SESSION['cpanel_id'])){
			$bestseller = isset($_POST['bestseller']) ? 1 : 0;
			$id_produk = $this->input->post('id_produk');
			$id_kategori = $this->input->post('id_kategori');
			$id_brand = $this->input->post('id_brand');
			$nama_produk = $this->input->post('nama_produk');
			$harga_jual = str_replace(".", "", $this->input->post('harga_jual'));
			$harga_beli = str_replace(".", "", $this->input->post('harga_beli'));
			$stok = $this->input->post('stok');
			$deskripsi = $this->input->post('deskripsi');
			$old = $this->produk->detailProduk($id_produk);
			$cover = $old[0]->cover;
			if($_FILES["cover"]["name"] != "") {
				
				$cover = $this->uploadIcon($cover);
			}
			$edit = $this->produk->editProduk($id_produk,$id_kategori,$id_brand,$nama_produk,$harga_beli,$harga_jual,$cover,$deskripsi,$bestseller);
			if($edit){
				$result['status'] = 1;
				$result["message"] = "success";
				if ($cover != $old[0]->cover){				
					if(file_exists(FCPATH.$old[0]->cover)){						
						unlink(FCPATH.$old[0]->cover);
					}
				}
			}
		}
		echo json_encode($result);
	}
	function detail(){
		if(isset($_SESSION['cpanel_id'])){
			$id_produk = $this->input->post('id_produk');
			$produk = $this->produk->detailProduk($id_produk);
			foreach ($produk as $key) {
				if($key->cover != ""){
					$key->cover = base_url().$key->cover;	
				} else {
					$key->cover = base_url().'assets/dist/img/empty.png';	
				}
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->updated_at = date('d M Y, h:i:s ',$time);
				}else{
					$key->updated_at = "";
				}
				$time = strtotime($key->created_at);
				$key->created_at = date('d M Y, h:i:s ',$time);
				
			}
			echo json_encode($produk);
		}
	}
	function delete(){
		$result['status'] = 1;
		$result['message'] = "success";		
		if(isset($_SESSION['cpanel_id'])){
			$id_produk = $this->input->post('id_produk');
			$produk = $this->produk->detailProduk($id_produk);
			$delete = $this->produk->deleteProduk($id_produk);
			if($delete){
				$result['status'] = 1;
				$result['message'] = "success";
				if(($produk[0]->subimage != "")||($produk[0]->subimage != null)){
				$subimage = explode(",", $produk[0]->subimage);
				foreach ($subimage as $key ) {
					if(file_exists(FCPATH.$key)){						
						unlink(FCPATH.$key);
					}
				}
				}
			}
		}
		echo json_encode($result);
	}
	function uploadIcon($url){
		$date = new DateTime();
		$config['file_name'] = $date->getTimestamp().random_string('alnum', 5);
		$config['upload_path'] = 'assets/img/produk/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']  = 2000;
		
		$this->upload->initialize($config);
		
		 if ( ! $this->upload->do_upload('cover'))
                {
                        $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $url = $config['upload_path'].$data['upload_data']['orig_name'];
                }		
        return $url;
	}
	function checkImage(){
		if(isset($_SESSION['cpanel_id'])){
			$id_produk = $this->input->post('id_produk');
			$image = $this->produk->checkImage($id_produk);
			echo json_encode($image);
		}
	}
	function image(){
		if(isset($_SESSION['cpanel_id'])){
			$id_produk = $this->input->post('id_produk');
			if($_FILES['image1']['name'] != ""){
				$url = "";
				while ($url == "") {
					$url = $this->uploadImage("image1",$url);
					
				}
				if(($url != "failed")||($url != "")){
					$id_image = $this->input->post('indicator1');					
					if($id_image != 0){
						$image = $this->produk->detailImage($id_image);
						$editImage = $this->produk->editImage($id_image,$id_produk,$url);
						if($editImage){
							if(file_exists(FCPATH.$image[0]->image)){						
								unlink(FCPATH.$image[0]->image);
							}
						}
					} else {
						$insert = $this->produk->addImage($id_produk,$url);
					}
					
				}
			}
			if($_FILES['image2']['name'] != ""){
				$url = "";
				while ($url == "") {
					$url = $this->uploadImage("image2",$url);
				}
				if(($url != "failed")||($url != "")){
					$id_image = $this->input->post('indicator2');
					if($id_image != 0){
						$image = $this->produk->detailImage($id_image);
						$editImage = $this->produk->editImage($id_image,$id_produk,$url);
						if($editImage){
							if(file_exists(FCPATH.$image[0]->image)){						
								unlink(FCPATH.$image[0]->image);
							}
						}
					} else {
						$insert = $this->produk->addImage($id_produk,$url);
					}
					
				}
			}
			if($_FILES['image3']['name'] != ""){
				$url = "";
				while ($url == "") {
					$url = $this->uploadImage("image3",$url);
				}
				if(($url != "failed")||($url != "")){
					$id_image = $this->input->post('indicator3');
					if($id_image != 0){
						$image = $this->produk->detailImage($id_image);

						$editImage = $this->produk->editImage($id_image,$id_produk,$url);
						if($editImage){
							if(file_exists(FCPATH.$image[0]->image)){						
								unlink(FCPATH.$image[0]->image);
							}
						}
					} else {
						$insert = $this->produk->addImage($id_produk,$url);
					}
					
				}
			}
			$result['status'] = 1;
			$result['message'] = "success";
			echo json_encode($result);

		}
	}
	function uploadImage($file,$url){
		$date = new DateTime();
		$config['file_name'] = $date->getTimestamp().random_string('alnum', 5);
		$config['upload_path'] = 'assets/img/produk/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']  = 2000;
		
		$this->upload->initialize($config);
		
		 if ( ! $this->upload->do_upload($file))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $url = "failed";
                        return $url;
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $url = $config['upload_path'].$data['upload_data']['orig_name'];
                }		
        return $url;
	}
	function deleteImage(){
		$result['status'] = 0;
		if(isset($_SESSION['cpanel_id'])){
			$id_image = $this->input->post('id_image');
			$detailImage = $this->produk->detailImage($id_image);
			$delete = $this->produk->deleteImage($id_image);
			if($delete){
				$result['status'] = 1;
				if(file_exists(FCPATH.$detailImage[0]->image)){						
					unlink(FCPATH.$detailImage[0]->image);
				}
			}
		}
		echo json_encode($result);
	}
}

/* End of file A_Product.php */
/* Location: ./application/controllers/Admin/A_Product.php */
