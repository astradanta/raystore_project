<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class A_Dashboard extends CI_Controller {

	public function index()
	{
		$this->load->view('admin/static/header');
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/dashboard');
		$this->load->view('admin/static/footer');
	}


}

/* End of file A_Dashboard.php */
/* Location: ./application/controllers/Admin/A_Dashboard.php */