<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class A_Kategori extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('kategori','',true);
	}
	public function index()
	{
		$this->load->view('admin/static/header');
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/kategori');
		$this->load->view('admin/static/footer');	
	}
	function list(){
		if(isset($_SESSION['cpanel_id'])){
			$data = $this->kategori->listKategori();
			echo json_encode($data);
		}
	}

	function inc(){
		if(isset($_SESSION['cpanel_id'])){
			$data = $this->kategori->kategoriInc();
			echo $data[0]->Auto_increment;
		}
	}
	function add(){
		$result['status'] = 0;
		$result["message"] = "failed";
		if(isset($_SESSION['cpanel_id'])){
			$name = $this->input->post('name');
			$insert = $this->kategori->addKategori($name);
			if($insert){
				$result['status'] = 1;
				$result["message"] = "success";
			}
		}
		echo json_encode($result);
	}
	function edit(){
		$result['status'] = 0;
		$result["message"] = "failed";
		if(isset($_SESSION['cpanel_id'])){
			$id_kategori = $this->input->post('id_kategori');
			$name = $this->input->post('name');
			$edit = $this->kategori->editKategori($id_kategori,$name);
			if($edit){
				$result['status'] = 1;
				$result["message"] = "success";
			}
		}
		echo json_encode($result);
	}
	function detail(){
		if(isset($_SESSION['cpanel_id'])){
			$id_kategori = $this->input->post('id_kategori');
			$kategori = $this->kategori->detailKategori($id_kategori);
			foreach ($kategori as $key) {
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->updated_at = date('d M Y, h:i:s ',$time);
				}else{
					$key->updated_at = "";
				}
				$time = strtotime($key->created_at);
				$key->created_at = date('d M Y, h:i:s ',$time);
				
			}
			echo json_encode($kategori);
		}
	}
	function delete(){
		$result['status'] = 1;
		$result['message'] = "success";		
		if(isset($_SESSION['cpanel_id'])){
			$id_kategori = $this->input->post('id_kategori');
			$delete = $this->kategori->deleteKategori($id_kategori);
			if($delete){
				$result['status'] = 1;
				$result['message'] = "success";
			}
		}
		echo json_encode($result);
	}
}

/* End of file A_Kategori.php */
/* Location: ./application/controllers/Admin/A_Kategori.php */