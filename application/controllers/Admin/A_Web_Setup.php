<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class A_Web_Setup extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('web','',true);
		$this->load->library('upload');
		$this->load->helper('string');
	}

	public function index()
	{
		
	}
	public function slider(){
		$this->load->view('admin/static/header');
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/slider');
		$this->load->view('admin/static/footer');
	}
	function slider_action(){
		$action = $this->uri->segment(3);
		switch ($action) {
			case 'add':
				$this->slider_add();
				break;
			case 'list':
				$this->slider_list();
				break;
			case 'detail':
				$this->slider_detail();
				break;
			case 'edit':
				$this->slider_edit();
				break;
			case 'delete':
				$this->slider_delete();
				break;
			default:				
				break;
		}
	}
	function slider_add(){
		$result['status'] = 0;
		$result["message"] = "failed";
		if(isset($_SESSION['cpanel_id'])){
			$url = "";
			if($_FILES["icon"]["name"] != "") {
				
				$url = $this->uploadImage("icon",$url,'assets/img/slider/');
			}
			$link = $this->input->post('link');
			if(($url != "")&&($url!="failed")){
				$insert = $this->web->addSlider($url,$link);
				if($insert){
					$result['status'] = 1;
					$result["message"] = "success";
				}
			}
			
		}
		echo json_encode($result);
	}
	function slider_delete(){
		$result['status'] = 1;
		$result['message'] = "success";		
		if(isset($_SESSION['cpanel_id'])){
			$id_slider = $this->input->post('id_slider');
			$slider = $this->web->detailSlider($id_slider);
			$delete = $this->web->deleteSlider($id_slider);
			if($delete){
				$result['status'] = 1;
				$result['message'] = "success";
				if(file_exists(FCPATH.$slider[0]->image)){						
					unlink(FCPATH.$slider[0]->image);
				}
			}
		}
		echo json_encode($result);

	}
	function slider_list(){
		if(isset($_SESSION['cpanel_id'])){
			$data = $this->web->listSlider();
			echo json_encode($data);
		}
	}
	function slider_detail(){
		if(isset($_SESSION['cpanel_id'])){
			$id_slider = $this->input->post('id_slider');
			$slider = $this->web->detailSlider($id_slider);
			foreach ($slider as $key) {
				if($key->image != ""){
					$key->image = base_url().$key->image;	
				} else {
					$key->image = base_url().'assets/dist/img/empty.png';	
				}
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->updated_at = date('d M Y, h:i:s ',$time);
				}else{
					$key->updated_at = "";
				}
				$time = strtotime($key->created_at);
				$key->created_at = date('d M Y, h:i:s ',$time);
				
			}
			echo json_encode($slider);
		}
	}
	function slider_edit(){
		$result['status'] = 0;
		$result["message"] = "failed";
		if(isset($_SESSION['cpanel_id'])){
			$id_slider = $this->input->post('id_slider');
			$link = $this->input->post('link');
			$old = $this->web->detailSlider($id_slider);
			$url = $old[0]->image;
			if($_FILES["icon"]["name"] != "") {
				
				$url = $this->uploadImage("icon",$url,'assets/img/slider/');
			}
			$edit = $this->web->editSlider($id_slider,$url,$link);
			if($edit){
				$result['status'] = 1;
				$result["message"] = "success";
				if ($url != $old[0]->image){
					if(file_exists(FCPATH.$old[0]->image)){
						
						unlink(FCPATH.$old[0]->image);
					}
				}
			}
		}
		echo json_encode($result);
	}

	function uploadImage($file,$url,$path){
		$date = new DateTime();
		$config['file_name'] = $date->getTimestamp().random_string('alnum', 5);
		$config['upload_path'] = $path;
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']  = 2000;
		
		$this->upload->initialize($config);
		
		 if ( ! $this->upload->do_upload($file))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $url = "failed";
                        return $url;
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $url = $config['upload_path'].$data['upload_data']['orig_name'];
                }		
        return $url;
	}
	public function banner(){
		$this->load->view('admin/static/header');
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/banner');
		$this->load->view('admin/static/footer');
	}
	function banner_action(){
		$action = $this->uri->segment(3);
		switch ($action) {
			case 'list':
				$this->banner_list();
				break;
			case 'detail':
				$this->banner_detail();
				break;
			case 'edit':
				$this->banner_edit();
				break;
			default:				
				break;
		}
	}
	function banner_list(){
		if(isset($_SESSION['cpanel_id'])){
			$data = $this->web->listBanner();
			echo json_encode($data);
		}
	}
	function banner_edit(){
		$result['status'] = 0;
		$result["message"] = "failed";
		if(isset($_SESSION['cpanel_id'])){
			$id_banner = $this->input->post('id_banner');
			$link = $this->input->post('link');
			$old = $this->web->detailBanner($id_banner);
			$url = $old[0]->image;
			if($_FILES["icon"]["name"] != "") {
				
				$url = $this->uploadImage("icon",$url,'assets/img/banner/');
			}
			$edit = $this->web->editBanner($id_banner,$url,$link);
			if($edit){
				$result['status'] = 1;
				$result["message"] = "success";
				if ($url != $old[0]->image){
					if(file_exists(FCPATH.$old[0]->image)){
						
						unlink(FCPATH.$old[0]->image);
					}
				}
			}
		}
		echo json_encode($result);
	}
	function banner_detail(){
		if(isset($_SESSION['cpanel_id'])){
			$id_banner = $this->input->post('id_banner');
			$banner = $this->web->detailBanner($id_banner);
			foreach ($banner as $key) {
				if($key->image != ""){
					$key->image = base_url().$key->image;	
				} else {
					$key->image = base_url().'assets/dist/img/empty.png';	
				}
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->updated_at = date('d M Y, h:i:s ',$time);
				}else{
					$key->updated_at = "";
				}
				
			}
			echo json_encode($banner);
		}
	}
}

/* End of file A_Web_Setup.php */
/* Location: ./application/controllers/Admin/A_Web_Setup.php */