<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_Dashboard extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("web","",true);
	}

	public function index()
	{
		$this->load->view('front/static/header');
		$this->load->view('front/dashboard');
		$this->load->view('front/static/footer');		
	}

}
