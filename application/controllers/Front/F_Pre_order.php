<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_Pre_order extends MY_Controller {

	public function index()
	{
		if($this->uri->segment(2)==="load"){
			$this->load();
		}else if ($this->uri->segment(2)==="pagination"){
			$this->pagination();
		} else { 
			$this->load->view('front/static/header');
			$this->load->view('front/pre_order');
			$this->load->view('front/static/footer');
		} 
	}
	function load(){
		$sort = $this->input->post('sort');
		$limit = $this->input->post('limit');
		$page = $this->input->post('page');
		$sort = ($sort === "") ? null : $sort;
		$produk = $this->produk_po->poFront($sort, $limit, $page);
		echo json_encode($produk);
	}
	function pagination(){
		$limit = $this->input->post('limit');
		$page =  $this->produk_po->poPagination($limit);
		echo json_encode($page);
	}
}

/* End of file F_Pre_order.php */
/* Location: ./application/controllers/Front/F_Pre_order.php */