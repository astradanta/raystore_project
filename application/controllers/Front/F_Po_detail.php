<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_Po_detail extends MY_Controller {

	public function index()
	{
		$po = $this->produk_po->detailProdukPo($this->uri->segment(2));
		if(sizeof($po)>0){
				$data['po'] = $po[0];
				$this->load->view('front/static/header',$data);
				$this->load->view('front/po_detail');
				$this->load->view('front/static/footer');
		} else {
			redirect(base_url()."po_detail",'refresh');
		}
	}

}

/* End of file F_Po_detail.php */
/* Location: ./application/controllers/Front/F_Po_detail.php */