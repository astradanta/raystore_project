<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_Checkout extends MY_Controller {

	public function index()
	{
		$this->load->view('front/static/header');
		$this->load->view('front/checkout');
		$this->load->view('front/static/footer');
	}

}

/* End of file F_Checkout.php */
/* Location: ./application/controllers/Front/F_Checkout.php */