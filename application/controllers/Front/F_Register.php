<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_Register extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user','',true);
		$this->load->model('customer','',true);
	}

	public function index()
	{
		$this->load->view('front/static/header');
		$this->load->view('front/register');
		$this->load->view('front/static/footer');
	}
	function customer(){
		$result['status'] = 0;
		$result['message'] = 'Register failed';
		$nama_depan = $this->input->post('firstname');
		$nama_belakang = $this->input->post('lastname');
		$nama = $nama_depan." ".$nama_belakang;
		$email = $this->input->post('email');
		$no_hp = $this->input->post('no_hp');
		$alamat = $this->input->post('address');
		$kota = $this->input->post('city');
		$kode_pos = $this->input->post('kode_pos');
		$code = $this->input->post('code');
		$type_customer = $this->input->post('type_customer');
		$password = $this->encryption->encrypt($this->input->post('password'));
		$customer = $this->customer->addCustomer($nama,$email,$no_hp,$alamat,$kota,$kode_pos,$code,$type_customer);
		if($customer['status']){
			$id_customer = $customer['id'];
			$user = $this->user->addCustomerLogin($nama,$id_customer,$email,$password);
			if($user){
				$result['status'] = 1;
				$result['message'] = 'Register success';
				$_SESSION['customer_id'] = $id_customer;
				$_SESSION['customer_name'] = $nama;
			}
		}
		echo json_encode($result);
	}

}

/* End of file F_Register.php */
/* Location: ./application/controllers/Front/F_Register.php */