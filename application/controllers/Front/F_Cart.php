<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_Cart extends MY_Controller {

	public function index()
	{
		$this->load->view('front/static/header');
		$this->load->view('front/cart');
		$this->load->view('front/static/footer');
	}

}

/* End of file F_Cart.php */
/* Location: ./application/controllers/Front/F_Cart.php */