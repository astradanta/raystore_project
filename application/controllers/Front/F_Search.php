<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_Search extends MY_Controller {

	public function index()
	{
		if($this->uri->segment(2)==="load"){
			$this->load();
		}else if ($this->uri->segment(2)==="pagination"){
			$this->pagination();
		} else {
				$this->load->view('front/static/header');
				$this->load->view('front/search');
				$this->load->view('front/static/footer');
		}
		
	}
	function load(){
		$key = (empty($this->input->post('key')) ? '' :$this->input->post('key'));
		$id_kategori = $this->input->post('id_kategori');
		$sort = $this->input->post('sort');
		$limit = $this->input->post('limit');
		$page = $this->input->post('page');
		$sort = ($sort === "") ? null : $sort;
		$produk = $this->produk->produkSearch($key,$id_kategori,$sort,$limit,$page);
		echo json_encode($produk);
	}
	function pagination(){
		$key = (empty($this->input->post('key')) ? '' :$this->input->post('key'));
		$id_kategori = $this->input->post('id_kategori');
		$limit = $this->input->post('limit');
		$page =  $this->produk->pagginationSearch($key,$id_kategori,$limit);
		echo json_encode($page);
	}
}

/* End of file F_Search.php */
/* Location: ./application/controllers/Front/F_Search.php */