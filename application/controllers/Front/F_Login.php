<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_Login extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user','',true);
	}

	public function index()
	{
		$this->load->view('front/static/header');
		$this->load->view('front/login');
		$this->load->view('front/static/footer');
	}
	function logout(){
		unset($_SESSION['customer_id']);
		unset($_SESSION['customer_name']);
		redirect(base_url(),'refresh');
	}
	function auth(){
		$result["status"] = 0;
		$result["message"] = "misiing requiere field";
		if(isset($_POST['email']) && isset($_POST['password'])){
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$auth = $this->user->auth($email);
			if(sizeof($auth)>0){
				$dataPassword = $this->encryption->decrypt($auth[0]->password);
				if($password == $dataPassword){
					$_SESSION['customer_id'] = $auth[0]->id_user;
					$_SESSION['customer_name'] = $auth[0]->name;				
					$result["status"] = 1;
					$result["message"] = "success login";
				} else {
					$result["message"] = "wrong password";
				}
			} else {
				$result["message"] = "you are not registered";
			}
		}
		echo json_encode($result);

	}

}

/* End of file F_Login.php */
/* Location: ./application/controllers/Front/F_Login.php */