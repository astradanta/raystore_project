<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_About extends MY_Controller {

	public function index()
	{
		$this->load->view('front/static/header');
		$this->load->view('front/about');
		$this->load->view('front/static/footer');
	}

}

/* End of file F_About.php */
/* Location: ./application/controllers/Front/F_About.php */