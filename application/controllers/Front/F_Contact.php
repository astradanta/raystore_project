<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_Contact extends MY_Controller {

	public function index()
	{
		$this->load->view('front/static/header');
		$this->load->view('front/contact');
		$this->load->view('front/static/footer');
	}

}

/* End of file F_Contact.php */
/* Location: ./application/controllers/Front/F_Contact.php */