<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_Product_detail extends MY_Controller {

	public function index()
	{
		$produk = $this->produk->detailProdukFront($this->uri->segment(2));
		if(sizeof($produk)>0){
				$data['produk'] = $produk[0];
				$this->load->view('front/static/header',$data);
				$this->load->view('front/product_detail');
				$this->load->view('front/static/footer');
		} else {
			redirect(base_url()."product_detail",'refresh');
		}

	}

}

/* End of file F_Product_detail.php */
/* Location: ./application/controllers/Front/F_Product_detail.php */