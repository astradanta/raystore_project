<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_Account extends MY_Controller {

	public function index()
	{
		$this->load->view('front/static/header');
		$this->load->view('front/account');
		$this->load->view('front/static/footer');
	}

}

/* End of file F_Account.php */
/* Location: ./application/controllers/Front/F_Account.php */