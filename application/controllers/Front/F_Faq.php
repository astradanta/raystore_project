<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_Faq extends MY_Controller {

	public function index()
	{
		$this->load->view('front/static/header');
		$this->load->view('front/faq');
		$this->load->view('front/static/footer');
	}

}

/* End of file F_Faq.php */
/* Location: ./application/controllers/Front/F_Faq.php */