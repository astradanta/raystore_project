<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_Brand extends MY_Controller {

	public function index()
	{
		if($this->uri->segment(2)==="load"){
			$this->load();
		}else if ($this->uri->segment(2)==="pagination"){
			$this->pagination();
		} else {
			if(is_numeric($this->uri->segment(2))||($this->uri->segment(2) === "all")){
				$this->load->view('front/static/header');
				$this->load->view('front/brand');
				$this->load->view('front/static/footer');
			} else {
				redirect(base_url()."ready_stock/all",'refresh');
			}
		}
		
	}
	function load(){
		$id_kategori = $this->input->post('id_kategori');
		$sort = $this->input->post('sort');
		$limit = $this->input->post('limit');
		$page = $this->input->post('page');
		$sort = ($sort === "") ? null : $sort;
		$produk = $this->produk->produkByBrand($id_kategori,$sort,$limit,$page);
		echo json_encode($produk);
	}
	function pagination(){
		$id_kategori = $this->input->post('id_kategori');
		$limit = $this->input->post('limit');
		$page =  $this->produk->pagginationBrand($id_kategori,$limit);
		echo json_encode($page);
	}

}

/* End of file F_Brand.php */
/* Location: ./application/controllers/Front/F_Brand.php */