<div class="content-wrapper" id="content">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        DATA BRAND / MERK
        <small>Master Data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master Data</a></li>
        Brand</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- /.col -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          
            <a class="btn btn-block btn-social btn-google" id="addBtn">
                <i class="fa fa-plus"></i> Tambah Brand
            </a>
          <br>
          <!-- /.info-box -->
        </div>
      </div>
      <!-- /.col -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">

              <h3 class="box-title">LIST DATA</h3>
            </div>
            <!-- /.box-header -->
             <div class="box-body">
                  <div class="row" id="contentInvoice">
                       <div class="col-md-12">
                         <table id="tableBrand" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>Kode</th>
                              <th>Nama Brand</th>                 
                              <th>#</th>
                            </tr>
                          </thead>
                          <tbody id="listView">
                                <tr>
                                  <td>01</td>
                                  <td>Nike</td>
                                 
                                  <td class="text-center">
                                    <span data-toggle="tooltip" data-placement="top" title="Delete Item"><button type="button" class="btn btn-danger" ><i class="fa fa-trash"></i>
                                    </button></span>
                                    <span data-toggle="tooltip" data-placement="top" title="Edit"><button type="button" class="btn btn-info"><i class="fa fa-pencil"></i>
                                    
                                  </td>
                                </tr>
                          </tbody>
                        </table>                       
                      </div>
                </div>
                <!-- /.box-body -->
              </div>  
          </div>
          <!-- /.box -->
        </div>
      </div>

      <!-- MODALS -->
      <div class="modal fade"  id="inputModal">
          <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Input Brand</h4>
              </div>
              <form role="form" action="<?php echo(base_url()) ?>cpanel/brand/add" method="post" enctype="multipart/form-data"  id="inputForm">
              <div class="modal-body" id="inputContent">
                <div class="row">

                  <div class="col-md-12">

                    <div class="box box-danger">
                      
                      <!-- /.box-header -->
                      <div class="box-body">
                        
                          <!-- text input -->
                          <div class="form-group text-center">
                            <img src="<?php echo(base_url()) ?>assets/dist/img/empty.png" width="200" id="displayIcon" height="200" style="cursor: pointer;">
                            <input type="file" name="icon" id="icon" style="display: none;" accept="image/*">
                            <h4 id="iconValidate" style="color: red;display: none;">You Must Insert Photo</h4>
                          </div>
                          <div class="form-group">
                            <label>Kode</label>
                            <input type="text" class="form-control" placeholder="08323" disabled="" id="kode">
                          </div>
                          
                          <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" placeholder="Enter ..." id="name" name="name" required="">
                          </div>                                                                          
                      </div>
                      <!-- /.box-body -->
                    </div>
                  </div>
                
                 
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <div class="modal fade"  id="editModal">
          <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Brand</h4>
              </div>
              <form role="form" action="<?php echo(base_url()) ?>cpanel/brand/edit" method="post" enctype="multipart/form-data"  id="editForm">
              <div class="modal-body" id="editContent">
                <div class="row">
                  <input type="hidden" name="id_brand" id="id_brand">
                  <div class="col-md-12">

                    <div class="box box-danger">
                      
                      <!-- /.box-header -->
                      <div class="box-body">
                        
                          <!-- text input -->
                          <div class="form-group text-center">
                            <img src="<?php echo(base_url()) ?>assets/dist/img/empty.png" width="200" id="displayEditIcon" height="200" style="cursor: pointer;">
                            <input type="file" name="icon" id="iconEdit" style="display: none;" accept="image/*">
                            <h4 id="iconEditValidate" style="color: red;display: none;">You Must Insert Photo</h4>
                          </div>
                          <div class="form-group">
                            <label>Kode</label>
                            <input type="text" class="form-control" placeholder="08323" disabled="" id="kodeEdit">
                          </div>
                          
                          <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" placeholder="Enter ..." id="nameEdit" name="name" required="">
                          </div>  
                      </div>
                      <!-- /.box-body -->
                    </div>
                  </div>
                
                 
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <div class="modal fade"  id="detailModal">
          <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detail Brand</h4>
              </div>
              <div class="modal-body" id="inputContent">
                <div class="row">
                  <input type="hidden" name="id_brand" id="id_brand">
                  <div class="col-md-12">

                    <div class="box box-danger">
                      
                      <!-- /.box-header -->
                      <div class="box-body">
                        
                          <!-- text input -->
                          <div class="form-group text-center">
                            <img src="<?php echo(base_url()) ?>assets/dist/img/empty.png" width="200" id="displayDetailIcon" height="200" style="cursor: pointer;">
                          </div>
                          <div class="form-group">
                            <label>Kode</label>
                            <input type="text" class="form-control" placeholder="08323" readonly="" id="kodeDetail">
                          </div>
                          
                          <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" placeholder="Enter ..." id="nameDetail" name="name" readonly="">
                          </div>
                          <div class="form-group">
                            <label>Create At</label>
                            <input type="text" class="form-control"  id="createDetail" name="email" readonly="">
                          </div>
                          <div class="form-group">
                            <label>Update At</label>
                            <input type="text" class="form-control"  id="updateDetail" name="email" readonly="">
                          </div>                            
                      </div>
                      <!-- /.box-body -->
                    </div>
                  </div>
                
                 
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
               
              </div>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Are you sure delete this item?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Yes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->