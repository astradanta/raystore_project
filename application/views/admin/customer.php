<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        USER SETTING 
        <small>Account Management</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        
        <li class="active">User Setting</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- /.col -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          
          <a class="btn btn-block btn-social btn-google" data-toggle="modal" data-target="#form-add-material">
                <i class="fa fa-plus"></i> Tambah Customer
            </a>
          <br>
          <!-- /.info-box -->
        </div>
      </div>
      <!-- /.col -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">

              <h3 class="box-title">Data Customer</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Kode</th>
                  <th>Nama</th>                 
                  <th>Type Customer</th>
                  <th>Email</th>
                  <th>No Hp</th>
                  <th>Alamat</th>
                  <th>Kota</th>
                  <th>Kode Pos</th>
                  <th>Negara</th>
                  <th>#</th>
                </tr>
                <tr>
                  <td>183</td>
                  <td>Doni Agustina</td>
                  <td>Reseller</td>
                  <td>doniagustina27@gmail.com</td>
                  <td>086872126281</td>
                  <td>Singaraja</td>
                  <td>Singaraja</td>
                  <td>80721</td>
                  <td>Indonesia</td>
                  
                  <td class="text-center">
                    <span data-toggle="tooltip" data-placement="top" title="Delete Item"><button type="button" class="btn btn-danger" ><i class="fa fa-trash"></i>
                    </button></span>
                    <span data-toggle="tooltip" data-placement="top" title="Edit"><button type="button" class="btn btn-info"><i class="fa fa-pencil"></i>
                    </button></span>
                    
                  </td>
                </tr>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

      <!-- MODALS -->
      <div class="modal fade" id="form-add-material">
          <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Input Customer</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">

                    <div class="box box-danger">
                      
                      <!-- /.box-header -->
                      <div class="box-body">
                        <form role="form">
                          <!-- text input -->
                          <div class="form-group">
                            <label>Kode</label>
                            <input type="text" class="form-control" placeholder="08323" disabled="">
                          </div>
                          
                          <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" placeholder="Enter ...">
                          </div>
                         
                          <!-- select -->
                          <div class="form-group">
                            <label>Type Customer</label>
                            <select class="form-control">
                              <option>Reseller</option>
                              <option>Customer Umum</option>
                             
                             
                            </select>
                          </div>
                        
                          <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" placeholder="email">
                          </div>
                          <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" class="form-control" placeholder="alamat">
                          </div>
                          <div class="form-group">
                            <label>Kota</label>
                            <input type="text" class="form-control" placeholder="">
                          </div>
                          <div class="form-group">
                            <label>Kode Pos</label>
                            <input type="text" class="form-control" placeholder="">
                          </div>
                          <div class="form-group">
                            <label>Negara</label>
                            <input type="text" class="form-control" placeholder="">
                          </div>
                      
                        </form>
                      </div>
                      <!-- /.box-body -->
                    </div>
                  </div>
                
                 
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    </section>
    <!-- /.content -->
  </div>