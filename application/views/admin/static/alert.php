
<div style="padding: 10px;" id="alertContainer">
	
</div>
<div id="warning" style="display: none;">
	<div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning"></i> Warning !!</h4>
                <p id="warningText"> Warning alert preview. This alert is dismissable. </p>
    </div>
</div>
<div id="error" style="display: none;">
	<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <p id="errorText"> Warning alert preview. This alert is dismissable. </p>
    </div>
</div>
<div id="success" style="display: none;">
	<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                <p id="successText"> Warning alert preview. This alert is dismissable. </p>
    </div>
</div>