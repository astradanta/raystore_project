 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Ganeshcom Studio</a>.</strong> All rights
    reserved.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo(base_url()); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo(base_url()); ?>assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo(base_url()); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo(base_url()); ?>assets/bower_components/chart.js/Chart.js"></script>
<!-- Morris.js charts -->
<!-- <script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script> -->
<!-- Sparkline -->
<script src="<?php echo(base_url()); ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo(base_url()); ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo(base_url()); ?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo(base_url()); ?>assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo(base_url()); ?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo(base_url()); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo(base_url()); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo(base_url()); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo(base_url()); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo(base_url()); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo(base_url()); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo(base_url()); ?>assets/dist/js/pages/dashboard.js"></script>
<script src="<?php echo(base_url()); ?>assets/script/alert.js"></script>
<script src="<?php echo(base_url()); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo(base_url()); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- content js -->
<?php 
    $text = '';
    $route = $this->uri->segment(2);
    switch ($route) {
      case 'user':
       $text .='<script src="'.base_url().'assets/script/admin/user.js"></script>';
        break;
      case 'kategori':
       $text .='<script src="'.base_url().'assets/script/admin/kategori.js"></script>';;
        break;
      case 'brand':
       $text .='<script src="'.base_url().'assets/script/admin/brand.js"></script>';
        break;
      case 'product':
       $text .='<script src="'.base_url().'assets/script/admin/produk.js"></script>';
        break;
      case 'product_po':
       $text .='<script src="'.base_url().'assets/script/admin/produk_po.js"></script>';
        break;
      case 'summary_product':
       $text .='<script src="'.base_url().'assets/bower_components/select2/dist/js/select2.full.min.js"></script>';
       $text .='<script src="'.base_url().'assets/script/admin/stock.js"></script>';
        break; 
      case 'slider':
       $text .='<script src="'.base_url().'assets/script/admin/slider.js"></script>';
        break;
      case 'banner':
       $text .='<script src="'.base_url().'assets/script/admin/banner.js"></script>';
        break;                 
      default:
        # code...
        break;
    }
    echo $text;
 ?>
</body>
</html>
