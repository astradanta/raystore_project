  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
     
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li >
          <a href="<?php echo(base_url()); ?>cpanel/dashboard">
            <i class="fa fa-dashboard"></i> <span>Home</span>
           
          </a>
          
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-clone"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right text-yellow"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
            <li><a href="<?php echo(base_url()); ?>cpanel/kategori"><i class="fa fa-angle-right"></i> Kategori Produk</a></li>
            <li><a href="<?php echo(base_url()); ?>cpanel/brand"><i class="fa fa-angle-right"></i> Merk / Brand</a></li>           
           
          </ul>
        </li>

        <li >
          <a href="<?php echo(base_url()); ?>cpanel/customer">
            <i class="fa fa-handshake-o"></i>
            <span>Customer</span>           
          </a>
          
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cubes"></i>
            <span>Product Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right text-yellow"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo(base_url()); ?>cpanel/product"><i class="fa fa-angle-right"></i> Ready Stock</a></li>            
            <li><a href="<?php echo(base_url()); ?>cpanel/product_po"><i class="fa fa-angle-right"></i> Produk PO</a></li>
            <li><a href="<?php echo(base_url()); ?>cpanel/summary_product"><i class="fa fa-angle-right"></i> Stock Summary</a></li>
            
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-puzzle-piece"></i>
            <span>Purchase Order</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right text-yellow"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo(base_url()); ?>cpanel/list_po"><i class="fa fa-angle-right"></i> List PO</a></li>
            <li><a href="<?php echo(base_url()); ?>cpanel/summary_po"><i class="fa fa-angle-right"></i> Summary PO</a></li>

          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-shopping-cart"></i>
            <span>Penjualan Online</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right text-yellow"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo(base_url()); ?>cpanel/order"><i class="fa fa-angle-right"></i> Data Order</a></li>
            <li><a href="<?php echo(base_url()); ?>cpanel/transaksi"><i class="fa fa-angle-right"></i> Summary Transaksi</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-chrome"></i>
            <span>Web Setup</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right text-yellow"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo(base_url()); ?>cpanel/slider"><i class="fa fa-angle-right"></i> Slider</a></li>
            <li><a href="<?php echo(base_url()); ?>cpanel/banner"><i class="fa fa-angle-right"></i> Banner</a></li>
          </ul>
        </li>
        
        <li><a href="<?php echo(base_url()); ?>cpanel/report"><i class="fa fa-circle-o text-red"></i> <span>Laporan</span></a></li>
        <li><a href="<?php echo(base_url()); ?>cpanel/user"><i class="fa fa-circle-o text-aqua"></i> <span>User Setting</span></a></li>

        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>