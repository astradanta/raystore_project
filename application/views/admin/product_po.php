<div class="content-wrapper" id="content">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        PRODUK - Barang PO 
        <small>Inventory Management</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product Management</a></li>
        <li class="active">Produk PO</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- /.col -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          
          <a class="btn btn-block btn-social btn-google" id="addBtn">
                <i class="fa fa-plus"></i> Add Product
            </a>
          <br>
          <!-- /.info-box -->
        </div>
      </div>
      <!-- /.col -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">

              <h3 class="box-title">Product Data</h3>
            </div>
            <!-- /.box-header -->
            <!-- /.box-body -->
            <div class="box-body">
                  <div class="row" id="contentInvoice">
                       <div class="col-md-12">
                         <table id="tableProduk" class="table table-bordered table-striped">
                          <thead>
                           <tr>
                              <th>Produk</th>
                              <th>Cover</th>
                              <th>Ketegori</th>
                              <th>Brand</th>
                              <th>Harga Beli</th>
                              <th>Harga Jual</th>
                              <th>Deskripsi</th>                              
                              <th>#</th>
                            </tr>
                          </thead>
                          <tbody id="listView">
                                <tr>
                                  <td>Nike Zoom</td>
                                  <td><img class="img-responsive" src="dist/img/user4-128x128.jpg" alt="User profile picture"></td>
                                  <td>Sepatu Pria</td>
                                  <td>Nike</td>
                                  <td>200.000</td>
                                  <td>350.000</td>
                                  <td>loren ipsun sit dolor amet...</td>                                  
                                  <td class="text-center">
                                    <span data-toggle="tooltip" data-placement="top" title="Delete Item"><button type="button" class="btn btn-danger" ><i class="fa fa-trash"></i>
                                    </button></span>
                                    <span data-toggle="tooltip" data-placement="top" title="Edit"><button type="button" class="btn btn-info"><i class="fa fa-pencil"></i>
                                    </button></span>
                                    <span data-toggle="tooltip" data-placement="top" title="Image Slider"><a href="view-image-produk.php" target="blank"><button type="button" class="btn btn-warning"><i class="fa fa-image"></i>
                                    </button></a></span>
                                   
                                  </td>
                                </tr>
                          </tbody>
                        </table>                       
                      </div>
                </div>
                <!-- /.box-body -->
              </div>
          </div>
          <!-- /.box -->
        </div>
      </div>

      <!-- MODALS -->
      <div class="modal fade" id="inputModal">
          <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Product Item</h4>
              </div>
              <form action="<?php echo(base_url()) ?>cpanel/product_po/add" method="post" enctype="multipart/form-data"  id="inputForm">
              <div class="modal-body" id="inputContent">
                <div class="row">
                  <div class="col-md-8">

                    <div class="box box-danger">
                      <div class="box-header with-border">
                        <h3 class="box-title">General Elements</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                          <!-- text input -->
                          <div class="form-group">
                            <label>Kode Produk</label>
                            <input type="text" class="form-control" id="kode" placeholder="Enter ..." disabled="">
                          </div>
                          
                          <div class="form-group">
                            <label>Nama Produk</label>
                            <input type="text" class="form-control" placeholder="Enter ..." required="" id="nama_barang" name="nama_barang">
                          </div>
                          <!-- textarea -->
                          <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea class="form-control" rows="3" placeholder="Enter ..." required="" id="deskripsi" name="deskripsi"></textarea>
                          </div>           

                          <!-- select -->
                          <div class="form-group">
                            <label>Kategori</label>
                            <select class="form-control" id="kategori" name="id_kategori">
                              <option value="0">Please select kategori</option>
                              <?php $kategori = $this->kategori->listKategori(); 
                                foreach ($kategori as $key) {
                                  echo ('<option value="'.$key->id_kategori.'">'.$key->name.'</option>');
                                }
                              ?>
                            </select>
                          </div>

                          <!-- select -->
                          <div class="form-group">
                            <label>Brand</label>
                            <select class="form-control" id="brand" name="id_brand">
                              <option value="0">Please select brand</option>
                              <?php $brand = $this->brand->listBrand(); 
                                foreach ($brand as $key) {
                                  echo ('<option value="'.$key->id_brand.'">'.$key->name.'</option>');
                                }
                              ?>
                            </select>
                          </div>
                          
                          <div class="form-group">
                            <label>Harga Beli</label>
                            <input type="text" class="form-control number" placeholder="0" required="" id="harga_beli" name="harga_beli">
                          </div>
                          <div class="form-group">
                            <label>Harga Jual</label>
                            <input type="text" class="form-control number" placeholder="0" required="" id="harga_jual" name="harga_jual">
                          </div>                         
                      </div>
                      <!-- /.box-body -->
                    </div>
                  </div>
                
                  <div class="col-md-4">
                    <div class="box box-warning">
                      <div class="box-body">

                      <img class="profile-user-img img-responsive img-circle" id="displayCover" src="<?php echo(base_url().'assets/dist/img/empty.png') ?>" alt="User profile picture">
                      <hr>
                        <div class="form-group">
                          <label for="exampleInputFile">Browse File image</label>
                          <input type="file" id="cover" name="cover" accept="image/*">

                         <!--  <p class="help-block">Example block-level help text here.</p> -->
                        </div>
                      </div>
                    </div>
                  </div>
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
              </form>
            </div>
            <!-- /.modal-content -->

          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="modal fade" id="editModal">
          <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit produk</h4>
              </div>
              <form action="<?php echo(base_url()) ?>cpanel/product_po/edit" method="post" enctype="multipart/form-data"  id="editForm">
              <div class="modal-body" id="editContent"> 

                <div class="row">
                  <div class="col-md-8">

                    <div class="box box-danger">
                      <div class="box-header with-border">
                        <h3 class="box-title">General Elements</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                          <!-- text input -->
                          <div class="form-group">
                            <label>Kode Produk</label>
                            <input type="text" class="form-control" id="kodeEdit" placeholder="Enter ..." disabled="">
                          </div>
                          <input type="hidden" name="id_barang_po" id="id_barang_po">
                          <div class="form-group">
                            <label>Nama Produk</label>
                            <input type="text" class="form-control" placeholder="Enter ..." id="nama_barangEdit" name="nama_barang">
                          </div>
                          <!-- textarea -->
                          <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea class="form-control" rows="3" placeholder="Enter ..." id="deskripsiEdit" name="deskripsi"></textarea>
                          </div>           

                          <!-- select -->
                          <div class="form-group">
                            <label>Kategori</label>
                            <select class="form-control" id="kategoriEdit" name="id_kategori">
                              <option value="0">Please select kategori</option>
                              <?php $kategori = $this->kategori->listKategori(); 
                                foreach ($kategori as $key) {
                                  echo ('<option value="'.$key->id_kategori.'">'.$key->name.'</option>');
                                }
                              ?>
                            </select>
                          </div>

                          <!-- select -->
                          <div class="form-group">
                            <label>Brand</label>
                            <select class="form-control" id="brandEdit" name="id_brand">
                              <option value="0">Please select brand</option>
                              <?php $brand = $this->brand->listBrand(); 
                                foreach ($brand as $key) {
                                  echo ('<option value="'.$key->id_brand.'">'.$key->name.'</option>');
                                }
                              ?>
                            </select>
                          </div>
                          
                          <div class="form-group">
                            <label>Harga Beli</label>
                            <input type="text" class="form-control number" placeholder="0" id="harga_beliEdit" name="harga_beli">
                          </div>
                          <div class="form-group">
                            <label>Harga Jual</label>
                            <input type="text" class="form-control number" placeholder="0" id="harga_jualEdit" name="harga_jual">
                          </div>
                      </div>
                      <!-- /.box-body -->
                    </div>
                  </div>
                
                  <div class="col-md-4">
                    <div class="box box-warning">
                      <div class="box-body">

                      <img class="profile-user-img img-responsive img-circle" id="displayCoverEdit" src="<?php echo(base_url().'assets/dist/img/empty.png') ?>" alt="User profile picture">
                      <hr>
                        <div class="form-group">
                          <label for="exampleInputFile">Browse File image</label>
                          <input type="file" id="coverEdit" name="cover" accept="image/*">

                         <!--  <p class="help-block">Example block-level help text here.</p> -->
                        </div>
                      </div>
                    </div>
                  </div>
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
              </form>
            </div>
            <!-- /.modal-content -->

          </div>
          <!-- /.modal-dialog -->
        </div>


        <div class="modal fade" id="detailModal">
          <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detail Product</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-8">

                    <div class="box box-danger">
                      <div class="box-header with-border">
                        <h3 class="box-title">General Elements</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                          <!-- text input -->
                          <div class="form-group">
                            <label>Kode Produk</label>
                            <input type="text" class="form-control" id="kodeDetail" placeholder="Enter ..." disabled="">
                          </div>
                          <input type="hidden" name="id_barang_po" id="id_barang_po">
                          <div class="form-group">
                            <label>Nama Produk</label>
                            <input type="text" class="form-control" placeholder="Enter ..." id="nama_barangDetail" readonly=""  name="nama_barang">
                          </div>
                          <!-- textarea -->
                          <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea class="form-control" rows="3" placeholder="Enter ..." id="deskripsiDetail" readonly="" name="deskripsi"></textarea>
                          </div>           
                           <div class="form-group">
                            <label>Katerori</label>
                            <input type="text" class="form-control" placeholder="0" id="kategoriDetail" readonly="" name="harga_beli">
                          </div>
                          <!-- select -->
                          <div class="form-group">
                            <label>Brand</label>
                            <input type="text" class="form-control" placeholder="0" id="brandDetail" readonly="" name="harga_beli">
                          </div>
                          <div class="form-group">
                            <label>Harga Beli</label>
                            <input type="text" class="form-control" placeholder="0" id="harga_beliDetail" readonly="" name="harga_beli">
                          </div>
                          <div class="form-group">
                            <label>Harga Jual</label>
                            <input type="text" class="form-control" placeholder="0" id="harga_jualDetail" readonly="" name="harga_jual">
                          </div>
                          <div class="form-group">
                            <label>Create At</label>
                            <input type="text" class="form-control" placeholder="0" id="createDetail" readonly="" name="harga_jual">
                          </div>
                          <div class="form-group">
                            <label>Update At</label>
                            <input type="text" class="form-control" id="updateDetail" readonly="" name="harga_jual">
                          </div>
                      </div>
                      <!-- /.box-body -->
                    </div>
                  </div>
                
                  <div class="col-md-4">
                    <div class="box box-warning">
                      <div class="box-body">

                      <img class="profile-user-img img-responsive img-circle" id="displayCoverDetail" src="<?php echo(base_url().'assets/dist/img/empty.png') ?>" alt="User profile picture">
                      <hr>
                      <h5>Cover Image</h5>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4" id="subImage1" style="display: none">
                    <div class="box box-warning">
                      <div class="box-body">

                      <img class="profile-user-img img-responsive img-circle" id="displaySubImage1" src="<?php echo(base_url().'assets/dist/img/empty.png') ?>" alt="User profile picture">
                      <hr>
                      <h5>Sub Image</h5>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4" id="subImage2" style="display: none">
                    <div class="box box-warning">
                      <div class="box-body">

                      <img class="profile-user-img img-responsive img-circle" id="displaySubImage2" src="<?php echo(base_url().'assets/dist/img/empty.png') ?>" alt="User profile picture">
                      <hr>
                      <h5>Sub Image</h5>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4" id="subImage3" style="display: none">
                    <div class="box box-warning">
                      <div class="box-body">

                      <img class="profile-user-img img-responsive img-circle" id="displaySubImage3" src="<?php echo(base_url().'assets/dist/img/empty.png') ?>" alt="User profile picture">
                      <hr>
                      <h5>Sub Image</h5>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                
              </div>              
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

      <div class="modal fade" id="imageModal">
          <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content" id="image">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Image List</h4>
              </div>
              <form action="<?php echo(base_url()) ?>cpanel/product_po/image" method="post" enctype="multipart/form-data"  id="imageForm">
              <div class="modal-body" id="imageContent">
                <div class="row">
                  <input type="hidden" name="id_barang_po" id="image_id_barang_po">
                  <div class="col-md-4">
                      <div class="box box-warning">
                        <div class="box-body">
                        <img class="profile-user-img img-responsive img-circle" id="displayImage1" src="<?php echo(base_url().'assets/dist/img/empty.png') ?>" alt="User profile picture">
                        <hr>
                          <div class="form-group">
                            <label for="exampleInputFile">Browse File image</label>
                            <input type="hidden" id="indicator1" name="indicator1" value="0">
                            <input type="file" id="image1" name="image1" data-display="displayImage1" class="productImade" accept="image/*">
                            <button type="button" id="delete1" data-id="0" class="pull-right btn btn-danger deleteImage" data-indicator = "indicator1" data-display="displayImage1"><i class="fa fa-trash"></i></button>
                           <!--  <p class="help-block">Example block-level help text here.</p> -->
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                      <div class="box box-warning">
                        <div class="box-body">
                        <img class="profile-user-img img-responsive img-circle" id="displayImage2" src="<?php echo(base_url().'assets/dist/img/empty.png') ?>" alt="User profile picture">
                        <hr>
                          <div class="form-group">
                            <label for="exampleInputFile">Browse File image</label>
                            <input type="hidden" id="indicator2" name="indicator2" value="0">
                            <input type="file" id="image2" name="image2" data-display="displayImage2" class="productImade" accept="image/*">
                            <button type="button" id="delete2" data-id="0" class="pull-right btn btn-danger deleteImage" data-indicator ="indicator2" data-display="displayImage2"><i class="fa fa-trash"></i></button>
                           <!--  <p class="help-block">Example block-level help text here.</p> -->
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                      <div class="box box-warning">
                        <div class="box-body">
                        <img class="profile-user-img img-responsive img-circle" id="displayImage3" src="<?php echo(base_url().'assets/dist/img/empty.png') ?>" alt="User profile picture">
                        <hr>
                          <div class="form-group">
                            <label for="exampleInputFile">Browse File image</label>
                            <input type="hidden" id="indicator3" name="indicator3" value="0">
                            <input type="file" id="image3" name="image3" data-display="displayImage3" class="productImade" accept="image/*">
                            <button type="button" id="delete3" data-id="0" class="pull-right btn btn-danger deleteImage" data-indicator = "indicator3" data-display="displayImage3"><i class="fa fa-trash"></i></button>
                           <!--  <p class="help-block">Example block-level help text here.</p> -->
                          </div>
                        </div>
                    </div>
                  </div>                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
              </form>
            </div>
            <!-- /.modal-content -->

          </div>
          <!-- /.modal-dialog -->
        </div>       

        <div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Are you sure delete this item?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Yes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <!-- /.modal -->

    </section>
    <!-- /.content -->
  </div>