<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ray Store Cpanel | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo(base_url()) ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo(base_url()) ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo(base_url()) ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo(base_url()) ?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo(base_url()) ?>assets/plugins/iCheck/square/blue.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<input type="hidden" name="" id="baselink" value="<?php echo(base_url()); ?>cpanel/">
<div class="login-box">
  <div class="login-logo" >
    <a href="<?php echo(base_url()) ?>cpanel/login"><b style="color: #f99432">Ray</b> <b style="color: #fff">Store</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body" id="loginPanel">
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="<?php echo(base_url()) ?>cpanel/login/auth" method="post" id="loginForm">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="email" required="required">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password" required="required">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <a href="#" id="forgotLink">I forgot my password</a>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-block btn-flat" style="background-color: #f99432;color: #fff">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <div class="login-box-body" id="forgotPanel" style="display: none;">
    <p class="login-box-msg">Insert your email to get your previous password</p>

    <form action="<?php echo(base_url()) ?>cpanel/login/forget" method="post" id="forgetForm">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <button type="button" class="btn btn-danger btn-flat col-xs-6" id="cancel_btn">Cancel</button>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-block btn-flat" style="background-color: #f99432;color: #fff">Send</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo(base_url()) ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo(base_url()) ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo(base_url()) ?>assets/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo(base_url()) ?>assets/script/admin/login.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
