<div class="content-wrapper" id="content">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Banner Setting
        <small>Web Setup</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        
        <li class="active">Banner</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">      
     <!-- /.col -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">

              <h3 class="box-title">Data Banner</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                  <div class="row" id="contentInvoice">
                       <div class="col-md-12">
                         <table id="tableBanner" class="table table-bordered table-striped">
                          <thead>
                             <tr>
                                <th width="60%">Image</th>
                                <th>Link</th>                 
                                <th>#</th>
                              </tr>
                          </thead>
                          <tbody id="listView">
                               <tr>
                                <td><img src="<?php echo(base_url()) ?>assets/dist/img/empty.png"  id="displayIcon"  style="" ></td>
                                <td>Baju Anak</td>
                               
                                <td class="text-center">
                                  <span data-toggle="tooltip" data-placement="top" title="Delete Item"><button type="button" class="btn btn-danger" ><i class="fa fa-trash"></i>
                                  </button></span>
                                  <span data-toggle="tooltip" data-placement="top" title="Edit"><button type="button" class="btn btn-info"><i class="fa fa-pencil"></i>
                                  
                                </td>
                              </tr>
                          </tbody>
                        </table>                       
                      </div>
                </div>
                <!-- /.box-body -->
              </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

      <!-- MODALS -->
        <!-- /.modal -->
        <div class="modal fade"  id="editModal">
          <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Banner</h4>
              </div>
              <form role="form" action="<?php echo(base_url()) ?>cpanel/banner/edit" method="post" enctype="multipart/form-data"  id="editForm">
              <div class="modal-body" id="editContent">
                <div class="row">
                  <input type="hidden" name="id_banner" id="id_banner">
                  <div class="col-md-12">

                    <div class="box box-danger">
                      
                      <!-- /.box-header -->
                      <div class="box-body">
                        
                          <!-- text input -->
                          <div class="form-group text-center">
                            <img src="<?php echo(base_url()) ?>assets/dist/img/empty.png" width="200" id="displayEditIcon" height="200" style="cursor: pointer; pointer; display: block;    margin-left: auto; margin-right: auto; width: 50%; height: auto">
                            <input type="file" name="icon" id="iconEdit" style="display: none;" accept="image/*">
                            <h4 id="iconEditValidate" style="color: red;display: none;">You Must Insert Photo</h4>
                          </div>                          
                          <div class="form-group">
                            <label>Link</label>
                            <input type="text" class="form-control" placeholder="Enter ..." id="linkEdit" name="link" required="">
                          </div>  
                      </div>
                      <!-- /.box-body -->
                    </div>
                  </div>
                
                 
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <div class="modal fade"  id="detailModal">
          <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detail Banner</h4>
              </div>
              <div class="modal-body" id="inputContent">
                <div class="row">
                  <input type="hidden" name="id_banner" id="id_banner">
                  <div class="col-md-12">

                    <div class="box box-danger">
                      
                      <!-- /.box-header -->
                      <div class="box-body">
                        
                          <!-- text input -->
                          <div class="form-group text-center">
                            <img src="<?php echo(base_url()) ?>assets/dist/img/empty.png"  id="displayDetailIcon" style="cursor: pointer; pointer; display: block;    margin-left: auto; margin-right: auto; width: 50%; height: auto">
                          </div>                          
                          <div class="form-group">
                            <label>Link</label>
                            <input type="text" class="form-control" id="linkDetail" name="name" readonly="">
                          </div>
                          <div class="form-group">
                            <label>Update At</label>
                            <input type="text" class="form-control"  id="updateDetail" name="email" readonly="">
                          </div>                            
                      </div>
                      <!-- /.box-body -->
                    </div>
                  </div>
                
                 
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
               
              </div>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
    </section>
    <!-- /.content -->
  </div>