<div class="content-wrapper" id="content">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Stock Summary 
        <small>Stock Management</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        
        <li class="active">Stock Summary</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- /.col -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">          
          <a class="btn btn-block btn-social btn-google" id="addBtn" data-toggle="modal" data-target="#inputModal">
                <i class="fa fa-plus"></i> Add product Stock
            </a>
          <br>
          <!-- /.info-box -->
        </div>
         
      </div>
      <!-- /.col -->

      <div class="row">
        <div class="col-xs-12" >
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Stock Data</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                  <div class="row" id="contentInvoice">
                       <div class="col-md-12">
                         <table id="tableStock" class="table table-bordered table-striped">
                          <thead>
                             <tr>
                                <th>Tanggal</th>
                                <th>Type</th>
                                <th>Produk</th>
                                <th>Amount</th>
                                <th>Description</th>
                                <th>#</th>
                              </tr>
                          </thead>
                          <tbody id="listView">
                               <tr>
                                <td>22 Agustus 2018</td>
                                <td>Adding</td>
                                <td>DC Shoes</td>
                                <td>3</td>
                                <td>Lorem ipsum</td>
                                <td class="text-center">
                                  <span data-toggle="tooltip" data-placement="top" title="Delete Item"><button type="button" class="btn btn-danger" ><i class="fa fa-trash"></i>
                                  </button></span>
                                  <span data-toggle="tooltip" data-placement="top" title="Edit"><button type="button" class="btn btn-info"><i class="fa fa-pencil"></i>
                                  
                                </td>
                              </tr>
                          </tbody>
                        </table>                       
                      </div>
                </div>
                <!-- /.box-body -->
              </div> 
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

      <!-- MODALS -->
      <div class="modal fade" id="inputModal">
          <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalTitle">Input Data</h4>
              </div>
              <form action="<?php echo(base_url()) ?>cpanel/summary_product/add" method="post" id="inputForm">
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">

                    <div class="box box-danger">
                      
                      <!-- /.box-header -->
                      <div class="box-body">

                          <!-- text input -->
                          <div class="form-group">
                            <label>Produk</label>
                            <input type="hidden" id="currentStock" value="">
                            <select class="form-control select2" style="width: 100%;border-radius: 0px;height: 38px;" id="produk" name="id_produk">
                              <option selected="selected">Select produk</option>
                              <?php $produk = $this->produk->listProduk(); 
                                foreach ($produk as $key ) {
                                  ?>
                                  <option value="<?php echo($key->id_produk) ?>" stock="<?php echo($key->amount) ?>"><?php echo $key->nama_produk; ?></option>
                                  <?php
                                }
                              ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Type</label>
                               <div class="radio">
                                  <label class="radio-inline"><input type="radio" id="firstRadio" name="type" value="0">Add</label>
                                  <label class="radio-inline"><input type="radio" id="secondRadio" name="type" value="1">Remove</label>
                              </div>  

                          </div>
                          <div class="form-group">
                            <label>Amount</label>
                            <input type="text" class="form-control number" id="amount" name="amount">
                          </div>
                         <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3"  required="" id="description" name="description"></textarea>
                          </div>
                          <!-- select -->
                         
                       </div>
                      <!-- /.box-body -->
                    </div>
                  </div>
                
                 
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="editModal">
          <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalTitle">Edit Data</h4>
              </div>
              <form action="<?php echo(base_url()) ?>cpanel/summary_product/edit" method="post" id="editForm">
              <div class="modal-body" id="editContent">
                <div class="row">
                  <div class="col-md-12">

                    <div class="box box-danger">
                      
                      <!-- /.box-header -->
                      <div class="box-body">
                          <input type="hidden" id="id_stock" name="id_stock">
                          <!-- text input -->
                          <div class="form-group">
                            <label>Produk</label>
                            <input type="hidden" id="currentStockEdit" value="">
                            <select class="form-control select2" style="width: 100%;border-radius: 0px;height: 38px;" id="produkEdit" name="id_produk">
                              <option selected="selected">Select produk</option>
                              <?php $produk = $this->produk->listProduk(); 
                                foreach ($produk as $key ) {
                                  ?>
                                  <option value="<?php echo($key->id_produk) ?>" stock="<?php echo($key->amount) ?>"><?php echo $key->nama_produk; ?></option>
                                  <?php
                                }
                              ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Type</label>
                               <div class="radio">
                                  <label class="radio-inline"><input type="radio" id="firstRadioEdit" name="type" value="0">Add</label>
                                  <label class="radio-inline"><input type="radio" id="secondRadioEdit" name="type" value="1">Remove</label>
                              </div>  

                          </div>
                          <div class="form-group">
                            <label>Amount</label>
                            <input type="text" class="form-control number" id="amountEdit" name="amount">
                          </div>
                         <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3"  required="" id="descriptionEdit" name="description"></textarea>
                          </div>
                          <!-- select -->
                         
                       </div>
                      <!-- /.box-body -->
                    </div>
                  </div>
                
                 
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="modal fade"  id="detailModal">
          <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detail Data</h4>
              </div>
              <div class="modal-body" id="inputContent">
                <div class="row">
                  <input type="hidden" name="id_user" id="id_user">
                  <div class="col-md-12">

                    <div class="box box-danger">
                      
                      <!-- /.box-header -->
                      <div class="box-body">
                        <div class="form-group">
                            <label>Produk</label>
                            <input type="text" class="form-control number" id="produkDetail" readonly="" name="amount">
                          </div>
                        <div class="form-group">
                            <label>Type</label>
                            <input type="text" class="form-control number" id="typeDetail" readonly="" name="amount">
                          </div>
                        <div class="form-group">
                            <label>Amount</label>
                            <input type="text" class="form-control number" id="amountDetail" readonly="" name="amount">
                          </div>
                         <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3"  required="" readonly="" id="descriptionDetail" name="description"></textarea>
                          </div>
                          <div class="form-group">
                            <label>Create At</label>
                            <input type="text" class="form-control"  id="createDetail" name="email" readonly="">
                          </div>
                          <div class="form-group">
                            <label>Update At</label>
                            <input type="text" class="form-control"  id="updateDetail" name="email" readonly="">
                          </div>                            
                      </div>
                      <!-- /.box-body -->
                    </div>
                  </div>
                
                 
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
               
              </div>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

         <div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Are you sure delete this item?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Yes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
    </section>
    <!-- /.content -->
  </div>