<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan 
        <small>Ray Store</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
   
        <li class="active">Laporan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
    

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
           
            <!-- /.box-header -->
           <div class="box-body" style="min-height: 200px">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <a href="#" data-toggle="modal" data-target="#form-add-pl">
                    <div class="info-box bg-orange">
                      <span class="info-box-icon"><i class="fa fa-newspaper-o"></i></span>

                      <div class="info-box-content">                       
                        <span class="info-box-pos text-center">Penjualan Online</span>                       
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                  </a>
                  <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <a href="#" data-toggle="modal" data-target="#form-add-pk">
                    <div class="info-box bg-teal">
                      <span class="info-box-icon"><i class="fa fa-newspaper-o"></i></span>

                      <div class="info-box-content">                       
                        <span class="info-box-pos text-center">Purchase Order</span>                       
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                  </a>
                  <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <a href="#" data-toggle="modal" data-target="#form-add-pk">
                    <div class="info-box bg-red">
                      <span class="info-box-icon"><i class="fa fa-newspaper-o"></i></span>

                      <div class="info-box-content">                       
                        <span class="info-box-pos text-center">Stok</span>                       
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                  </a>
                  <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <a href="#" data-toggle="modal" data-target="#form-add-pk">
                    <div class="info-box bg-blue">
                      <span class="info-box-icon"><i class="fa fa-newspaper-o"></i></span>

                      <div class="info-box-content">                       
                        <span class="info-box-pos text-center">Konfirmasi Pembayaran</span>                       
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                  </a>
                  <!-- /.info-box -->
                </div>

                <br><br><br>

              </div>
              <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

      <!-- MODALS -->
      <div class="modal fade" id="form-add-stock">
          <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Image</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">

                    <div class="box box-danger">
                      <div class="box-header with-border">
                        <h3 class="box-title">General Elements</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                       
                      </div>
                      <!-- /.box-body -->
                    </div>
                  </div>
                
                  
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    </section>
    <!-- /.content -->
  </div>