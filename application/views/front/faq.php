 <div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo(base_url()); ?>faq">FAQ</a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
        <!--Middle Part Start-->
        <div id="content" class="col-sm-12">
          <h1 class="title">Help &amp; FAQ</h1>
          <hr>
          <div class="row">
            <div class="col-sm-3">
              <h3>My Account &amp; My Orders</h3>
            </div>
            <div class="col-sm-9">
              <div class="faq">
                <div> <a href="#faq1" data-parent="#accordion" data-toggle="collapse" class="panel-title">What is 'My Account'? How do I update my information ? <i class="fa fa-caret-down"></i></a>
                  <div id="faq1" class="panel-collapse collapse">
                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec eros tellus, scelerisque nec, rhoncus eget, sollicitudin eu, vehicula venenatis, tempor vitae, est. Praesent vitae dui. Morbi id tellus. Nullam ac nisi non eros gravida venenatis. Ut euismod, turpis sollicitudin lobortis pellentesque, libero massa dapibus dui, eu.</div>
                  </div>
                </div>
                <div> <a href="#faq2" data-parent="#accordion" data-toggle="collapse" class="panel-title">How do I merge my accounts linked to different email ids? <i class="fa fa-caret-down"></i></a>
                  <div id="faq2" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec eros tellus, scelerisque nec, rhoncus eget, sollicitudin eu, vehicula venenatis, tempor vitae, est. Praesent vitae dui. Morbi id tellus. Nullam ac nisi non eros gravida venenatis.</p>
                      <ul>
                        <li>Ut euismod, turpis sollicitudin lobortis pellentesque, libero massa dapibus dui, eu.</li>
                        <li>Consectetuer adipiscing elit. Donec eros tellus.</li>
                        <li>Donec eros tellus, scelerisque nec, rhoncus eget, sollicitudin eu, vehicula venenatis, tempor vitae, est.</li>
                        <li>Morbi id tellus. Nullam ac nisi non eros gravida venenatis.</li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div> <a href="#faq3" data-parent="#accordion" data-toggle="collapse" class="panel-title"> How do I know my order has been confirmed? <i class="fa fa-caret-down"></i></a>
                  <div id="faq3" class="panel-collapse collapse">
                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec eros tellus, scelerisque nec, rhoncus eget, sollicitudin eu, vehicula venenatis, tempor vitae, est. Praesent vitae dui. Morbi id tellus. Nullam ac nisi non eros gravida venenatis. Ut euismod, turpis sollicitudin lobortis pellentesque, libero massa dapibus dui, eu.</div>
                  </div>
                </div>
                <div> <a href="#faq4" data-parent="#accordion" data-toggle="collapse" class="panel-title">Can I order a product that is 'Out of Stock'? <i class="fa fa-caret-down"></i></a>
                  <div id="faq4" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec eros tellus, scelerisque nec, rhoncus eget, sollicitudin eu, vehicula venenatis, tempor vitae, est. Praesent vitae dui. Morbi id tellus. Nullam ac nisi non eros gravida venenatis.</p>
                      <ul>
                        <li>Ut euismod, turpis sollicitudin lobortis pellentesque, libero massa dapibus dui, eu.</li>
                        <li>Consectetuer adipiscing elit. Donec eros tellus.</li>
                        <li>Donec eros tellus, scelerisque nec, rhoncus eget, sollicitudin eu, vehicula venenatis, tempor vitae, est.</li>
                        <li>Morbi id tellus. Nullam ac nisi non eros gravida venenatis.</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-3">
              <h3>Shopping</h3>
            </div>
            <div class="col-sm-9">
              <div class="faq">
                <div> <a href="#faq5" data-parent="#accordion" data-toggle="collapse" class="panel-title">I see different prices with the same title. Why? <i class="fa fa-caret-down"></i></a>
                  <div id="faq5" class="panel-collapse collapse">
                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec eros tellus, scelerisque nec, rhoncus eget, sollicitudin eu, vehicula venenatis, tempor vitae, est. Praesent vitae dui. Morbi id tellus. Nullam ac nisi non eros gravida venenatis. Ut euismod, turpis sollicitudin lobortis pellentesque, libero massa dapibus dui, eu.</div>
                  </div>
                </div>
                <div> <a href="#faq6" data-parent="#accordion" data-toggle="collapse" class="panel-title">Why do I see different prices for the same product? <i class="fa fa-caret-down"></i></a>
                  <div id="faq6" class="panel-collapse collapse">
                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec eros tellus, scelerisque nec, rhoncus eget, sollicitudin eu, vehicula venenatis, tempor vitae, est. Praesent vitae dui. Morbi id tellus. Nullam ac nisi non eros gravida venenatis. Ut euismod, turpis sollicitudin lobortis pellentesque, libero massa dapibus dui, eu.</div>
                  </div>
                </div>
                <div> <a href="#faq7" data-parent="#accordion" data-toggle="collapse" class="panel-title">Is it necessary to have an account to shop on Marketshop? <i class="fa fa-caret-down"></i></a>
                  <div id="faq7" class="panel-collapse collapse">
                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec eros tellus, scelerisque nec, rhoncus eget, sollicitudin eu, vehicula venenatis, tempor vitae, est. Praesent vitae dui. Morbi id tellus. Nullam ac nisi non eros gravida venenatis. Ut euismod, turpis sollicitudin lobortis pellentesque, libero massa dapibus dui, eu.</div>
                  </div>
                </div>
                <div> <a href="#faq8" data-parent="#accordion" data-toggle="collapse" class="panel-title">What do I need to know before getting an order gift wrapped? <i class="fa fa-caret-down"></i></a>
                  <div id="faq8" class="panel-collapse collapse">
                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec eros tellus, scelerisque nec, rhoncus eget, sollicitudin eu, vehicula venenatis, tempor vitae, est. Praesent vitae dui. Morbi id tellus. Nullam ac nisi non eros gravida venenatis. Ut euismod, turpis sollicitudin lobortis pellentesque, libero massa dapibus dui, eu.</div>
                  </div>
                </div>
                <div> <a href="#faq9" data-parent="#accordion" data-toggle="collapse" class="panel-title">What is Advantage? <i class="fa fa-caret-down"></i></a>
                  <div id="faq9" class="panel-collapse collapse">
                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec eros tellus, scelerisque nec, rhoncus eget, sollicitudin eu, vehicula venenatis, tempor vitae, est. Praesent vitae dui. Morbi id tellus. Nullam ac nisi non eros gravida venenatis. Ut euismod, turpis sollicitudin lobortis pellentesque, libero massa dapibus dui, eu.</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
         
        </div>
        <!--Middle Part End -->
      </div>
    </div>
  </div>
<!-- footer 