 <div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo(base_url()); ?>account">Account</a></li>
        <li><a href="<?php echo(base_url()); ?>account">My Account</a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
        <!--Middle Part Start-->
        <div class="col-sm-12" id="content">
          <h1 class="title">My Account Page</h1>
         
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true">Order List</a></li>
                <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false">History</a></li>
                <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false">Account Overview</a></li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab-1">
                <h3 class="title">Your Order List</h3>
                  <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          
                          <td class="text-left">No Nota</td>
                          <td class="text-left">Tanggal</td>
                          <td class="text-right">Total</td>
                          <td class="text-right">Pick Up Date</td>
                          <td class="text-right">Status</td>
                          <td class="text-right">Action</td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          
                          <td class="text-left"><a href="product.html">#121334</a></td>
                          <td class="text-left">25/10/2018</td>
                          <td class="text-right">200.000</td>
                          <td class="text-right"><div class="price"> 26/10/2018 </div></td>
                          <td class="text-right"><span>Terkonfirmasi</span></td>
                          <td class="text-right">

                          <button class="btn btn-primary" title="" data-toggle="tooltip" type="button" data-original-title="View Detail"><i class="fa fa-chevron-circle-right"></i></button>
                            </td>
                        </tr>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="tab-pane" id="tab-2">
                  <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          
                          <td class="text-left">No Nota</td>
                          <td class="text-left">Tanggal</td>
                          <td class="text-right">Total</td>                          
                          <td class="text-right">Status</td>
                          <td class="text-right">Action</td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          
                          <td class="text-left"><a href="product.html">#121334</a></td>
                          <td class="text-left">25/10/2018</td>
                          <td class="text-right">200.000</td>                          
                          <td class="text-right"><span >Selesai</span></td>
                          <td class="text-right">

                          <button class="btn btn-success" title="" data-toggle="tooltip"  type="button" data-original-title="Nota"><i class="fa fa-cart-arrow-down"></i> View Nota</button>
                            </td>
                        </tr>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="tab-pane" id="tab-3">Consectetuer adipiscing elit. Donec eros tellus, scelerisque nec, rhoncus eget, sollicitudin eu, vehicula venenatis, tempor vitae, est. Praesent vitae dui. Morbi id tellus. Lorem ipsum dolor sit amet,</div>
              </div>
        </div>
        <!--Middle Part End -->
       
      </div>
    </div>
  </div>