<div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo(base_url()); ?>cart">Shopping Cart</a></li>
        <li><a href="<?php echo(base_url()); ?>checkout">Checkout</a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
        <!--Middle Part Start-->
        <div id="content" class="col-sm-12">
          <h1 class="title">Checkout</h1>
          <div class="row">
            
            <div class="col-sm-12">
            <div class="alert alert-danger"><i class="fa fa-clock-o"></i><h3 class="text-right"> Cart Timer : (01:58) </h3></div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title"><i class="fa fa-truck"></i> Pick up Time</h4>
                    </div>
                      <div class="panel-body">
                        <p>Please select the preferred shipping method to use on this order.</p>
                        <form class="form-horizontal">
                          <fieldset>
                            
                            <div class="form-group required">
                              <label class="col-md-2 col-sm-3 control-label" for="input-name">Date Pick Up</label>
                              <div class="col-md-10 col-sm-6">
                                <input type="text" name="name" value="" id="input-name" class="form-control">
                              </div>
                            </div>
                            <div class="form-group required">
                              <label class="col-md-2 col-sm-3 control-label" for="input-email">Time Pick-Up</label>
                              <div class="col-md-10 col-sm-6">
                                <input type="text" name="email" value="" id="input-email" class="form-control">
                              </div>
                            </div>
                           
                          </fieldset>
                         
                        </form>
                      </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title"><i class="fa fa-truck"></i> Delivery Method</h4>
                    </div>
                      <div class="panel-body">
                        <p>Please select the preferred shipping method to use on this order.</p>
                        <div class="radio">
                          <label>
                            <input type="radio" checked="checked" name="Free Shipping">
                            Pick Up Order </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="Flat Shipping Rate">
                            JNE</label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="Per Item Shipping Rate">
                            POS</label>
                        </div>
                      </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title"><i class="fa fa-credit-card"></i> Payment Method</h4>
                    </div>
                      <div class="panel-body">
                        <p>Please select the preferred payment method to use on this order.</p>
                        <div class="radio">
                          <label>
                            <input type="radio" checked="checked" name="Cash On Delivery">
                            Cash On Delivery</label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="Bank Transfer">
                            Bank Transfer</label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="Paypal">
                            Online Payment (DOKU)</label>
                        </div>
                      </div>
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title"><i class="fa fa-shopping-cart"></i> Shopping cart</h4>
                    </div>
                      <div class="panel-body">
                        <div class="table-responsive">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <td class="text-center">Image</td>
                                <td class="text-left">Product Name</td>
                                <td class="text-left">Quantity</td>
                                <td class="text-right">Unit Price</td>
                                <td class="text-right">Total</td>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td class="text-center"><a href="product.html"><img width="50" height="50" src="<?php echo(base_url()); ?>assets/front/image/product/sepatu.jpeg" alt="Xitefun Causal Wear Fancy Shoes" title="Xitefun Causal Wear Fancy Shoes" class="img-thumbnail"></a></td>
                                <td class="text-left"><a href="product.html">Xitefun Causal Wear Fancy Shoes</a></td>
                                <td class="text-left">2</td>
                                <td class="text-right">$902.00</td>
                                <td class="text-right">$902.00</td>
                              </tr>
                            </tbody>
                            <tfoot>
                              <tr>
                                <td class="text-right" colspan="4"><strong>Sub-Total:</strong></td>
                                <td class="text-right">$750.00</td>
                              </tr>
                              <tr>
                                <td class="text-right" colspan="4"><strong>Flat Shipping Rate:</strong></td>
                                <td class="text-right">$5.00</td>
                              </tr>
                              <tr>
                                <td class="text-right" colspan="4"><strong>Eco Tax (-2.00):</strong></td>
                                <td class="text-right">$4.00</td>
                              </tr>
                              <tr>
                                <td class="text-right" colspan="4"><strong>VAT (20%):</strong></td>
                                <td class="text-right">$151.00</td>
                              </tr>
                              <tr>
                                <td class="text-right" colspan="4"><strong>Total:</strong></td>
                                <td class="text-right">$910.00</td>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                      </div>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title"><i class="fa fa-pencil"></i> Add Comments About Your Order</h4>
                    </div>
                      <div class="panel-body">
                        <textarea rows="4" class="form-control" id="confirm_comment" name="comments"></textarea>
                        <br>
                        <label class="control-label" for="confirm_agree">
                          <input type="checkbox" checked="checked" value="1" required="" class="validate required" id="confirm_agree" name="confirm agree">
                          <span>I have read and agree to the <a class="agree" href="#"><b>Terms &amp; Conditions</b></a></span> </label>
                        <div class="buttons">
                          <div class="pull-right">
                            <input type="button" class="btn btn-primary" id="button-confirm" value="Confirm Order">
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--Middle Part End -->
      </div>
    </div>
  </div>