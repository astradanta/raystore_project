 <div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="#">Pre Order</a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
       
        <!--Middle Part Start-->
        <div id="content" class="col-sm-12">
          <h1 class="title">Pre Order</h1>
        
         
          <div class="product-filter">
            <div class="row">
              <div class="col-md-4 col-sm-5">
                <div class="btn-group">
                  <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>
                  <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="Grid"><i class="fa fa-th"></i></button>
                </div>
              </div>
              <div class="col-sm-2 text-right">
                <label class="control-label" for="input-sort">Sort By:</label>
              </div>
              <div class="col-md-3 col-sm-2 text-right">
                <select id="input-sort" class="form-control col-sm-3">
                  <option value="" selected="selected">Default</option>
                  <option value="name_asc">Name (A - Z)</option>
                  <option value="name_desc">Name (Z - A)</option>
                  <option value="price_asc">Price (Low &gt; High)</option>
                  <option value="price_desc">Price (High &gt; Low)</option>
                </select>
              </div>
              <div class="col-sm-1 text-right">
                <label class="control-label" for="input-limit">Show:</label>
              </div>
              <div class="col-sm-2 text-right">
                <select id="input-limit" class="form-control">                 
                  <option value="20" selected="selected">20</option>
                  <option value="25">25</option>
                  <option value="50">50</option>
                  <option value="75">75</option>
                  <option value="100">100</option>
                </select>
              </div>
            </div>
          </div>
          <br />
          <div class="row products-category" id="listView">
            <?php $po = $this->produk_po->poFront(); 
                foreach ($po as $key) {
                  ?>
                    <div class="product-layout product-list col-xs-12">
                      <div class="product-thumb">
                        <div class="image"><a href="<?=(base_url()."po_detail/".$key->id_produk) ?>"><img src="<?=$key->cover ?>" alt="<?=$key->nama_produk ?>" title="<?=$key->nama_produk ?>" class="img-responsive" /></a></div>
                        <div>
                          <div class="caption">
                            <h4><a href="<?=(base_url()."po_detail/".$key->id_produk) ?>"><?=$key->nama_produk ?></a></h4>
                            <p class="description"><?=$key->deskripsi ?></p>
                            <p class="price"> Rp. <?=$key->harga_jual ?></p>
                          </div>
                          <div class="button-group">
                            <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                            
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php
                }
            ?>
           
          </div>
          <div class="row">
           <?php $page = $this->produk_po->poPagination(); 
              if($page['page_count'] != 0){
                ?>
                <div class="col-sm-6 text-left" id="paginContent">
                  <ul class="pagination" id="pagination"></ul>
                </div>
                <?php
              }
            ?>
            <input type="hidden" id="page_count" name="" value="<?=$page['page_count']  ?>">
            <div class="col-sm-6 text-right" id="bottomLabel">Showing 1 to <?=$page['show_item'] ?> of <?=$page['item_count'] ?> (<?=$page['page_count'] ?> Pages)</div>
          </div>
        </div>
        <!--Middle Part End -->
      </div>
    </div>
  </div>