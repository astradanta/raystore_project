 <div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo(base_url()); ?>contact_us">Contact Us</a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
        <!--Middle Part Start-->
        <div id="content" class="col-sm-9">
          <h1 class="title">Contact Us</h1>
          <h3 class="subtitle">Our Location</h3>
          <div class="row">
            <div class="col-sm-3"><img src="<?php echo(base_url()); ?>assets/front/image/product/store_location-275x180.jpg" alt="MarketShop Template" title="MarketShop Template" class="img-thumbnail" /></div>
            <div class="col-sm-3"><strong>MarketShop Template</strong><br />
              <address>
              Central Square,<br />
              22 Hoi Wing Road,<br />
              New Delhi,<br />
              India
              </address>
            </div>
            <div class="col-sm-3"><strong>Telephone</strong><br>
              +91 9898989898<br />
              <br />
            </div>
            <div class="col-sm-3"> <strong>Opening Times</strong><br />
              24X7 Customer Care<br />
              <br />
              <strong>Comments</strong><br />
              This field is for any special notes you would like to tell the customer i.e. Store does not accept cheques. </div>
          </div>
          <form class="form-horizontal">
            <fieldset>
              <h3 class="subtitle">Send us an Email</h3>
              <div class="form-group required">
                <label class="col-md-2 col-sm-3 control-label" for="input-name">Your Name</label>
                <div class="col-md-10 col-sm-9">
                  <input type="text" name="name" value="" id="input-name" class="form-control" />
                </div>
              </div>
              <div class="form-group required">
                <label class="col-md-2 col-sm-3 control-label" for="input-email">E-Mail Address</label>
                <div class="col-md-10 col-sm-9">
                  <input type="text" name="email" value="" id="input-email" class="form-control" />
                </div>
              </div>
              <div class="form-group required">
                <label class="col-md-2 col-sm-3 control-label" for="input-enquiry">Enquiry</label>
                <div class="col-md-10 col-sm-9">
                  <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control"></textarea>
                </div>
              </div>
            </fieldset>
            <div class="buttons">
              <div class="pull-right">
                <input class="btn btn-primary" type="submit" value="Submit" />
              </div>
            </div>
          </form>
        </div>
       
        <!--Middle Part End -->
      </div>
    </div>
  </div>