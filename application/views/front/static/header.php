<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<?php echo(base_url()); ?>assets/front/image/favicon.png" rel="icon" />
<title>Ray Store - Terlengkap dan Terupdate</title>
<meta name="description" content="Responsive and clean html template design for any kind of ecommerce webshop">
<!-- CSS Part Start-->
<link rel="stylesheet" type="text/css" href="<?php echo(base_url()); ?>assets/front/js/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo(base_url()); ?>assets/front/css/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo(base_url()); ?>assets/front/css/stylesheet.css" />
<link rel="stylesheet" type="text/css" href="<?php echo(base_url()); ?>assets/front/css/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="<?php echo(base_url()); ?>assets/front/css/owl.transitions.css" />
<link rel="stylesheet" type="text/css" href="<?php echo(base_url()); ?>assets/front/css/responsive.css" />
<link rel="stylesheet" type="text/css" href="<?php echo(base_url()); ?>assets/front/css/stylesheet-skin3.css" />
<link rel="stylesheet" type="text/css" href="<?php echo(base_url()); ?>assets/front/js/swipebox/src/css/swipebox.min.css">
<link href='//fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
<!-- CSS Part End-->
</head>
<body>
<div class="wrapper-wide">
	<div id="header">
    <!-- Top Bar Start-->
    <nav id="top" class="htop">
      <div class="container">
        <div class="row"> <span class="drop-icon visible-sm visible-xs"><i class="fa fa-align-justify"></i></span>
          <div class="pull-left flip left-top">
            <div class="links">
              <ul>
                <li class="mobile"><i class="fa fa-phone"></i>+91 9898777656</li>
                <li class="email"><a href="mailto:info@marketshop.com"><i class="fa fa-envelope"></i>info@marketshop.com</a></li>
               
              
              </ul>
            </div>                     
          </div>
          <div id="top-links" class="nav pull-right flip">
            <ul>
              <li><a href="<?php echo(base_url()) ?>login">Login</a></li>
              <li><a href="<?php echo(base_url()); ?>register">Register</a></li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
    <!-- Top Bar End-->
    <!-- Header Start-->
    <header class="header-row">
      <div class="container">
        <div class="table-container">
          <!-- Logo Start -->
          <div class="col-table-cell col-lg-4 col-md-4 col-sm-12 col-xs-12 inner">
            <div id="logo"><a href="<?php echo(base_url()); ?>dashboard"><img class="img-responsive" src="<?php echo(base_url()); ?>assets/front/image/logo-ray.png" title="MarketShop" alt="MarketShop" /></a></div>
          </div>
          <!-- Logo End -->
          <!-- Search Start-->
          <div class="col-table-cell col-lg-5 col-md-5 col-md-push-0 col-sm-6 col-sm-push-6 col-xs-12">
            <div id="search" class="input-group">
              <input id="filter_name" type="text" name="search" value="" placeholder="Search" class="form-control input-lg" />
              <button type="button" class="button-search" id="btn_search_gb"><a href="#"><i class="fa fa-search" ></i></a></button>
            </div>
          </div>
          <!-- Search End-->
          <!-- Mini Cart Start-->
          <div class="col-table-cell col-lg-3 col-md-3 col-md-pull-0 col-sm-6 col-sm-pull-6 col-xs-12 inner">
            <div id="cart">
              <button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="heading dropdown-toggle"> <span class="cart-icon pull-left flip"></span> <span id="cart-total">Your Cart</span> &nbsp <span class="badge badge-danger">2</span></button>
              <ul class="dropdown-menu">
                <li>
                  <table class="table">
                    <tbody>
                      <!-- Timer Set -->
                      <div class="alert alert-danger"><i class="fa fa-clock-o"></i><h3 class="text-right"> 01:58 </h3></div>
                      <tr>
                      <!-- end timer set -->
                        <td class="text-center"><a href="product.html"><img class="img-thumbnail" title="Xitefun Causal Wear Fancy Shoes" alt="Xitefun Causal Wear Fancy Shoes" src="<?php echo(base_url()); ?>assets/front/image/product/sony_vaio_1-50x50.jpg"></a></td>
                        <td class="text-left"><a href="product.html">Xitefun Causal Wear Fancy Shoes</a></td>
                        <td class="text-right">x 1</td>
                        <td class="text-right">$902.00</td>
                        <td class="text-center"><button class="btn btn-danger btn-xs remove" title="Remove" onClick="" type="button"><i class="fa fa-times"></i></button></td>
                      </tr>
                      <tr>
                        <td class="text-center"><a href="product.html"><img class="img-thumbnail" title="Aspire Ultrabook Laptop" alt="Aspire Ultrabook Laptop" src="<?php echo(base_url()); ?>assets/front/image/product/samsung_tab_1-50x50.jpg"></a></td>
                        <td class="text-left"><a href="product.html">Aspire Ultrabook Laptop</a></td>
                        <td class="text-right">x 1</td>
                        <td class="text-right">$230.00</td>
                        <td class="text-center"><button class="btn btn-danger btn-xs remove" title="Remove" onClick="" type="button"><i class="fa fa-times"></i></button></td>
                      </tr>
                    </tbody>
                  </table>
                </li>
                <li>
                  <div>
                   
                    <p class="checkout"><a href="<?php echo(base_url()) ?>cart" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> View Cart</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo(base_url()); ?>checkout" class="btn btn-success"><i class="fa fa-share"></i> Checkout</a></p>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <!-- Mini Cart End-->
        </div>
      </div>
    </header>
    <!-- Header End-->
    <!-- Main Menu Start-->
    <nav id="menu" class="navbar">
      <div class="container">
        <div class="navbar-header"> <span class="visible-xs visible-sm"> Menu <b></b></span></div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav">
            <li><a class="home_link" title="Home" href="index.php"><span>Home</span></a></li>
            <li class="dropdown"><a>Ready Stock</a>
              <div class="dropdown-menu">
                <ul>
                  <li> <a href="<?php echo(base_url()); ?>ready_stock/all">All</a></li>
                  <?php $kategori = $this->kategori->listKategori(); 
                    foreach ($kategori as $key) {
                      ?>
                      <li> <a href="<?=(base_url()."ready_stock/$key->id_kategori"); ?>"><?=$key->name ?></a></li>
                      <?php
                    }
                  ?>
                </ul>
              </div>
            </li>
            <li class="dropdown"><a href="<?php echo(base_url()); ?>pre_order">Pre Order</a>
              
            </li>
            <li class="menu_brands dropdown"><a href="#">Brands</a>
              <div class="dropdown-menu">
                <?php 
                  $brand = $this->brand->listBrand();
                  foreach ($brand as $key ) {
                    ?>

                <div class="col-lg-1 col-md-2 col-sm-3 col-xs-6"><a href="<?=(base_url().'brand/'.$key->id_brand) ?>"><img width="60" height="60" src="<?php echo(base_url().$key->icon); ?>" title="<?php echo ($key->name); ?>" alt="<?php echo ($key->name); ?>" /></a><a href="<?=(base_url().'brand/'.$key->id_brand) ?>"><?php echo ($key->name); ?></a></div>
                <?php
                   } 
                ?>
              </div>
            </li>
            <li class="contact-link"><a href="<?php echo(base_url()) ?>faq">FAQs</a></li>
            <li class="contact-link"><a href="<?php echo(base_url()) ?>contact_us">Contact Us</a></li>            
            <li class="custom-link-right dropdown wrap_custom_block hidden-sm hidden-xs sub"><a>My Account</a>
              <?php if(isset($_SESSION['customer_id'])) { ?>
              <span class="submore"></span><div class="dropdown-menu custom_block" style="display: none;">
                <ul>
                  <li>
                    <table width="250">
                      <tbody>
                        <tr>
                          <td colspan="2" style="text-align: center;"><h4><?=(isset($_SESSION['customer_name']) ? $_SESSION['customer_name']:"") ?></h4></td>
                        </tr>
                        <tr style="text-align: center;">
                          <td><p class=""><a href="<?php echo(base_url()) ?>login/logout" class="btn btn-default"><i class="fa fa-share"></i> Logout</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo(base_url());?>account" class="btn btn-default"><i class="fa fa-user "></i> Profile</a></p></td>                          
                        </tr>
                      </tbody>
                    </table>
                  </li>
                </ul>
              </div>
              <?php } ?>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Main Menu End-->
  </div>
  <input type="hidden" id="baselink" value="<?=base_url() ?>">