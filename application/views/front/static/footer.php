  <!--Footer Start-->
  <footer id="footer">
    <div class="fpart-first">
      <div class="container">
        <div class="row">
          <div class="contact col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <h5>Contact Details</h5>
            <ul>
              <li class="address"><i class="fa fa-map-marker"></i>Central Square, 22 Hoi Wing Road, New Delhi, India</li>
              <li class="mobile"><i class="fa fa-phone"></i>+91 9898777656</li>
              <li class="email"><i class="fa fa-envelope"></i>Send email via our <a href="<?php echo (base_url()); ?>contact_us">Contact Us</a>
            </ul>
          </div>
          <div class="column col-lg-2 col-md-2 col-sm-3 col-xs-12">
            <h5>Information</h5>
            <ul>
              <li><a href="<?php echo (base_url()); ?>about_us">About Us</a></li>             
              <li><a href="<?php echo (base_url()); ?>about_us">Privacy Policy</a></li>
              <li><a href="<?php echo (base_url()); ?>about_us">Terms &amp; Conditions</a></li>
            </ul>
          </div>
          <div class="column col-lg-2 col-md-2 col-sm-3 col-xs-12">
            <h5>Customer Service</h5>
            <ul>
              <li><a href="<?php echo (base_url()); ?>contact_us">Contact Us</a></li>
             
            </ul>
          </div>
          <div class="column col-lg-2 col-md-2 col-sm-3 col-xs-12">
            <h5>Extras</h5>
            <ul>
              <li><a href="#">Brands</a></li>
             
            </ul>
          </div>
          <div class="column col-lg-2 col-md-2 col-sm-3 col-xs-12">
            <h5>My Account</h5>
            <ul>
              <li><a href="<?php echo(base_url()) ?>account">My Account</a></li>
            
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="fpart-second">
      <div class="container">
        <div id="powered" class="clearfix">
          <div class="powered_text pull-left flip">
            <p>Marketshop Ecommerce Template © 2016 | Template By <a href="http://harnishdesign.net" target="_blank">Harnish Design</a></p>
          </div>
          <div class="social pull-right flip"> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo(base_url()); ?>assets/front/image/socialicons/facebook.png" alt="Facebook" title="Facebook"></a> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo(base_url()); ?>assets/front/image/socialicons/twitter.png" alt="Twitter" title="Twitter"> </a> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo(base_url()); ?>assets/front/image/socialicons/google_plus.png" alt="Google+" title="Google+"> </a> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo(base_url()); ?>assets/front/image/socialicons/pinterest.png" alt="Pinterest" title="Pinterest"> </a> <a href="#" target="_blank"> <img data-toggle="tooltip" src="<?php echo(base_url()); ?>assets/front/image/socialicons/rss.png" alt="RSS" title="RSS"> </a> </div>
        </div>
        
      </div>
    </div>
    <div id="back-top"><a data-toggle="tooltip" title="Back to Top" href="javascript:void(0)" class="backtotop"><i class="fa fa-chevron-up"></i></a></div>
  </footer>
  <!--Footer End-->
  <!-- Facebook Side Block Start -->
  <div id="facebook" class="fb-left sort-order-1">
    <div class="facebook_icon"><i class="fa fa-facebook"></i></div>
    <div class="fb-page" data-href="https://www.facebook.com/harnishdesign/" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true" data-show-posts="false">
      <div class="fb-xfbml-parse-ignore">
        <blockquote cite="https://www.facebook.com/harnishdesign/"><a href="https://www.facebook.com/harnishdesign/">Harnish Design</a></blockquote>
      </div>
    </div>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  </div>
  <!-- Facebook Side Block End -->
</div>
<!-- JS Part Start-->
<script type="text/javascript" src="<?php echo(base_url()); ?>assets/front/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url()); ?>assets/front/js/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url()); ?>assets/front/js/jquery.easing-1.3.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url()); ?>assets/front/js/jquery.dcjqaccordion.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url()); ?>assets/front/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url()); ?>assets/front/js/custom.js"></script>
<script type="text/javascript" src="<?php echo(base_url()); ?>assets/front/twbs-pagination-master/jquery.twbsPagination.js" type="text/javascript"></script>
<script src="<?php echo(base_url()); ?>assets/script/alert.js"></script>
<script src="<?php echo(base_url()); ?>assets/script/global.js"></script>
<!-- JS Part End-->
<?php 
  $segment = $this->uri->segment(1);
  $text = "";
  switch ($segment) {
    case 'register':
       $text .='<script src="'.base_url().'assets/script/front/register.js"></script>';
      break;
    case 'login':
       $text .='<script src="'.base_url().'assets/script/front/login.js"></script>';
      break;
    case 'product_detail':
      $text .='<script src="'.base_url().'assets/front/js/jquery.elevateZoom-3.0.8.min.js"></script>';
      $text .='<script src="'.base_url().'assets/front/js/swipebox/lib/ios-orientationchange-fix.js"></script>';
      $text .='<script src="'.base_url().'assets/front/js/swipebox/src/js/jquery.swipebox.min.js"></script>';
       $text .='<script src="'.base_url().'assets/script/front/product_detail.js"></script>';
      break;
    case 'po_detail':
      $text .='<script src="'.base_url().'assets/front/js/jquery.elevateZoom-3.0.8.min.js"></script>';
      $text .='<script src="'.base_url().'assets/front/js/swipebox/lib/ios-orientationchange-fix.js"></script>';
      $text .='<script src="'.base_url().'assets/front/js/swipebox/src/js/jquery.swipebox.min.js"></script>';
       $text .='<script src="'.base_url().'assets/script/front/po_detail.js"></script>';
      break;
    case 'ready_stock':
       $text .='<script src="'.base_url().'assets/script/front/ready_stock.js"></script>';
      break;
    case 'pre_order':
       $text .='<script src="'.base_url().'assets/script/front/pre_order.js"></script>';
      break;
    case 'brand':
       $text .='<script src="'.base_url().'assets/script/front/brand.js"></script>';
      break;
    case 'search':
       $text .='<script src="'.base_url().'assets/script/front/search.js"></script>';
      break;
    default:
      # code...
      break;
  }
  echo($text);
?>
</body>
</html>