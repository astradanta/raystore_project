<div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb" id="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo(base_url()); ?>register">Register</a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
        <!--Middle Part Start-->
        <div class="col-sm-9" id="content">
          <h1 class="title">Register Account</h1>
          <p>If you already have an account with us, please login at the <a href="login">Login Page</a>.</p>
          <form action="<?=base_url() ?>register/customer" method = "post" class="form-horizontal" id="registerForm">
            <fieldset id="account">
              <legend>Your Personal Details</legend>
              
              <div class="form-group required">
                <label for="input-firstname" class="col-sm-2 control-label">First Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" required="" id="firstname" placeholder="First Name" value="" name="firstname">
                </div>
              </div>
              <div class="form-group required">
                <label for="input-lastname" class="col-sm-2 control-label">Last Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" required="" id="lastname" placeholder="Last Name" value="" name="lastname">
                </div>
              </div>
              <div class="form-group required">
                <label for="input-email" class="col-sm-2 control-label">E-Mail</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" required="" id="email" placeholder="E-Mail" value="" name="email">
                </div>
              </div>
              <div class="form-group required">
                <label for="input-telephone" class="col-sm-2 control-label">Telephone</label>
                <div class="col-sm-10">
                  <input type="tel" class="form-control" id="no_hp" placeholder="Telephone" value="" name="no_hp">
                </div>
              </div>
             
            </fieldset>
            <fieldset id="">
              <legend>Your Address</legend>
             
              <div class="form-group required">
                <label for="input-address-1" class="col-sm-2 control-label">Address</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" required="" id="address" placeholder="Address 1" value="" name="address">
                </div>
              </div>
              <div class="form-group required">
                <label for="input-city" class="col-sm-2 control-label">City</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" required="" id="city" placeholder="City" value="" name="city">
                </div>
              </div>
              <div class="form-group required">
                <label for="input-postcode" class="col-sm-2 control-label">Post Code</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" required="" id="kode_pos" placeholder="Post Code" value="" name="kode_pos">
                </div>
              </div>
              <div class="form-group required">
                <label for="input-country" class="col-sm-2 control-label">Country</label>
                <div class="col-sm-10">
                  <select class="form-control" id="code" name="code">
                    <option value=""> --- Please Select --- </option>
                    <?php $negara = $this->web->listNegara();
                        foreach ($negara as $key ) {
                            ?>
                            <option value="<?=$key->code  ?>"> <?=$key->name ?> </option>
                            <?php
                        }
                     ?>
                  </select>
                </div>
              </div>

            <fieldset>
              <legend>Account Category</legend>
              <div class="form-group">
                <label class="col-sm-2 control-label">Reseller?</label>
                <div class="col-sm-10">
                  <label class="radio-inline">
                    <input type="radio" id="firstRadio" value="1" name="type_customer">
                    Yes</label>
                  <label class="radio-inline">
                    <input type="radio" id="secondRadio" checked="checked" value="0" name="type_customer">
                    No</label>
                </div>
              </div>
            </fieldset>
              
            </fieldset>
            <fieldset>
              <legend>Your Password</legend>
              <div class="form-group required">
                <label for="input-password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control" required="" id="password" placeholder="Password" value="" name="password">
                </div>
              </div>
              <div class="form-group required">
                <label for="input-confirm" class="col-sm-2 control-label">Password Confirm</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control" required="" id="confirm" placeholder="Password Confirm" value="" name="confirm">
                </div>
              </div>
            </fieldset>
            
            <div class="buttons">
              <div class="pull-right">
                <input type="submit" class="btn btn-primary" value="Continue">
              </div>
            </div>
          </form>
        </div>
        <!--Middle Part End -->
       
      </div>
    </div>
  </div>
<!-- footer -->