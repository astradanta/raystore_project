<div id="container">
    <div class="container">
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo(base_url()); ?>dashboard" itemprop="url"><span itemprop="title"><i class="fa fa-home"></i></span></a></li>
        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="#" itemprop="url"><span itemprop="title"><?=$produk->kategori ?></span></a></li>
        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="#" itemprop="url"><span itemprop="title"><?=$produk->nama_produk ?></span></a></li>
      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
     
        <!--Middle Part Start-->
        <div id="content" class="col-sm-12">
          <div itemscope itemtype="http://schema.org/Product">
            <h1 class="title" itemprop="name"><?=$produk->nama_produk ?></h1>
            <div class="row product-info">
              <div class="col-sm-6">
                <div class="image"><img class="img-responsive" itemprop="image" id="zoom_01" src="<?=(base_url().$produk->cover)?>" title="<?=$produk->nama_produk ?>" alt="<?=$produk->nama_produk ?>" data-zoom-image="<?=(base_url().$produk->cover)?>" /> </div>
                <div class="center-block text-center"><span class="zoom-gallery"><i class="fa fa-search"></i> Click image for Gallery</span></div>
                <div class="image-additional" id="gallery_01"> 
                  <a class="thumbnail" href="#" data-zoom-image="<?=(base_url().$produk->cover)?>" data-image="<?=(base_url().$produk->cover)?>" title="<?=$produk->nama_produk ?>"> <img src="<?=(base_url().$produk->cover)?>" title="<?=$produk->nama_produk ?>" alt = "<?=$produk->nama_produk ?>"/></a>
                  <?php if($produk->subimage != null){
                    $variable = explode(",", $produk->subimage);
                    foreach ($variable as $key){                    
                    ?>
                      <a class="thumbnail" href="#" data-zoom-image="<?=base_url().$key ?>" data-image="<?=base_url().$key ?>" title="<?=$produk->nama_produk ?>"> <img src="<?=base_url().$key ?>" title="<?=$produk->nama_produk ?>"  alt = "<?=$produk->nama_produk ?>"/></a>  
                    <?php
                    }
                  } ?></div>
              </div>
              <div class="col-sm-6">
                <ul class="list-unstyled description">
                  <li><b>Brand:</b> <a href="#"><span itemprop="brand"><?=$produk->brand?></span></a></li>
                  <li><b>Product Category:</b> <span itemprop="mpn"><?=$produk->kategori ?></span></li>
                  <li><b>Stock :</b><?=$produk->amount ?></li>
                  <li><b>Availability:</b> <span class="instock">In Stock</span></li>
                </ul>
                <ul class="price-box">
                  <li class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer"> <span itemprop="price">IDR <?=$produk->harga_jual ?>,-<span itemprop="availability" content="In Stock"></span></span></li>
                 
                  
                </ul>
                <div id="product">
                  <h3 class="subtitle">Options</h3>
                 
                  <div class="cart">
                    <div>
                      <div class="qty">
                        <label class="control-label" for="input-quantity">Qty</label>
                        <input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control" />
                        <a class="qtyBtn plus" href="javascript:void(0);">+</a><br />
                        <a class="qtyBtn mines" href="javascript:void(0);">-</a>
                        <div class="clear"></div>
                      </div>
                      <button type="button" id="button-cart" class="btn btn-primary btn-lg">Add to Cart</button>
                    </div>
                    
                  </div>
                </div>
             
                <hr>
                <!-- AddThis Button BEGIN -->
                <div class="addthis_toolbox addthis_default_style"> <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_google_plusone" g:plusone:size="medium"></a> <a class="addthis_button_pinterest_pinit" pi:pinit:layout="horizontal" pi:pinit:url="http://www.addthis.com/features/pinterest" pi:pinit:media="http://www.addthis.com/cms-content/images/features/pinterest-lg.png"></a> <a class="addthis_counter addthis_pill_style"></a> </div>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-514863386b357649"></script>
                <!-- AddThis Button END -->
              </div>
            </div>
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab-description" data-toggle="tab">Description</a></li>
             
              <li><a href="#tab-discuss" data-toggle="tab">Discuss</a></li>
            </ul>
            <div class="tab-content">
              <div itemprop="description" id="tab-description" class="tab-pane active">
                <div>
                  <p><?=$produk->deskripsi ?></p>                  
                </div>
              </div>
             
              
              <div id="tab-discuss" class="tab-pane">
               <!-- Discuss -->
               <span>USE DISCUSS PLUG IN</span>
              </div>
            </div>
            
          </div>
        </div>
        <!--Middle Part End -->
        
      </div>
    </div>
  </div>
<!-- footer -->