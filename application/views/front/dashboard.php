 <div id="container">
    <div class="container">
      <div class="row">
        <!--Middle Part Start-->
        <div id="content" class="col-xs-12">
          <div class="row">
            <div class="col-sm-8">
              <!-- Slideshow Start-->
              <div class="slideshow single-slider owl-carousel">
                <?php $slider = $this->web->listSlider(); 
                  $count = 1;
                  foreach ($slider as $key ) {
                    ?>
                    <div class="item"> <a href="<?php echo($key->link); ?>"><img class="img-responsive" src="<?php echo(base_url().$key->image); ?>" alt="banner <?php echo($count); ?>" /></a></div>
                    <?php
                    $count++;
                  }
                ?>
              </div>
              <!-- Slideshow End-->
            </div>
            <div class="col-sm-4 pull-right flip">
              <div class="marketshop-banner">
                <div class="row">
                  <?php $banner = $this->web->listBanner();
                      foreach ($banner as $key ) {
                        ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <a href="<?= $key->link ?>"><img title="banner" alt="banner" src="<?= base_url().$key->image ?>"></a></div>

                        <?php
                      }
                   ?>
                </div>
              </div>
            </div>
          </div>
          <!-- Bestsellers Product Start-->
          <h3 class="subtitle">Bestsellers</h3>
          <div class="owl-carousel product_carousel">
            <!-- Product Home -->
            <?php $produk = $this->produk->produkBestseller();
                  foreach ($produk as $key) {
                    ?>
                      <div class="product-thumb clearfix">
                        <div class="image"><a href="<?=(base_url()."product_detail/".$key->id_produk)?>"><img src="<?=(base_url().$key->cover)?>" alt="<?=$key->nama_produk?>" title="<?=$key->nama_produk?>" class="img-responsive" /></a></div>
                        <div class="caption">
                          <h4><a href="<?=(base_url()."product_detail/".$key->id_produk)?>"><?=$key->nama_produk?></a></h4>
                          <p class="price"> IDR <?=$key->harga_jual?> </p>

                        </div>
                        <div class="button-group">
                          <button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>
                         
                        </div>
                      </div>
                    <?php
                  }
             ?>
          </div>
          <!-- Featured Product End-->
          
         
         
         
         
        </div>
        <!--Middle Part End-->
      </div>
    </div>
  </div>