<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'F_Dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['cpanel'] = "Admin/A_Dashboard";
$route['cpanel/dashboard'] = "Admin/A_Dashboard";

$route['cpanel/brand'] = "Admin/A_Brand";
$route['cpanel/brand/list'] = "Admin/A_Brand/list";
$route['cpanel/brand/inc'] = "Admin/A_Brand/inc";
$route['cpanel/brand/add'] = "Admin/A_Brand/add";
$route['cpanel/brand/edit'] = "Admin/A_Brand/edit";
$route['cpanel/brand/detail'] = "Admin/A_Brand/detail";
$route['cpanel/brand/delete'] = "Admin/A_Brand/delete";

$route['cpanel/kategori'] = "Admin/A_Kategori";
$route['cpanel/kategori/list'] = "Admin/A_Kategori/list";
$route['cpanel/kategori/inc'] = "Admin/A_Kategori/inc";
$route['cpanel/kategori/add'] = "Admin/A_Kategori/add";
$route['cpanel/kategori/edit'] = "Admin/A_Kategori/edit";
$route['cpanel/kategori/detail'] = "Admin/A_Kategori/detail";
$route['cpanel/kategori/delete'] = "Admin/A_Kategori/delete";

$route['cpanel/customer'] = "Admin/A_Customer";
$route['cpanel/kategori'] = "Admin/A_Kategori";

$route['cpanel/product'] = "Admin/A_Product";
$route['cpanel/product/list'] = "Admin/A_Product/list";
$route['cpanel/product/inc'] = "Admin/A_Product/inc";
$route['cpanel/product/add'] = "Admin/A_Product/add";
$route['cpanel/product/edit'] = "Admin/A_Product/edit";
$route['cpanel/product/detail'] = "Admin/A_Product/detail";
$route['cpanel/product/delete'] = "Admin/A_Product/delete";
$route['cpanel/product/checkImage'] = "Admin/A_Product/checkImage";
$route['cpanel/product/image'] = "Admin/A_Product/image";
$route['cpanel/product/deleteImage'] = "Admin/A_Product/deleteImage";

$route['cpanel/product_po'] = "Admin/A_Product_po";
$route['cpanel/product_po/list'] = "Admin/A_Product_po/list";
$route['cpanel/product_po/inc'] = "Admin/A_Product_po/inc";
$route['cpanel/product_po/add'] = "Admin/A_Product_po/add";
$route['cpanel/product_po/edit'] = "Admin/A_Product_po/edit";
$route['cpanel/product_po/detail'] = "Admin/A_Product_po/detail";
$route['cpanel/product_po/delete'] = "Admin/A_Product_po/delete";
$route['cpanel/product_po/checkImage'] = "Admin/A_Product_po/checkImage";
$route['cpanel/product_po/image'] = "Admin/A_Product_po/image";
$route['cpanel/product_po/deleteImage'] = "Admin/A_Product_po/deleteImage";

$route['cpanel/summary_product'] = "Admin/A_Sumary_product";
$route['cpanel/summary_product/list'] = "Admin/A_Sumary_product/list";
$route['cpanel/summary_product/inc'] = "Admin/A_Sumary_product/inc";
$route['cpanel/summary_product/add'] = "Admin/A_Sumary_product/add";
$route['cpanel/summary_product/edit'] = "Admin/A_Sumary_product/edit";
$route['cpanel/summary_product/detail'] = "Admin/A_Sumary_product/detail";
$route['cpanel/summary_product/delete'] = "Admin/A_Sumary_product/delete";

$route['cpanel/list_po'] = "Admin/A_List_po";
$route['cpanel/summary_po'] = "Admin/A_Summary_po";
$route['cpanel/order'] = "Admin/A_Order";
$route['cpanel/transaksi'] = "Admin/A_Transaksi";
$route['cpanel/report'] = "Admin/A_Report";

$route['cpanel/user'] = "Admin/A_User";
$route['cpanel/user/list'] = "Admin/A_User/list";
$route['cpanel/user/inc'] = "Admin/A_User/inc";
$route['cpanel/user/role'] = "Admin/A_User/role";
$route['cpanel/user/add'] = "Admin/A_User/add";
$route['cpanel/user/edit'] = "Admin/A_User/edit";
$route['cpanel/user/detail'] = "Admin/A_User/detail";
$route['cpanel/user/delete'] = "Admin/A_User/delete";

$route['cpanel/slider'] = "Admin/A_Web_Setup/slider";
$route['cpanel/slider/:any'] = "Admin/A_Web_Setup/slider_action";

$route['cpanel/banner'] = "Admin/A_Web_Setup/banner";
$route['cpanel/banner/:any'] = "Admin/A_Web_Setup/banner_action";

$route['cpanel/web'] = "Admin/A_Web_Setup";

$route['cpanel/login'] = "Admin/A_Login";
$route['cpanel/login/auth'] = "Admin/A_Login/auth";
$route['cpanel/login/forget'] = "Admin/A_Login/forget";


$route['dashboard'] = "F_Dashboard";
$route['about_us'] = "Front/F_About";
$route['account'] = "Front/F_Account";
$route['cart'] = "Front/F_Cart";
$route['checkout'] = "Front/F_Checkout";
$route['contact_us'] = "Front/F_Contact";
$route['faq'] = "Front/F_Faq";

$route['login'] = "Front/F_Login";
$route['login/logout'] = "Front/F_Login/logout";
$route['login/auth'] = "Front/F_Login/auth";

$route['pre_order'] = "Front/F_Pre_order";
$route['pre_order/:any'] = "Front/F_Pre_order";

$route['product_detail/:any'] = "Front/F_Product_detail";

$route['po_detail/:num'] = "Front/F_Po_detail";

$route['ready_stock'] = "Front/F_Ready_stock";
$route['ready_stock/:any'] = "Front/F_Ready_stock";

$route['brand'] = "Front/F_Brand";
$route['brand/:any'] = "Front/F_Brand";

$route['register'] = "Front/F_Register";
$route['register/customer'] = "Front/F_Register/customer";

$route['search'] = "Front/F_Search";
$route['search/:any'] = "Front/F_Search";



