-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2018 at 07:39 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `garagedo_db_raystore`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_barang_po`
--

CREATE TABLE `tb_barang_po` (
  `id_barang_po` int(11) NOT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_brand` int(11) DEFAULT NULL,
  `nama_barang` varchar(225) DEFAULT NULL,
  `harga_beli` bigint(25) DEFAULT NULL,
  `harga_jual` bigint(25) DEFAULT NULL,
  `cover` text,
  `deskripsi` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_barang_po`
--

INSERT INTO `tb_barang_po` (`id_barang_po`, `id_kategori`, `id_brand`, `nama_barang`, `harga_beli`, `harga_jual`, `cover`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 2, 4, 'Net SE Shoes 302297', 1200000, 1300000, 'assets/img/produk/1542608611M7hCr.jpg', 'Sepatu dari DC', '2018-11-19 07:23:31', '2018-11-19 07:32:27');

-- --------------------------------------------------------

--
-- Table structure for table `tb_brand`
--

CREATE TABLE `tb_brand` (
  `id_brand` int(11) NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `icon` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_brand`
--

INSERT INTO `tb_brand` (`id_brand`, `name`, `icon`, `created_at`, `updated_at`) VALUES
(3, 'Adidas', 'assets/img/brand/1542264117LytDa.png', '2018-11-15 07:40:45', '2018-11-15 07:41:57'),
(4, 'DC', 'assets/img/brand/1542340549ElCHJ.png', '2018-11-16 04:55:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_customer`
--

CREATE TABLE `tb_customer` (
  `id_customer` int(11) NOT NULL,
  `nama` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `no_hp` varchar(25) DEFAULT NULL,
  `alamat` varchar(225) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `kode_pos` varchar(10) DEFAULT NULL,
  `negara` varchar(50) DEFAULT NULL,
  `type_customer` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_order`
--

CREATE TABLE `tb_detail_order` (
  `id_detail_order` int(11) NOT NULL,
  `id_order` int(11) DEFAULT NULL,
  `id_produk` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `sub_total` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_po`
--

CREATE TABLE `tb_detail_po` (
  `id_detail_po` int(11) NOT NULL,
  `id_po` int(11) DEFAULT NULL,
  `id_barang_po` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `sub_total` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_transaksi`
--

CREATE TABLE `tb_detail_transaksi` (
  `id_detail_transaksi` int(11) NOT NULL,
  `id_transaksi` int(11) DEFAULT NULL,
  `produk` varchar(225) DEFAULT NULL,
  `qty` int(5) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `sub_total` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_history_po`
--

CREATE TABLE `tb_history_po` (
  `id_history_po` int(11) NOT NULL,
  `tanggal_po` date DEFAULT NULL,
  `customer` varchar(155) DEFAULT NULL,
  `barang_po` varchar(255) DEFAULT NULL,
  `qty` int(5) DEFAULT NULL,
  `harga_beli` bigint(20) DEFAULT NULL,
  `harga_jual` bigint(20) DEFAULT NULL,
  `margin` bigint(20) DEFAULT NULL,
  `sub_total` bigint(20) DEFAULT NULL,
  `status_po` varchar(155) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id_kategori` int(11) NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id_kategori`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Jaket', '2018-11-15 05:10:10', '2018-11-15 05:14:10'),
(2, 'Sepatu', '2018-11-19 07:22:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_konfirmasi_pembayaran`
--

CREATE TABLE `tb_konfirmasi_pembayaran` (
  `id_konfirmasi` int(11) NOT NULL,
  `id_order` int(11) DEFAULT NULL,
  `tgl_konfirmasi` date DEFAULT NULL,
  `jumlah_bayar` bigint(25) DEFAULT NULL,
  `bank` varchar(155) DEFAULT NULL,
  `atas_nama` varchar(155) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `status_konfirmasi` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `id_order` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `status_order` varchar(50) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `type_pembayaran` varchar(100) DEFAULT NULL,
  `type_shipping` varchar(100) DEFAULT NULL,
  `alamat_shipping` varchar(255) DEFAULT NULL,
  `biaya_shipping` bigint(20) DEFAULT NULL,
  `grand_total` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_po`
--

CREATE TABLE `tb_po` (
  `id_po` int(11) NOT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `tanggal_po` date DEFAULT NULL,
  `grand_total` bigint(20) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `status_po` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_po_image`
--

CREATE TABLE `tb_po_image` (
  `id_image` int(11) NOT NULL,
  `id_barang_po` int(11) NOT NULL,
  `image` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_po_image`
--

INSERT INTO `tb_po_image` (`id_image`, `id_barang_po`, `image`) VALUES
(1, 1, 'assets/img/produk/1542609360rS7WJ.jpg'),
(2, 1, 'assets/img/produk/1542609360Uilw0.jpg'),
(4, 1, 'assets/img/produk/1542609918RWVD4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_product_image`
--

CREATE TABLE `tb_product_image` (
  `id_image` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_product_image`
--

INSERT INTO `tb_product_image` (`id_image`, `id_produk`, `image`) VALUES
(12, 6, 'assets/img/produk/15424345060JSBH.jpg'),
(13, 6, 'assets/img/produk/1542434679CZlrD.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_produk`
--

CREATE TABLE `tb_produk` (
  `id_produk` int(11) NOT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_brand` int(11) DEFAULT NULL,
  `nama_produk` varchar(225) DEFAULT NULL,
  `harga_beli` bigint(25) DEFAULT NULL,
  `harga_jual` bigint(25) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `cover` text,
  `deskripsi` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_produk`
--

INSERT INTO `tb_produk` (`id_produk`, `id_kategori`, `id_brand`, `nama_produk`, `harga_beli`, `harga_jual`, `stok`, `cover`, `deskripsi`, `created_at`, `updated_at`) VALUES
(5, 1, 4, 'Hoodie Premium', 900000, 1300000, NULL, 'assets/img/produk/15424344160lfWP.jpg', 'Hoodie kekinian dari adidas dengan bahan yang nyaman', '2018-11-17 06:56:05', '2018-11-17 07:00:16'),
(6, 1, 4, 'DC Hoodie Grey', 700000, 900000, NULL, 'assets/img/produk/1542434487Ye9JL.jpg', 'sample', '2018-11-17 07:01:27', '2018-11-17 08:54:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_role`
--

CREATE TABLE `tb_role` (
  `id_role` int(11) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_role`
--

INSERT INTO `tb_role` (`id_role`, `description`) VALUES
(1, 'Customer'),
(2, 'Admin'),
(3, 'Super Admin');

-- --------------------------------------------------------

--
-- Table structure for table `tb_stock`
--

CREATE TABLE `tb_stock` (
  `id_stock` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_stock`
--

INSERT INTO `tb_stock` (`id_stock`, `id_produk`, `type`, `amount`, `description`, `created_at`, `updated_at`) VALUES
(5, 5, 0, 5, 'First add stock ', '2018-11-17 06:56:05', '2018-11-20 07:17:33'),
(7, 6, 0, 5, 'sample', '2018-11-20 06:22:44', '2018-11-20 07:13:19'),
(10, 6, 1, -2, 'Rusak', '2018-11-20 07:34:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_summary_stok`
--

CREATE TABLE `tb_summary_stok` (
  `id_summary_stok` int(11) NOT NULL,
  `nama_barang` varchar(255) DEFAULT NULL,
  `type_summary` varchar(100) DEFAULT NULL,
  `qty` int(10) DEFAULT NULL,
  `stok_terakhir` int(10) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi`
--

CREATE TABLE `tb_transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `tanggal_transaksi` date DEFAULT NULL,
  `customer` varchar(225) DEFAULT NULL,
  `type_shipping` varchar(150) DEFAULT NULL,
  `alamat_shipping` varchar(255) DEFAULT NULL,
  `biaya_shipping` bigint(20) DEFAULT NULL,
  `type_pembayaran` varchar(100) DEFAULT NULL,
  `grand_total` bigint(20) DEFAULT NULL,
  `note` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `id_role` int(2) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `icon` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `id_customer`, `id_role`, `name`, `email`, `password`, `icon`, `created_at`, `updated_at`) VALUES
(1, NULL, 3, 'Test Akun', 'astra.danta@gmail.com', '034cb6ad448a16e12c20adb23fc3f9ce2631a52f9b4ce3bc48846e8a6b4c6d374b414923e99220c12ea2e372aab1036143c6fcb0fa64e951bbd8051d8addd15eldY6h+fUDoilpQxSPXkbN2nNwZS882Tv6J0M9R+m+M4=', NULL, '2018-11-07 00:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_barang_po`
--
ALTER TABLE `tb_barang_po`
  ADD PRIMARY KEY (`id_barang_po`);

--
-- Indexes for table `tb_brand`
--
ALTER TABLE `tb_brand`
  ADD PRIMARY KEY (`id_brand`);

--
-- Indexes for table `tb_customer`
--
ALTER TABLE `tb_customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `tb_detail_order`
--
ALTER TABLE `tb_detail_order`
  ADD PRIMARY KEY (`id_detail_order`);

--
-- Indexes for table `tb_detail_po`
--
ALTER TABLE `tb_detail_po`
  ADD PRIMARY KEY (`id_detail_po`);

--
-- Indexes for table `tb_detail_transaksi`
--
ALTER TABLE `tb_detail_transaksi`
  ADD PRIMARY KEY (`id_detail_transaksi`);

--
-- Indexes for table `tb_history_po`
--
ALTER TABLE `tb_history_po`
  ADD PRIMARY KEY (`id_history_po`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `tb_konfirmasi_pembayaran`
--
ALTER TABLE `tb_konfirmasi_pembayaran`
  ADD PRIMARY KEY (`id_konfirmasi`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `tb_po`
--
ALTER TABLE `tb_po`
  ADD PRIMARY KEY (`id_po`);

--
-- Indexes for table `tb_po_image`
--
ALTER TABLE `tb_po_image`
  ADD PRIMARY KEY (`id_image`),
  ADD KEY `r_po_image` (`id_barang_po`);

--
-- Indexes for table `tb_product_image`
--
ALTER TABLE `tb_product_image`
  ADD PRIMARY KEY (`id_image`),
  ADD KEY `r_produk_image` (`id_produk`);

--
-- Indexes for table `tb_produk`
--
ALTER TABLE `tb_produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `tb_role`
--
ALTER TABLE `tb_role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `tb_stock`
--
ALTER TABLE `tb_stock`
  ADD PRIMARY KEY (`id_stock`),
  ADD KEY `r_stock` (`id_produk`);

--
-- Indexes for table `tb_summary_stok`
--
ALTER TABLE `tb_summary_stok`
  ADD PRIMARY KEY (`id_summary_stok`);

--
-- Indexes for table `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_barang_po`
--
ALTER TABLE `tb_barang_po`
  MODIFY `id_barang_po` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_brand`
--
ALTER TABLE `tb_brand`
  MODIFY `id_brand` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_customer`
--
ALTER TABLE `tb_customer`
  MODIFY `id_customer` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_detail_order`
--
ALTER TABLE `tb_detail_order`
  MODIFY `id_detail_order` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_detail_po`
--
ALTER TABLE `tb_detail_po`
  MODIFY `id_detail_po` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_detail_transaksi`
--
ALTER TABLE `tb_detail_transaksi`
  MODIFY `id_detail_transaksi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_history_po`
--
ALTER TABLE `tb_history_po`
  MODIFY `id_history_po` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_konfirmasi_pembayaran`
--
ALTER TABLE `tb_konfirmasi_pembayaran`
  MODIFY `id_konfirmasi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_po`
--
ALTER TABLE `tb_po`
  MODIFY `id_po` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_po_image`
--
ALTER TABLE `tb_po_image`
  MODIFY `id_image` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_product_image`
--
ALTER TABLE `tb_product_image`
  MODIFY `id_image` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tb_produk`
--
ALTER TABLE `tb_produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_role`
--
ALTER TABLE `tb_role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_stock`
--
ALTER TABLE `tb_stock`
  MODIFY `id_stock` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tb_summary_stok`
--
ALTER TABLE `tb_summary_stok`
  MODIFY `id_summary_stok` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_po_image`
--
ALTER TABLE `tb_po_image`
  ADD CONSTRAINT `r_po_image` FOREIGN KEY (`id_barang_po`) REFERENCES `tb_barang_po` (`id_barang_po`);

--
-- Constraints for table `tb_product_image`
--
ALTER TABLE `tb_product_image`
  ADD CONSTRAINT `r_produk_image` FOREIGN KEY (`id_produk`) REFERENCES `tb_produk` (`id_produk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_stock`
--
ALTER TABLE `tb_stock`
  ADD CONSTRAINT `r_stock` FOREIGN KEY (`id_produk`) REFERENCES `tb_produk` (`id_produk`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
