$(document).ready(function(){
	var baselink = $("#baselink").val();

	function changeState(state){
		if(state == 0){
			$("#loginPanel").slideUp();
			$("#forgotPanel").slideDown();
		} else {
			$("#forgotPanel").slideUp();
			$("#loginPanel").slideDown();
		}
	}
	$("#forgotLink").click(function(e){
		e.preventDefault();
		changeState(0);
	});
	$("#cancel_btn").click(function(){
		changeState(1);
		clearInput();
	});
	$("#loginForm").submit(function(e){
		e.preventDefault();
		$.ajax({
				type: "POST",
				url: $(this).prop("action"),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					data = jQuery.parseJSON(response);
					if(data.status == 1){
						alert("Login Success");
						clearInput();
						window.location.href = baselink;
					} else {
						alert(data.message);
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});	
	});
	function clearInput(){
		$("input[type=text]").val('');
	}
	$("#forgetForm").submit(function(e){
		e.preventDefault();
		$.ajax({
				type: "POST",
				url: $(this).prop("action"),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					console.log(response);
					data = jQuery.parseJSON(response);
					if(data.status == 1){
						alert("Login Success");
						clearInput();
						changeState(1);
					} else {
						alert(data.message);
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});
	});
});