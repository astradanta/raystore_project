$(document).ready(function(){
	var baselink = $("#baselink").val();
		keyword = "";
		emptyImage = $("#baselink").val()+"../assets/dist/img/empty.png";
	list();
	userRole();
	function customTable(){
		text = '<div class="input-group input-group-sm pull-right" style="width: 150px;"><input type="text" name="table_search" class="form-control pull-right" placeholder="Search" id="search_keyword"><div class="input-group-btn"><button type="btn" class="btn btn-default" id="search_btn"><i class="fa fa-search"></i></button></div></div>';
		$('#tableUser_filter').parent().append(text);
		$("#tableUser_wrapper").on('click','#search_btn',function(){
			$("#tableUser").DataTable().search( $("#search_keyword").val() ).draw();
		});
	}
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'user/list',
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
				$("#tableUser").DataTable({});
				$('#tableUser_filter').attr('style','display:none')
				customTable();
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});	
	}
	function parseList(item){
		var text = "<tr>"+
                    "<td>"+item.id_user+"</td>"+
                    "<td>"+item.name+"</td>"+
                    "<td>"+item.description+"</td>"+
                    "<td>"+item.email+"</td>"+
                    "<td>**********</td>"+
                    "<td class='text-center'>"+
                    	'<button class="btn btn-danger" id="deleteUser" data-id="'+item.id_user+'" style="margin: 5px;"><i class="fa fa-trash"></i></button>'+
                        '<button class="btn btn-warning" id="editUser" data-id="'+item.id_user+'" style="margin: 5px;"><i class="fa fa-pencil"></i></button>'+
                        '<button class="btn btn-info" id="detailUser" data-id="'+item.id_user+'" style="margin: 5px;"><i class="fa fa-eye"></i></button>'+
                    '</td>'+
                 "</tr>";
        $("#listView").append(text);
	}
	$("#addBtn").click(function(e){
		e.preventDefault();

		$.ajax({
			type: "POST",
			url: baselink+'user/inc',
			cache: false,
			success: function(response){
				$("#kode").val(response);
				$('#inputModal').modal('show');
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});	
	});
	function userRole(){
		$.ajax({
			type: "POST",
			url: baselink+'user/role',
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#role").html('<option value="0">Select user Role</option>');
				$("#roleEdit").html('<option value="0">Select user Role</option>');
				$.each(data,function(i,item){
					$("#role").append('<option value="'+item.id_role+'">'+item.description+'</option>')
					$("#roleEdit").append('<option value="'+item.id_role+'">'+item.description+'</option>')
				});

			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	}
	$("#displayIcon").click(function(){
		$("#icon").click();
	});
	$("#icon").change(function(){
		 readURL(this);
	});
	function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#displayIcon').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }

	}
	$("#inputForm").submit(function(e){
		e.preventDefault();
			$("#iconValidate").attr("style","color:red;display:none;")
			if($('#icon')[0].files[0] == undefined){
				$("#inputContent").alert("error","You must input user icon");
				return false
			}
			if($("#password").val() != $("#retype").val()){
				$("#inputContent").alert("error","The password is not match");
				return false
			}
			var fsize = $('#icon')[0].files[0].size; //get file size
			var ftype = $('#icon')[0].files[0].type; // get file type			
			switch(ftype) {
	            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
	                break;
	            default:
	                $("#iconValidate").html("<b>"+ftype+"</b> is not image file");
	                $("#iconValidate").attr("style","color:red;display:block;");
					return false
			        }

		        if(fsize>1048576) {
					$("#iconValidate").html("<b>"+bytesToSize(fsize) +"</b> The file size is to big  <br /> Please use another image file");
					$("#iconValidate").attr("style","color:red;display:block;")
					return false
				}
			$.ajax({
				type: "POST",
				url: $(this).attr('action'),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == 1){
						$("#content").alert("success","Success adding new item");
						$("#inputModal").modal("hide");
						$("#tableUser").DataTable().destroy();
						list();
					}else{
						$("#content").alert("error","Failed adding new item")
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});

    });
    $("#editForm").submit(function(e){
		e.preventDefault();
			$("#iconEditValidate").attr("style","color:red;display:none;")
			if($('#iconEdit')[0].files[0] != undefined){
				var fsize = $('#iconEdit')[0].files[0].size; //get file size
				var ftype = $('#iconEdit')[0].files[0].type; // get file type			
				switch(ftype) {
		            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
		                break;
		            default:
		                $("#iconEditValidate").html("<b>"+ftype+"</b> is not image file");
		                $("#iconEditValidate").attr("style","color:red;display:block;");
						return false
				        }

			        if(fsize>1048576) {
						$("#iconEditValidate").html("<b>"+bytesToSize(fsize) +"</b> The file size is to big  <br /> Please use another image file");
						$("#iconEditValidate").attr("style","color:red;display:block;")
						return false
					}
			}
			if($("#passwordEdit").val() != $("#retypeEdit").val()){
				$("#editContent").alert("error","The password is not match");
				return false
			}
			
			$.ajax({
				type: "POST",
				url: $(this).attr('action'),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == 1){
						$("#content").alert("success","Success editing item");
						$("#editModal").modal("hide");
						$("#tableUser").DataTable().destroy();
						list();
					}else{
						$("#content").alert("error","Failed editing item")
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});

    });
    function clearForm(){
    	$('input[type="text"]').val('');
  		$('input[type="email"]').val('');
  		$('input[type="password"]').val('');
    	$("#displayIcon").attr("src",emptyImage);
    	$("#displayEditIcon").attr("src",emptyImage);
    	$("#displayDetailIcon").attr("src",emptyImage);
    	$("#role").val($("#role option:first").val());
    	$("#roleEdit").val($("#roleEdit option:first").val());
    }
	$("#listView").on('click','#editUser',function(){
		var id_user = $(this).attr('data-id');
		detail(id_user);
	});	
	$('#inputModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})
	$('#editModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})
	$('#detailModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})

	function detail(id_user,type = 0){
		$.ajax({
			type: "POST",
			url: baselink+'user/detail',
			data:{"id_user":id_user},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if(type == 0){
					parseDetail(data[0]);
				} else {
					parseDetail(data[0],1);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 
	}
	function parseDetail(data,type = 0){
		if(type == 0){		
			$("#id_user").val(data.id_user);
			$("#displayEditIcon").attr("src",data.icon);
			$("#kodeEdit").val(data.id_user);
			$("#nameEdit").val(data.name);
			$("#emailEdit").val(data.email);
			$("#passwordEdit").val(data.password);
			$("#retypeEdit").val(data.password);
			$('#roleEdit option[value='+data.id_role+']').prop('selected',true);
			$("#editModal").modal("show");
		} else {

			$("#displayDetailIcon").attr("src",data.icon);
			$("#kodeDetail").val(data.id_user);
			$("#nameDetail").val(data.name);
			$("#emailDetail").val(data.email);
			$('#roleDetail').val(data.description);
			$('#createDetail').val(data.created_at);
			$('#updateDetail').val(data.updated_at);
			$("#detailModal").modal("show");
		}
	}
    function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}
	$("#displayEditIcon").click(function(){
		$("#iconEdit").click();
	});
	$("#iconEdit").change(function(){
		 readURL2(this);
	});
	function readURL2(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	            $('#displayEditIcon').attr('src', e.target.result);
	        }
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	$("#listView").on('click',"#detailUser",function(){
		var id_user = $(this).attr('data-id');
		detail(id_user,1);
	});
	$("#listView").on('click','#deleteUser',function(){
		var id_user = $(this).attr("data-id");
		$("#btn_modal").attr('data-id',id_user);
		$("#modal_delete").modal("show");
	});
	$("#btn_modal").click(function(){
		var id_user = $(this).attr("data-id");
		$.ajax({
			type: "POST",
			url: baselink+'user/delete',
			data:{"id_user":id_user},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#modal_delete").modal("hide");
				if(data.status == 1){
					$("#content").alert("success","Success delete item");
					$("#tableUser").DataTable().destroy();
					list();
				} else {
					$("#content").alert("error","Failed delete item");
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});
});
