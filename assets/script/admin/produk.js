$(document).ready(function(){
	var baselink = $("#baselink").val();
		keyword = "";
		emptyImage = $("#baselink").val()+"../assets/dist/img/empty.png";
	list();
	function customTable(){
		text = '<div class="input-group input-group-sm pull-right" style="width: 150px;"><input type="text" name="table_search" class="form-control pull-right" placeholder="Search" id="search_keyword"><div class="input-group-btn"><button type="btn" class="btn btn-default" id="search_btn"><i class="fa fa-search"></i></button></div></div>';
		$('#tableProduk_filter').parent().append(text);
		$("#tableProduk_wrapper").on('click','#search_btn',function(){
			$("#tableProduk").DataTable().search( $("#search_keyword").val() ).draw();
		});
	}
	clearForm();
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'product/list',
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);

				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
				$("#tableProduk").DataTable({});
				$('#tableProduk_filter').attr('style','display:none')
				customTable();
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});	
	}
	function parseList(item){
		var text = "<tr>"+
                    "<td>"+item.nama_produk+"</td>"+
                    "<td class='text-center'><img class='img-responsive' src='"+baselink+'../'+item.cover+"' alt='User profile picture' width='40' height='40'></td></td>"+
                    "<td>"+item.kategori+"</td>"+
                    "<td>"+item.brand+"</td>"+
                    "<td>"+addCommas(item.harga_beli)+"</td>"+
                    "<td>"+addCommas(item.harga_jual)+"</td>"+
                    "<td>"+item.deskripsi+"</td>"+
                    "<td>"+addCommas(item.amount)+"</td>"+
                    "<td class='text-center'>"+
                    	'<span data-toggle="tooltip" data-placement="top" title="Delete Item"><button type="button" style="margin:5px;" class="btn btn-danger" id="deleteProduct" data-id="'+item.id_produk+'"><i class="fa fa-trash"></i></button></span>'+
                        '<span data-toggle="tooltip" data-placement="top" title="Edit"><button type="button" class="btn btn-info" style="margin:5px;" id="editProduct" data-id="'+item.id_produk+'"><i class="fa fa-pencil"></i></button></span>'+
                        '<span data-toggle="tooltip" data-placement="top" title="Image Slider"><button style="margin:5px;" type="button" class="btn btn-warning" id="imageProduct" data-id="'+item.id_produk+'"><i class="fa fa-image"></i></button></span>'+
                        '<span data-toggle="tooltip" data-placement="top" title="Detail Item"><button type="button" class="btn btn-info" style="margin:5px;" id="detailProduct" data-id="'+item.id_produk+'"><i class="fa fa-eye"></i></button></span>'+
                    '</td>'+
                 "</tr>";
        $("#listView").append(text);
	}
	$("#addBtn").click(function(e){
		e.preventDefault();

		$.ajax({
			type: "POST",
			url: baselink+'product/inc',
			cache: false,
			success: function(response){
				$("#kode").val(response);
				$('#inputModal').modal('show');
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});	
	});
	$("#displayCover").click(function(){
		$("#cover").click();
	});
	$("#cover").change(function(){
		 readURL(this);
	});
	function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#displayCover').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }

	}
	$("#inputForm").submit(function(e){
		e.preventDefault();
			if($('#cover')[0].files[0] == undefined){
				$("#inputContent").alert("error","You must input product cover image");
				return false
			}			
			var fsize = $('#cover')[0].files[0].size; //get file size
			var ftype = $('#cover')[0].files[0].type; // get file type			
			switch(ftype) {
	            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
	                break;
	            default:
	            	$("#inputContent").alert("error",ftype+" is not image file");	                
	                $("#iconValidate").attr("style","color:red;display:block;");
					return false
			        }

		        if(fsize>1048576) {
		        	$("#inputContent").alert("error",bytesToSize(fsize)+" The file size is to big, Please use another image file");					
					return false
				}

			$.ajax({
				type: "POST",
				url: $(this).attr('action'),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == 1){
						$("#content").alert("success","Success adding new item");
						$("#inputModal").modal("hide");
						$("#tableProduk").DataTable().destroy();
						list();
					}else{
						$("#content").alert("error","Failed adding new item")
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});

    });
    $("#editForm").submit(function(e){
		e.preventDefault();
			if($('#coverEdit')[0].files[0] != undefined){
				var fsize = $('#coverEdit')[0].files[0].size; //get file size
				var ftype = $('#coverEdit')[0].files[0].type; // get file type			
				switch(ftype) {
		            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
		                break;
		            default:
		                $("#editContent").alert("error",ftype+" is not image file");
						return false
				        }

			        if(fsize>1048576) {
						$("#editContent").alert("error",bytesToSize(fsize)+" The file size is to big, Please use another image file");
						return false
					}
			}
			
			$.ajax({
				type: "POST",
				url: $(this).attr('action'),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == 1){
						$("#content").alert("success","Success editing item");
						$("#editModal").modal("hide");
						$("#tableProduk").DataTable().destroy();
						list();
					}else{
						$("#editContent").alert("error","Failed editing item")
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});

    });
    function clearForm(){
    	$('input[type="text"]').val('');
    	$('input[type="file"]').val('');
    	$('input[type="checkbox"]').prop("checked",false);
    	$('textarea').val('');
    	$("#harga_beli").val("0");
    	$("#harga_jual").val("0");
    	$("#stok").val("0");
    	$("#brand").val($("#brand option:first").val());
    	$("#kategori").val($("#kategori option:first").val());
    	$("#displayImage1").attr('src',emptyImage);
    	$("#displayImage2").attr('src',emptyImage);
    	$("#displayImage3").attr('src',emptyImage);
    	$("#displayCover").attr('src',emptyImage);
    	$("#displayCoverEdit").attr('src',emptyImage);
    	$("#subImage1").attr('style',"display:none");
    	$("#subImage2").attr('style',"display:none");
    	$("#subImage3").attr('style',"display:none");
    }
	$("#listView").on('click','#editProduct',function(){
		var id_produk = $(this).attr('data-id');
		detail(id_produk);
	});	
	$('#inputModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})
	$('#editModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})
	$('#detailModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})
	$('#imageModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})

	function detail(id_produk,type = 0){
		$.ajax({
			type: "POST",
			url: baselink+'product/detail',
			data:{"id_produk":id_produk},
			cache: false,
			success: function(response){
				console.log(response);
				var data = jQuery.parseJSON(response);
				if(type == 0){
					parseDetail(data[0]);
				} else {
					parseDetail(data[0],1);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 
	}
	function parseDetail(data,type = 0){
		if(type == 0){		
			$("#id_produk").val(data.id_produk);
			$("#displayCoverEdit").attr("src",data.cover);
			$("#kodeEdit").val(data.id_produk);
			$("#nama_produkEdit").val(data.nama_produk);
			$("#deskripsiEdit").val(data.deskripsi);
			$('#brandEdit option[value='+data.id_brand+']').prop('selected',true);
			$('#kategoriEdit option[value='+data.id_kategori+']').prop('selected',true);
			$("#harga_beliEdit").val(addCommas(data.harga_beli));
			$("#harga_jualEdit").val(addCommas(data.harga_jual));
			(data.bestseller == 1 ? $("#bestsellerEdit").prop("checked",true) : "" );
			$("#editModal").modal("show");
		} else {
			$("#displayCoverDetail").attr("src",data.cover);
			$("#kodeDetail").val(data.id_produk);			
			$("#nama_produkDetail").val(data.nama_produk);
			$("#deskripsiDetail").val(data.deskripsi);
			$("#brandDetail").val(data.brand);
			$("#kategoriDetail").val(data.kategori);
			$("#harga_beliDetail").val(addCommas(data.harga_beli));
			$("#harga_jualDetail").val(addCommas(data.harga_jual));
			$('#createDetail').val(data.created_at);
			$('#updateDetail').val(data.updated_at);
			$("#detailModal").modal("show");
			if(data.subimage != null){
				arr = data.subimage.split(',');
				if(arr.length >= 1){
					$("#subImage1").attr("style","display:block");
					$("#displaySubImage1").attr('src',baselink+"../"+arr[0]);
				}
				if(arr.length >= 2){
					$("#subImage2").attr("style","display:block");
					$("#displaySubImage2").attr('src',baselink+"../"+arr[1]);
				}
				if(arr.length >= 3){
					$("#subImage3").attr("style","display:block");
					$("#displaySubImage3").attr('src',baselink+"../"+arr[2]);
				}
			}
		}
	}
    function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}
	$("#displayCoverEdit").click(function(){
		$("#coverEdit").click();
	});
	$("#coverEdit").change(function(){
		 readURL2(this);
	});
	function readURL2(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	            $('#displayCoverEdit').attr('src', e.target.result);
	        }
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	$("#listView").on('click',"#detailProduct",function(){
		var id_produk = $(this).attr('data-id');
		detail(id_produk,1);
	});
	$("#listView").on('click',"#imageProduct",function(){
		var id_produk = $(this).attr('data-id');
		$("#imageModal").modal("show");
		$("#image_id_produk").val(id_produk);
		checkImage(id_produk);
	});
	$("#listView").on('click','#deleteProduct',function(){
		var id_produk = $(this).attr("data-id");
		$("#btn_modal").attr('data-id',id_produk);
		$("#modal_delete").modal("show");
	});
	$("#btn_modal").click(function(){
		var id_produk = $(this).attr("data-id");
		$.ajax({
			type: "POST",
			url: baselink+'product/delete',
			data:{"id_produk":id_produk},
			cache: false,
			success: function(response){
				console.log(response)
				var data = jQuery.parseJSON(response);
				$("#modal_delete").modal("hide");
				if(data.status == 1){
					$("#content").alert("success","Success delete item");
					$("#tableProduk").DataTable().destroy();
					list();
				} else {
					$("#content").alert("error","Failed delete item");
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});
	$(".productImade").change(function(){
		var display = $(this).attr('data-display');
			if (this.files && this.files[0]) {
			        var reader = new FileReader();
			        reader.onload = function (e) {
			            $('#'+display).attr('src', e.target.result);
			        }
			        reader.readAsDataURL(this.files[0]);
			    }		
	});
	function checkImage(id_produk){
		$.ajax({
			type: "POST",
			url: baselink+'product/checkImage',
			data:{"id_produk":id_produk},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				parseImage(data);
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	}
	function parseImage(data){
		if(data.length >= 1){
			$("#indicator1").val(data[0].id_image);
			$("#displayImage1").attr('src',baselink+'../'+data[0].image);
			$("#delete1").attr('data-id',data[0].id_image);
		}
		if(data.length >= 2){
			$("#indicator2").val(data[1].id_image);
			$("#displayImage2").attr('src',baselink+'../'+data[1].image);
			$("#delete2").attr('data-id',data[1].id_image);
		}
		if(data.length >= 3){
			$("#indicator3").val(data[2].id_image);
			$("#displayImage3").attr('src',baselink+'../'+data[2].image);
			$("#delete3").attr('data-id',data[2].id_image);
		}
	}
	$(".deleteImage").click(function(){
		var id_image = $(this).attr('data-id');
			display = $("#"+$(this).attr('data-display'));
			indicator = $("#"+$(this).attr('data-indicator'));
		$.ajax({
			type: "POST",
			url: baselink+'product/deleteImage',
			data:{"id_image":id_image},
			cache: false,
			success: function(response){
				console.log(response);
				var data = jQuery.parseJSON(response);
				if(data.status == 1){
					$("#imageContent").alert("success","Success delete item");
					indicator.val("0");
					display.attr("src",emptyImage);
				} else {
					$("#imageContent").alert("error","Failed delete item");
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});

	});
	 $(".number").keydown(function (e) {

        // Allow: backspace, delete, tab, escape, enter and .
        
        if ($.inArray(e.keyCode, [8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 47 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
	 $(".number").keyup(function () {
	 	var value = $(this).val().replace(/\./g,'');
	 		arr = value.split('');
	 	if(value == ""){
	 		$(this).val("0");
	 		return
	 	}
	 	if ((arr.length > 1)&&(arr[0] == 0)){
	 		value = value.substring(1,arr.length);
	 	}
	 	//value = value.replace(/./g,'');
      	if(Math.floor(value == value)  && $.isNumeric(value)){
      	 	value = addCommas(value);
      	 	$(this).val(value);
      	} 
    });
	function addCommas(nStr) {
	    nStr += '';
	    var x = nStr.split('.');
	    var x1 = x[0];
	    var x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while (rgx.test(x1)) {
	        x1 = x1.replace(rgx, '$1' + '.' + '$2');
	    }

    	return x1 + x2;
	}
	$("#imageForm").submit(function(e){
		e.preventDefault();
			if($('#image1')[0].files[0] != undefined){
				var fsize = $('#image1')[0].files[0].size; //get file size
				var ftype = $('#image1')[0].files[0].type; // get file type			
				switch(ftype) {
		            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
		                break;
		            default:
		                $("#imageContent").alert("error",ftype+" is not image file");
						return false
				        }

			        if(fsize>1048576) {
						$("#imageContent").alert("error",bytesToSize(fsize)+" The file size is to big, Please use another image file");
						return false
					}
			}
			if($('#image2')[0].files[0] != undefined){
				var fsize = $('#image2')[0].files[0].size; //get file size
				var ftype = $('#image2')[0].files[0].type; // get file type			
				switch(ftype) {
		            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
		                break;
		            default:
		                $("#imageContent").alert("error",ftype+" is not image file");
						return false
				        }

			        if(fsize>1048576) {
						$("#imageContent").alert("error",bytesToSize(fsize)+" The file size is to big, Please use another image file");
						return false
					}
			}
			if($('#image3')[0].files[0] != undefined){
				var fsize = $('#image3')[0].files[0].size; //get file size
				var ftype = $('#image3')[0].files[0].type; // get file type			
				switch(ftype) {
		            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
		                break;
		            default:
		                $("#imageContent").alert("error",ftype+" is not image file");
						return false
				        }

			        if(fsize>1048576) {
						$("#imageContent").alert("error",bytesToSize(fsize)+" The file size is to big, Please use another image file");
						return false
					}
			}
			$.ajax({
				type: "POST",
				url: $(this).attr('action'),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == 1){
						$("#content").alert("success","Success editing item");
						$("#imageModal").modal("hide");
					}else{
						$("#imageContent").alert("error","Failed editing item")
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});

    });
});
