$(document).ready(function(){
	var baselink = $("#baselink").val();
		keyword = "";
		emptyImage = $("#baselink").val()+"../assets/dist/img/empty.png";
	$("#produk").select2();
	$("#produk").change(function(){
		$('#currentStock').val($(this).find(':selected').attr('stock'));
	});
	$("#produkEdit").select2();
	$("#produkEdit").change(function(){
		$('#currentStockEdit').val($(this).find(':selected').attr('stock'));
	});
	list();
	function customTable(){
		text = '<div class="input-group input-group-sm pull-right" style="width: 150px;"><input type="text" name="table_search" class="form-control pull-right" placeholder="Search" id="search_keyword"><div class="input-group-btn"><button type="btn" class="btn btn-default" id="search_btn"><i class="fa fa-search"></i></button></div></div>';
		$('#tableStock_filter').parent().append(text);
		$("#tableStock_wrapper").on('click','#search_btn',function(){
			$("#tableStock").DataTable().search( $("#search_keyword").val() ).draw();
		});
	}
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'summary_product/list',
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
				$("#tableStock").DataTable({});
				$('#tableStock_filter').attr('style','display:none')
				customTable();
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});	
	}
	function parseList(item){
		var text = "<tr>"+
                    "<td>"+item.created_at+"</td>"+
                    "<td>"+item.typeLabel+"</td>"+
                    "<td>"+item.nama_produk+"</td>"+
                    "<td>"+Math.abs(item.amount)+"</td>"+
                    "<td>"+item.description+"</td>"+
                    "<td class='text-center'>"+
                    	'<button class="btn btn-danger" id="deleteUser" data-id="'+item.id_stock+'" style="margin: 5px;"><i class="fa fa-trash"></i></button>'+
                        '<button class="btn btn-warning" id="editUser" data-id="'+item.id_stock+'" style="margin: 5px;"><i class="fa fa-pencil"></i></button>'+
                        '<button class="btn btn-info" id="detailUser" data-id="'+item.id_stock+'" style="margin: 5px;"><i class="fa fa-eye"></i></button>'+
                    '</td>'+
                 "</tr>";
        $("#listView").append(text);
	}
	$("#addBtn").click(function(e){
		e.preventDefault();

		$.ajax({
			type: "POST",
			url: baselink+'summary_product/inc',
			cache: false,
			success: function(response){
				$("#kode").val(response);
				$('#inputModal').modal('show');
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});	
	});
	$("#inputForm").submit(function(e){
		e.preventDefault();
			$.ajax({
				type: "POST",
				url: $(this).attr('action'),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == 1){
						$("#content").alert("success","Success adding new item");
						$("#inputModal").modal("hide");
						$("#tableStock").DataTable().destroy();
						list();
					}else{
						$("#content").alert("error","Failed adding new item")
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});

    });
    $("#editForm").submit(function(e){
		e.preventDefault();
			$.ajax({
				type: "POST",
				url: $(this).attr('action'),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == 1){
						$("#content").alert("success","Success editing item");
						$("#editModal").modal("hide");
						$("#tableStock").DataTable().destroy();
						list();
					}else{
						$("#content").alert("error","Failed editing item")
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});

    });
    function clearForm(){
    	$('input[type="text"]').val('');
    	$('textarea').val('');
    	$('select').val($("select option:first").val());
    	$("#firstRadio").prop('checked',true);
    	$("#firstRadioEdit").prop('checked',true);
    }
	$("#listView").on('click','#editUser',function(){
		var id_stock = $(this).attr('data-id');
		detail(id_stock);
	});	
	$('#inputModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})
	$('#editModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})
	$('#detailModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})

	function detail(id_stock,type = 0){
		$.ajax({
			type: "POST",
			url: baselink+'summary_product/detail',
			data:{"id_stock":id_stock},
			cache: false,
			success: function(response){
				console.log(response)
				var data = jQuery.parseJSON(response);
				if(type == 0){
					parseDetail(data[0]);
				} else {
					parseDetail(data[0],1);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 
	}
	function parseDetail(data,type = 0){
		if(type == 0){		
			$("#id_stock").val(data.id_stock);
			$('#produkEdit').val(data.id_produk).trigger('change.select2');
			$('#currentStockEdit').val($("#produkEdit").find(':selected').attr('stock'));
			$("#amountEdit").val(addCommas(Math.abs(data.amount)));
			$("#descriptionEdit").val(data.description);
			(data.type === "0") ? $("#firstRadioEdit").prop('checked',true) : $("#secondRadioEdit").prop('checked',true);
			$("#editModal").modal("show");
		} else {
			$('#produkDetail').val(data.nama_produk);			
			$("#amountDetail").val(addCommas(Math.abs(data.amount)));
			$("#descriptionDetail").val(data.description);
			$("#typeDetail").val(data.typeLabel);
			$('#createDetail').val(data.created_at);
			$('#updateDetail').val(data.updated_at);
			$("#detailModal").modal("show");
		}
	}
	$("#listView").on('click',"#detailUser",function(){
		var id_stock = $(this).attr('data-id');
		detail(id_stock,1);
	});
	$("#listView").on('click','#deleteUser',function(){
		var id_stock = $(this).attr("data-id");
		$("#btn_modal").attr('data-id',id_stock);
		$("#modal_delete").modal("show");
	});
	$("#btn_modal").click(function(){
		var id_stock = $(this).attr("data-id");
		$.ajax({
			type: "POST",
			url: baselink+'summary_product/delete',
			data:{"id_stock":id_stock},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#modal_delete").modal("hide");
				if(data.status == 1){
					$("#content").alert("success","Success delete item");
					$("#tableStock").DataTable().destroy();
					list();
				} else {
					$("#content").alert("error","Failed delete item");
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});
	 $(".number").keydown(function (e) {

        // Allow: backspace, delete, tab, escape, enter and .
        
        if ($.inArray(e.keyCode, [8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 47 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
	 $(".number").keyup(function () {
	 	var value = $(this).val().replace(/\./g,'');
	 		arr = value.split('');
	 	if(value == ""){
	 		$(this).val("0");
	 		return
	 	}
	 	if ((arr.length > 1)&&(arr[0] == 0)){
	 		value = value.substring(1,arr.length);
	 	}
	 	//value = value.replace(/./g,'');
      	if(Math.floor(value == value)  && $.isNumeric(value)){
      	 	value = addCommas(value);
      	 	$(this).val(value);
      	} 
    });
	function addCommas(nStr) {
	    nStr += '';
	    var x = nStr.split('.');
	    var x1 = x[0];
	    var x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while (rgx.test(x1)) {
	        x1 = x1.replace(rgx, '$1' + '.' + '$2');
	    }

    	return x1 + x2;
	}
});