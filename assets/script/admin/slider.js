$(document).ready(function(){
	var baselink = $("#baselink").val();
		keyword = "";
		emptyImage = $("#baselink").val()+"../assets/dist/img/empty.png";
	list();
	function customTable(){
		text = '<div class="input-group input-group-sm pull-right" style="width: 150px;"><input type="text" name="table_search" class="form-control pull-right" placeholder="Search" id="search_keyword"><div class="input-group-btn"><button type="btn" class="btn btn-default" id="search_btn"><i class="fa fa-search"></i></button></div></div>';
		$('#tableSlider_filter').parent().append(text);
		$("#tableSlider_wrapper").on('click','#search_btn',function(){
			$("#tableSlider").DataTable().search( $("#search_keyword").val() ).draw();
		});
	}
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'slider/list',
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item,i);
				});
				$("#tableSlider").DataTable({});
				$('#tableSlider_filter').attr('style','display:none')
				customTable();
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});	
	}
	function parseList(item,i){
		var text = "<tr>"+
                    "<td>"+(i+1)+"</td>"+
                    "<td> <img class='profile-user-img img-responsive' alt='Slider' src='"+baselink+"../"+item.image+"'></td>"+
                    "<td class='dont-break-out'>"+item.link+"</td>"+
                    "<td class='text-center'>"+
                    	'<button class="btn btn-danger" id="deleteUser" data-id="'+item.id_slider+'" style="margin: 5px;"><i class="fa fa-trash"></i></button>'+
                        '<button class="btn btn-warning" id="editUser" data-id="'+item.id_slider+'" style="margin: 5px;"><i class="fa fa-pencil"></i></button>'+
                        '<button class="btn btn-info" id="detailUser" data-id="'+item.id_slider+'" style="margin: 5px;"><i class="fa fa-eye"></i></button>'+
                    '</td>'+
                 "</tr>";
        $("#listView").append(text);
	}
	$("#displayIcon").click(function(){
		$("#icon").click();
	});
	$("#icon").change(function(){
		 readURL(this);
	});
	function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#displayIcon').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
	}
	$("#inputForm").submit(function(e){
		e.preventDefault();
			$("#iconValidate").attr("style","color:red;display:none;")
			if($('#icon')[0].files[0] == undefined){
				$("#inputContent").alert("error","You must input brand icon");
				return false
			}
			if($("#password").val() != $("#retype").val()){
				$("#inputContent").alert("error","The password is not match");
				return false
			}
			var fsize = $('#icon')[0].files[0].size; //get file size
			var ftype = $('#icon')[0].files[0].type; // get file type			
			switch(ftype) {
	            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
	                break;
	            default:
	                $("#iconValidate").html("<b>"+ftype+"</b> is not image file");
	                $("#iconValidate").attr("style","color:red;display:block;");
					return false
			        }

		        if(fsize>1048576) {
					$("#iconValidate").html("<b>"+bytesToSize(fsize) +"</b> The file size is to big  <br /> Please use another image file");
					$("#iconValidate").attr("style","color:red;display:block;")
					return false
				}
			$.ajax({
				type: "POST",
				url: $(this).attr('action'),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == 1){
						$("#content").alert("success","Success adding new item");
						$("#inputModal").modal("hide");
						$("#tableSlider").DataTable().destroy();
						list();
					}else{
						$("#content").alert("error","Failed adding new item")
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});

    });
    $("#editForm").submit(function(e){
		e.preventDefault();
			$("#iconEditValidate").attr("style","color:red;display:none;")
			if($('#iconEdit')[0].files[0] != undefined){
				var fsize = $('#iconEdit')[0].files[0].size; //get file size
				var ftype = $('#iconEdit')[0].files[0].type; // get file type			
				switch(ftype) {
		            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
		                break;
		            default:
		                $("#iconEditValidate").html("<b>"+ftype+"</b> is not image file");
		                $("#iconEditValidate").attr("style","color:red;display:block;");
						return false
				        }

			        if(fsize>1048576) {
						$("#iconEditValidate").html("<b>"+bytesToSize(fsize) +"</b> The file size is to big  <br /> Please use another image file");
						$("#iconEditValidate").attr("style","color:red;display:block;")
						return false
					}
			}
			if($("#passwordEdit").val() != $("#retypeEdit").val()){
				$("#editContent").alert("error","The password is not match");
				return false
			}
			
			$.ajax({
				type: "POST",
				url: $(this).attr('action'),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == 1){
						$("#content").alert("success","Success editing item");
						$("#editModal").modal("hide");
						$("#tableSlider").DataTable().destroy();
						list();
					}else{
						$("#content").alert("error","Failed editing item")
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});

    });
    function clearForm(){
    	$('input[type="text"]').val('');
  		$('input[type="email"]').val('');
  		$('input[type="password"]').val('');
    	$("#displayIcon").attr("src",emptyImage);
    	$("#displayEditIcon").attr("src",emptyImage);
    	$("#displayDetailIcon").attr("src",emptyImage);
    	$("#role").val($("#role option:first").val());
    	$("#roleEdit").val($("#roleEdit option:first").val());
    }
	$("#listView").on('click','#editUser',function(){
		var id_slider = $(this).attr('data-id');
		detail(id_slider);
	});	
	$('#inputModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})
	$('#editModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})
	$('#detailModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})

	function detail(id_slider,type = 0){
		$.ajax({
			type: "POST",
			url: baselink+'slider/detail',
			data:{"id_slider":id_slider},
			cache: false,
			success: function(response){
				console.log(response)
				var data = jQuery.parseJSON(response);
				if(type == 0){
					parseDetail(data[0]);
				} else {
					parseDetail(data[0],1);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 
	}
	function parseDetail(data,type = 0){
		if(type == 0){		
			$("#id_slider").val(data.id_slider);
			$("#displayEditIcon").attr("src",data.image);
			$("#linkEdit").val(data.link);
			$("#editModal").modal("show");
		} else {

			$("#displayDetailIcon").attr("src",data.image);
			$("#linkDetail").val(data.link);
			$('#createDetail').val(data.created_at);
			$('#updateDetail').val(data.updated_at);
			$("#detailModal").modal("show");
		}
	}
	$("#listView").on('click',"#detailUser",function(){
		var id_slider = $(this).attr('data-id');
		detail(id_slider,1);
	});
	$("#listView").on('click','#deleteUser',function(){
		var id_slider = $(this).attr("data-id");
		$("#btn_modal").attr('data-id',id_slider);
		$("#modal_delete").modal("show");
	});
	$("#btn_modal").click(function(){
		var id_slider = $(this).attr("data-id");
		$.ajax({
			type: "POST",
			url: baselink+'slider/delete',
			data:{"id_slider":id_slider},
			cache: false,
			success: function(response){
				console.log(response)
				var data = jQuery.parseJSON(response);
				$("#modal_delete").modal("hide");
				if(data.status == 1){
					$("#content").alert("success","Success delete item");
					$("#tableSlider").DataTable().destroy();
					list();
				} else {
					$("#content").alert("error","Failed delete item");
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});
	$("#displayEditIcon").click(function(){
		$("#iconEdit").click();
	});
	$("#iconEdit").change(function(){
		 readURL2(this);
	});
	function readURL2(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	            $('#displayEditIcon').attr('src', e.target.result);
	        }
	        reader.readAsDataURL(input.files[0]);
	    }
	}
});
