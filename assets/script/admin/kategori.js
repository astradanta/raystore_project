$(document).ready(function(){
	var baselink = $("#baselink").val();
		keyword = "";
		emptyImage = $("#baselink").val()+"../assets/dist/img/empty.png";
	list();
	function customTable(){
		text = '<div class="input-group input-group-sm pull-right" style="width: 150px;"><input type="text" name="table_search" class="form-control pull-right" placeholder="Search" id="search_keyword"><div class="input-group-btn"><button type="btn" class="btn btn-default" id="search_btn"><i class="fa fa-search"></i></button></div></div>';
		$('#tableKategori_filter').parent().append(text);
		$("#tableKategori_wrapper").on('click','#search_btn',function(){
			$("#tableKategori").DataTable().search( $("#search_keyword").val() ).draw();
		});
	}
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'kategori/list',
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
				$("#tableKategori").DataTable({});
				$('#tableKategori_filter').attr('style','display:none')
				customTable();
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});	
	}
	function parseList(item){
		var text = "<tr>"+
                    "<td>"+item.id_kategori+"</td>"+
                    "<td>"+item.name+"</td>"+
                    "<td class='text-center'>"+
                    	'<button class="btn btn-danger" id="deleteUser" data-id="'+item.id_kategori+'" style="margin: 5px;"><i class="fa fa-trash"></i></button>'+
                        '<button class="btn btn-warning" id="editUser" data-id="'+item.id_kategori+'" style="margin: 5px;"><i class="fa fa-pencil"></i></button>'+
                        '<button class="btn btn-info" id="detailUser" data-id="'+item.id_kategori+'" style="margin: 5px;"><i class="fa fa-eye"></i></button>'+
                    '</td>'+
                 "</tr>";
        $("#listView").append(text);
	}
	$("#addBtn").click(function(e){
		e.preventDefault();

		$.ajax({
			type: "POST",
			url: baselink+'kategori/inc',
			cache: false,
			success: function(response){
				$("#kode").val(response);
				$('#inputModal').modal('show');
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});	
	});
	$("#inputForm").submit(function(e){
		e.preventDefault();
			$.ajax({
				type: "POST",
				url: $(this).attr('action'),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == 1){
						$("#content").alert("success","Success adding new item");
						$("#inputModal").modal("hide");
						$("#tableKategori").DataTable().destroy();
						list();
					}else{
						$("#content").alert("error","Failed adding new item")
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});

    });
    $("#editForm").submit(function(e){
		e.preventDefault();
			$.ajax({
				type: "POST",
				url: $(this).attr('action'),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == 1){
						$("#content").alert("success","Success editing item");
						$("#editModal").modal("hide");
						$("#tableKategori").DataTable().destroy();
						list();
					}else{
						$("#content").alert("error","Failed editing item")
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});

    });
    function clearForm(){
    	$('input[type="text"]').val('');
  		$('input[type="email"]').val('');
  		$('input[type="password"]').val('');
    }
	$("#listView").on('click','#editUser',function(){
		var id_kategori = $(this).attr('data-id');
		detail(id_kategori);
	});	
	$('#inputModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})
	$('#editModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})
	$('#detailModal').on('hidden.bs.modal', function () {
    	clearForm();		
	})

	function detail(id_kategori,type = 0){
		$.ajax({
			type: "POST",
			url: baselink+'kategori/detail',
			data:{"id_kategori":id_kategori},
			cache: false,
			success: function(response){
				console.log(response)
				var data = jQuery.parseJSON(response);
				if(type == 0){
					parseDetail(data[0]);
				} else {
					parseDetail(data[0],1);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 
	}
	function parseDetail(data,type = 0){
		if(type == 0){		
			$("#id_kategori").val(data.id_kategori);
			$("#nameEdit").val(data.name);
			$("#editModal").modal("show");
		} else {
			$("#kodeDetail").val(data.id_kategori);
			$("#nameDetail").val(data.name);
			$('#createDetail').val(data.created_at);
			$('#updateDetail').val(data.updated_at);
			$("#detailModal").modal("show");
		}
	}
	$("#listView").on('click',"#detailUser",function(){
		var id_kategori = $(this).attr('data-id');
		detail(id_kategori,1);
	});
	$("#listView").on('click','#deleteUser',function(){
		var id_kategori = $(this).attr("data-id");
		$("#btn_modal").attr('data-id',id_kategori);
		$("#modal_delete").modal("show");
	});
	$("#btn_modal").click(function(){
		var id_kategori = $(this).attr("data-id");
		$.ajax({
			type: "POST",
			url: baselink+'kategori/delete',
			data:{"id_kategori":id_kategori},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#modal_delete").modal("hide");
				if(data.status == 1){
					$("#content").alert("success","Success delete item");
					$("#tableKategori").DataTable().destroy();
					list();
				} else {
					$("#content").alert("error","Failed delete item");
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});
});
