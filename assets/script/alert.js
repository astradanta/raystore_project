$.fn.alert = function(type, text = "") {
	var warningText = 'Warning alert preview. This alert is dismissable. ';
	var	errorText = 'Error alert preview. This alert is dismissable. ';
	var	successText = 'Success alert preview. This alert is dismissable. ';

	var	warningAlert = '<div style = "padding:10px;"><div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-warning"></i> Warning !!</h4><p id="warningText">'+warningText+'</p></div></div>';
	var	errorAlert = '<div style = "padding:10px;"><div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban"></i> Error!</h4><p id="errorText">'+errorText+'</p></div></div>';
	var	successAlert = '<div style = "padding:10px;"><div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Success!</h4><p id="successText">'+successText+'</p></div></div>';
	var id =0;		

		if (type == "warning") {
			if (text != ""){
				id = $.now();
				warningText = text;
				warningAlert = '<div id="'+id+'" style = "margin:10px;display:none;padding:10px;" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-warning"></i> Warning !!</h4><p id="warningText">'+warningText+'</p></div>';
			}
			this.prepend(warningAlert);
		} else if (type == "error") {
			if (text != ""){
				id = $.now();
				errorText = text;
				errorAlert = '<div id="'+id+'" style = "margin:10px;display:none;padding:10px;" class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban"></i> Error!</h4><p id="errorText">'+errorText+'</p></div>';
			}
			this.prepend(errorAlert);
		} else if (type == "success") {
			if (text != ""){
				id = $.now();
				successText = text;
				successAlert = '<div id="'+id+'" style = "margin:10px;display:none;padding:10px;" class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Success!</h4><p id="successText">'+successText+'</p></div>';
			}
			this.prepend(successAlert);

		} else {
			return false;
		}
		$("#"+id).fadeTo(2000, 500).slideUp(500, function(){
            $("#"+id).slideUp(500);
        });

};
