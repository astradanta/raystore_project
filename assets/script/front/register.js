$(document).ready(function(){
	var baselink = $("#baselink").val();
	$("#registerForm").submit(function(e){
		e.preventDefault();
		if($("#password").val()!=$("#confirm").val()){
			$("#container").alert("error","Password not match with your confirmation password")
			window.scrollTo(0, 0);
			return false;
		}
		
			$.ajax({
				type: "POST",
				url: $(this).attr('action'),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == 1){
						$("#container").alert("success","Register success");
						clearForm();
					}else{
						$("#container").alert("error","Register failed");
					}
					window.scrollTo(0, 0);
					window.location.href = baselink;
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});

    });
    function clearForm(){
    	$("input[type=text]").val('');
    	$("input[type=password]").val('');
    	$("input[type=email]").val('');
    	$("input[type=tel]").val('');
    	$("select").val($("select option:first").val());
    }
})
