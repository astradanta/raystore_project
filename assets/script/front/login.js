$(document).ready(function(){
	var baselink = $("#baselink").val();
	$("#loginForm").submit(function(e){
		e.preventDefault();	
			$.ajax({
				type: "POST",
				url: $(this).attr('action'),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					window.scrollTo(0, 0);
					if (data.status == 1){
						$("#container").alert("success","Login success");
						clearForm();
						window.location.href = baselink;
					}else{
						$("#container").alert("error",data.message);
					}
					
					
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});

    });
    function clearForm(){
    	$("input[type=text]").val('');
    	$("input[type=password]").val('');
    	$("input[type=email]").val('');
    	$("input[type=tel]").val('');
    	$("select").val($("select option:first").val());
    }
});