$(document).ready(function(){
	var baselink = $("#baselink").val();
		page_count = $("#page_count").val();
		currentPage = 1;
		item_count = 1;
		grid = 'product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12';
		list = 'product-layout product-list col-xs-12';
		layout = grid;
	set();
	function set(){
		$('#paginContent').html('');
		$('#paginContent').html('<ul class="pagination" id="pagination"></ul>');
		if(page_count > 0){
			$('#pagination').twbsPagination({
	            totalPages: page_count,
	            visiblePages: 5,
	            onPageClick: function (event, page) {
	                
	            }
		        }).on('page', function (event, page) {
		            currentPage = page;
		            var limit = $("#input-limit").val();
		             showing = ((page - 1) * limit) + 1;
		            $("#bottomLabel").html('Showing '+showing+' to '+((page * limit) < item_count ? (page * limit) : item_count)+' of '+item_count+' ('+page_count+' Pages)')
	                load();
		     });
		}
	}
	 
	$("#list-view").click(function(){
		layout = list;
	});
	$("#grid-view").click(function(){
		layout = grid;
	});

     function load(){
     	$.ajax({
			type: "POST",
			url: baselink+'ready_stock/load',
			data:{"id_kategori":$("#id_kategori").val(),"sort":$("#input-sort").val(),"limit":$("#input-limit").val(),"page":currentPage},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
				
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
     }
     function parseList(item){
     	var text = '<div class="'+layout+'">'+
                      '<div class="product-thumb">'+
                        '<div class="image"><a href="'+baselink+'"product_detail/"'+item.id_produk+'"><img src="'+baselink+item.cover+'" alt="'+item.nama_produk+'" title="'+item.nama_produk+'" class="img-responsive" /></a></div>'+
                        '<div>'+
                          '<div class="caption">'+
                            '<h4><a href="'+baselink+'"product_detail/"'+item.id_produk+'">'+item.nama_produk+'</a></h4>'+
                            '<p class="description">'+item.deskripsi+'</p>'+
                            '<p class="price">Rp. '+item.harga_jual+'</p>'+
                          '</div>'+
                          '<div class="button-group">'+
                            '<button class="btn-primary" type="button" onClick=""><span>Add to Cart</span></button>'+                            
                          '</div>'+
                        '</div>'+
                      '</div>'+
                    '</div>';
        $("#listView").append(text);
     }
     $("#input-limit").change(function(){
     	currentPage = 1;
     	page();
     	load();
     })
     function page(){
     	$.ajax({
			type: "POST",
			url: baselink+'ready_stock/pagination',
			data:{"id_kategori":$("#id_kategori").val(),"limit":$("#input-limit").val()},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				page_count = data.page_count;
				item_count = data.item_count;
				$("#bottomLabel").html('Showing 1 to '+data.show_item+' of '+data.item_count+' ('+data.page_count+' Pages)')
				set();
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
     }
     $("#input-sort").change(function(){
     	currentPage = 1;
     	page();
     	load();
     })
});